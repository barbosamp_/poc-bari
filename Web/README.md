# README
## Documentação Virtual (Script web)

## Sumário:
1. Objetivo da documentação.
2. Inserir informações.
3. Executar script.


## 1. Objetivo da documentação
Apresentar como executar o procedimento da virada do dia com a automação web, referente ao vendor, Virtual.

## Descrição: 
Este documento será utilizado para descrever quais informações serão inseridas, como datas e taxas, também, como executar o script. É **importante**
sempre procurar colocar as taxas atuais do mercado para investimento (CDI, LFT, IGPM e IPCA).

• Seguir procedimentos abaixo, conforme ordem do sumário;

## 2. Inserir informações

2.1 - Os dados utilizados para execução do processo e acesso a VPN já está no script conforme imagem (arquivo: `main.resource`) - img da stucture está no tópico 2.2.
      O campo `${BROWSER}` deverá ser inserido qual o browser utilizado (preferencial). Mas aconselhado rodar script no `chrome`.

  ![image](img/login.jpg)

2.2 - Após verificação do login, é importante inserir as seguintes informações no arquivo "turnDay.robot":

  ![image](img/structure.jpg)

`Parâmetros:`

  ![image](img/variables.jpg)

## 3. Executar script

Para executar o script, basta no terminal digitar a seguinte operação (Caso não esteja aberto, apertar `CTRL + "`):

  ![image](img/terminal.jpg)