*** Settings ***
Resource  ../Web/main.resource

*** Variables ***
${dataInicio}=  04/01/2020
${dataFim}=  04/01/2020
${taxa}=  1,90


*** Test Case ***

turnDay
    Abrir navegador
    Inserir Login

Registro de Cotações
    Registro Taxas CDI e LFT

Fechamento e Abertura
    Fechamento diario
    Abertura diaria

*** Keywords ***

Registro Taxas CDI e LFT

    Wait Until Page Contains  Cadastro de Cotações  50
    Click Element	class:icon-graph
    Capture Page Screenshot

    ${str}  Get Text  id:ctl00_lblDataSistema
    ${dateSystem}  Remove String  ${str}  /
    Log  ${dateSystem}
    
    ${date}=  Get Current Date
    ${date}=  Convert Date  ${date}  result_format=%d.%m.%Y
    Convert To String  ${date}
    ${dateCurrent}  Remove String  ${date}  .
    Log  ${dateCurrent}

    Run Keyword If  '${dateSystem}' == '${dateCurrent}'  exitTest

    Wait Until Page Contains  De:  30
    inputTextPersist  id:ctl00_ContentPlaceHolder1_txtDataInicio  ${dataInicio}
    
    Wait Until Element Is Enabled  id:ctl00_ContentPlaceHolder1_txtDataFim  30
    inputTextPersist  id:ctl00_ContentPlaceHolder1_txtDataFim  ${dataFim}

    Wait Until Element Is Not Visible  id:ctl00_ContentPlaceHolder1_UpProgress  30

    Wait Until Element Is Enabled  id:ctl00_ContentPlaceHolder1_lnkIncluir  30
    includInformation

    Wait Until Element Is Enabled  id:ctl00_ContentPlaceHolder1_txtDetData  90
    Input Text  id:ctl00_ContentPlaceHolder1_txtDetData  ${dataInicio}

    Wait Until Element Is Enabled  id:ctl00_ContentPlaceHolder1_txtCotacao  30
    Input Text  id:ctl00_ContentPlaceHolder1_txtCotacao  ${taxa}
    Capture Page Screenshot

    Wait Until Element Is Enabled  id:ctl00_ContentPlaceHolder1_lnkOK  50
    Click Element  id:ctl00_ContentPlaceHolder1_lnkOK
    Capture Page Screenshot

    Wait Until Element Is Not Visible  id:ctl00_ContentPlaceHolder1_UpProgress  90

    selectTypeTax
    selectOptionLFT

    Wait Until Element Is Enabled  id:ctl00_ContentPlaceHolder1_lnkIncluir  90
    includInformation

    Wait Until Element Is Enabled  id:ctl00_ContentPlaceHolder1_txtDetData  90
    Input Text  id:ctl00_ContentPlaceHolder1_txtDetData  ${dataInicio}

    Wait Until Element Is Enabled  id:ctl00_ContentPlaceHolder1_txtCotacao  30
    Input Text  id:ctl00_ContentPlaceHolder1_txtCotacao  ${taxa}

    Wait Until Element Is Enabled  id:ctl00_ContentPlaceHolder1_lnkOK  50
    Click Element  id:ctl00_ContentPlaceHolder1_lnkOK

    Wait Until Element Is Not Visible  id:ctl00_ContentPlaceHolder1_UpProgress  60


Fechamento diario

    Wait Until Element Is Enabled  xpath://a[text()='Processamento']/..  30
    Click Element  xpath://a[text()='Processamento']/..
    Capture Page Screenshot

    Wait Until Element Is Enabled  xpath://a[text()='Processamento']/..//a[text()='Fechamento']
    Click Element  xpath://a[text()='Processamento']/..//a[text()='Fechamento']
    Capture Page Screenshot

    Scroll Element Into View  id:ctl00_ContentPlaceHolder1_lnkFechamento
    Capture Page Screenshot

    Wait Until Element Is Enabled  id:ctl00_ContentPlaceHolder1_lnkFechamento  30
    Click Element  id:ctl00_ContentPlaceHolder1_lnkFechamento
    Capture Page Screenshot

    Wait Until Element Is Enabled  id:ctl00_ContentPlaceHolder1_btnConfirmaSim  30
    Click Element  id:ctl00_ContentPlaceHolder1_btnConfirmaSim
    Capture Page Screenshot

    Wait Until Element Is Enabled  id:ctl00_ContentPlaceHolder1_lblTempo  90
    Wait Until Element Is Enabled  id:ctl00_ContentPlaceHolder1_lblxxx  90

    Wait Until Page Contains  100 de 100  50
    Wait Until Page Contains  Fim do Processamento  50


Abertura diaria

    Wait Until Element Is Enabled  xpath://a[text()='Processamento']/..  30
    Click Element  xpath://a[text()='Processamento']/..
    Capture Page Screenshot

    Wait Until Element Is Enabled  xpath://a[text()='Processamento']/..//a[text()='Abertura']
    Click Element  xpath://a[text()='Processamento']/..//a[text()='Abertura']
    
    Scroll Element Into View  id:ctl00_ContentPlaceHolder1_lnkAbertura
    Capture Page Screenshot

    Wait Until Element Is Enabled  id:ctl00_ContentPlaceHolder1_lnkAbertura  30
    Click Element  id:ctl00_ContentPlaceHolder1_lnkAbertura
    Capture Page Screenshot

    Wait Until Element Is Enabled  id:ctl00_ContentPlaceHolder1_btnConfirmaSim  30  
    Click Element  id:ctl00_ContentPlaceHolder1_btnConfirmaSim

    Wait Until Element Is Enabled  id:ctl00_ContentPlaceHolder1_lblTempo  90
    Wait Until Element Is Enabled  id:ctl00_ContentPlaceHolder1_lblxxx  90

    Wait Until Page Contains  24 de 24  50
    Wait Until Page Contains  Fim do Processamento  50
    
    Fechar navegador


inputTextPersist
    [Arguments]  ${locator}  ${text}
    FOR  ${ITEM}  IN RANGE  0  10
        Input Text  ${locator}  ${text}  clear=True
        Press Keys  ${locator}  RETURN
        Wait Until Element Is Not Visible  id:ctl00_ContentPlaceHolder1_UpProgress  30
        Wait Until Element Is Enabled  ${locator}
        ${value}=  Get Element Attribute  ${locator}  value
        Exit for Loop if  '${value}'=='${text}'
    END

includInformation
    FOR  ${ITEM}  IN RANGE  0  1
        Wait Until Element Is Not Visible  id:ctl00_ContentPlaceHolder1_UpProgress  90
        Wait Until Element Is Enabled  id:ctl00_ContentPlaceHolder1_lnkIncluir  30
        Click Element	id:ctl00_ContentPlaceHolder1_lnkIncluir
    END

selectTypeTax
    FOR  ${ITEM}  IN RANGE  0  1
        Wait Until Element Is Not Visible  id:ctl00_ContentPlaceHolder1_UpProgress  30
        Wait Until Element Is Enabled  class:icon-check-alt  30
        Click Element  id:ctl00_ContentPlaceHolder1_imgAtivo
    END

selectOptionLFT
    FOR  ${ITEM}  IN RANGE  0  1
        Wait Until Element Is Enabled  xpath://td[text()='LFT']/../td/input  30
        Click Element  xpath://td[text()='LFT']/../td/input
        Wait Until Element Is Not Visible  id:ctl00_ContentPlaceHolder1_UpProgress  30
    END

exitTest
    Pass Execution  Não é necessário realizar a virada do dia, pelas datas estarem atualizadas (hoje).
    Close Browser





    
    


