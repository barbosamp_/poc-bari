import boto3
import json
import decimal
import ast
import uuid
import datetime
import hashlib
from datetime import datetime
from boto3.dynamodb.conditions import Key, Attr


def help_converter(item, key):
    item[key] = json.loads(item[key])
    return item

def consulta_dynamo(nameTable,nameAttribute,value):
    client = boto3.client('dynamodb', region_name='us-east-1')
    table = boto3.resource('dynamodb').Table(nameTable)
    response = table.scan(FilterExpression=Attr(nameAttribute).contains(value))
    item = response['Items']
    while 'LastEvaluatedKey' in response:
        response = table.scan(FilterExpression=Attr(nameAttribute).contains(value), ExclusiveStartKey=response['LastEvaluatedKey'])
        item.append(response['Items'])
    return item

def consulta_dynamo_attribute(nameTable,nameAttribute):
    att = 'attribute_exists('+str(nameAttribute)+')'
    client = boto3.client('dynamodb', region_name='us-east-1')
    table = boto3.resource('dynamodb').Table(nameTable)
    response = table.scan(FilterExpression=att)
    item = response['Items']
    while 'LastEvaluatedKey' in response:
        response = table.scan(FilterExpression=att, ExclusiveStartKey=response['LastEvaluatedKey'])
        item.append(response['Items'])
    return item

def consulta_dynamo_two_attributes(nameTable,nameAttribute1,value1,nameAttribute2,value2):
    client = boto3.client('dynamodb', region_name='us-east-1')
    table = boto3.resource('dynamodb').Table(nameTable)
    response = table.scan(FilterExpression=Attr(nameAttribute1).contains(value1) and Attr(nameAttribute2).contains(value2))
    item = response['Items']
    while 'LastEvaluatedKey' in response:
        response = table.scan(FilterExpression=Attr(nameAttribute1).contains(value1) and Attr(nameAttribute2).contains(value2), ExclusiveStartKey=response['LastEvaluatedKey'])
        item.append(response['Items'])
    return item

def consulta_dynamo_key(nameTable,nameKey,value):
    client = boto3.client('dynamodb', region_name='us-east-1')
    table = boto3.resource('dynamodb').Table(nameTable)
    response = table.query(KeyConditionExpression=Key(nameKey).eq(int(value)))
    item = response['Items']
    return item

def gera_guid():
    a = uuid.uuid4()
    return str(a)

def consulta_dynamo_int(nameTable,nameAttribute,value):
    client = boto3.client('dynamodb', region_name='us-east-1')
    table = boto3.resource('dynamodb').Table(nameTable)
    response = table.scan(FilterExpression=Attr(nameAttribute).eq(int(value)))
    item = response['Items']
    while 'LastEvaluatedKey' in response:
        response = table.scan(FilterExpression=Attr(nameAttribute).eq(int(value)), ExclusiveStartKey=response['LastEvaluatedKey'])
        item.append(response['Items'])
    return item

def consulta_dynamo_bool(nameTable,nameAttribute,value):
    client = boto3.client('dynamodb', region_name='us-east-1')
    table = boto3.resource('dynamodb').Table(nameTable)
    response = table.scan(FilterExpression=Attr(nameAttribute).eq(bool(value)))
    item = response['Items']
    while 'LastEvaluatedKey' in response:
        response = table.scan(FilterExpression=Attr(nameAttribute).eq(bool(value)), ExclusiveStartKey=response['LastEvaluatedKey'])
        item.append(response['Items'])
    return item

def retorna_data(stringData):
    date = str(datetime.strptime(stringData.strip(' \t\r\n'), '%b %d %Y'))
    data = date[0:10] 
    print(data)
    return  data

def update_windowTransaction(InitialTransactionWindowHour,FinalTransactionWindowHour):
    nameTable = 'Barigui.Services.Investment_Parameters'
    nameAttribute = 'Id'
    value = 'E05CD577-988F-41CE-97AB-B9745E2E3597'
    client = boto3.client('dynamodb', region_name='us-east-1')
    response = client.get_item(TableName=nameTable, Key={nameAttribute: {"S": value}}) 
    print(response.get('Item'))
    item = response['Item']
    print(item['InitialTransactionWindowHour'])
    print(item['FinalTransactionWindowHour'])
    hourinit = f'{InitialTransactionWindowHour}'
    hourend = f'{FinalTransactionWindowHour}'
    item['InitialTransactionWindowHour'] = {'S': hourinit}
    item['FinalTransactionWindowHour'] = {'S': hourend}
    response = client.put_item(TableName=nameTable, Item=item)
    print(response)

def update_windowTransactionVirtual(Id):
    nameTable = 'Barigui.Services.Investment_Parameters'
    nameAttribute = 'Id'
    value = f'{Id}'
    client = boto3.client('dynamodb', region_name='us-east-1')
    response = client.get_item(TableName=nameTable, Key={nameAttribute: {"S": value}}) 
    print(response.get('Item'))
    item = response['Item']
    item['TransactionWindows'] = {'L': [{'M': {'FactsheetType': {'S': 'Primary'}, 'FinalTransactionHour': {'S': '10:30'}, 'Vendor': {'S': 'Virtual'}, 'InitialTransactionHour': {'S': '09:00'}}}, {'M': {'FactsheetType': {'S': 'Secondary'}, 'FinalTransactionHour': {'S': '10:30'}, 'Vendor': {'S': 'Virtual'}, 'InitialTransactionHour': {'S': '09:00'}}}, {'M': {'InitialTransactionHour': {'S': '09:00'}, 'FinalTransactionHour': {'S': '22:00'}, 'Vendor': {'S': 'Lydians'}}}]}
    response = client.put_item(TableName=nameTable, Item=item)
    print(response)

def update_resetWindowTransactionVirtual(Id):
    nameTable = 'Barigui.Services.Investment_Parameters'
    nameAttribute = 'Id'
    value = f'{Id}'
    client = boto3.client('dynamodb', region_name='us-east-1')
    response = client.get_item(TableName=nameTable, Key={nameAttribute: {"S": value}}) 
    print(response.get('Item'))
    item = response['Item']
    item['TransactionWindows'] = {'L': [{'M': {'FactsheetType': {'S': 'Primary'}, 'FinalTransactionHour': {'S': '14:30'}, 'Vendor': {'S': 'Virtual'}, 'InitialTransactionHour': {'S': '09:00'}}}, {'M': {'FactsheetType': {'S': 'Secondary'}, 'FinalTransactionHour': {'S': '15:30'}, 'Vendor': {'S': 'Virtual'}, 'InitialTransactionHour': {'S': '09:00'}}}, {'M': {'InitialTransactionHour': {'S': '09:00'}, 'FinalTransactionHour': {'S': '22:00'}, 'Vendor': {'S': 'Lydians'}}}]}
    response = client.put_item(TableName=nameTable, Item=item)
    print(response)

def delete_customerSuitability(cpf):
    nameTable = 'Barigui.Services.Investment_CustomerSuitability'
    nameAttribute = 'CustomerId'
    value = f'{cpf}'
    print("Attempting a conditional delete...")
    client = boto3.client('dynamodb', region_name='us-east-1')
    response = client.delete_item(TableName=nameTable, Key={nameAttribute: {"S": value}}) 
    print(response) 

def delete_customerSuitabilityHistory(cpf): 
    nameTable = 'Barigui.Services.Investment_CustomerSuitabilityHistory'
    nameAttribute = 'CustomerId'
    value = f'{cpf}'
    print("Attempting a conditional delete...")
    client = boto3.client('dynamodb', region_name='us-east-1')
    response = client.delete_item(TableName=nameTable, Key={nameAttribute: {"S": value}}) 
    print(response)

def update_investmentParameters(Id): 
    nameTable = 'Barigui.Services.Investment_Parameters'
    nameAttribute = 'Id'
    value = f'{Id}'
    client = boto3.client('dynamodb', region_name='us-east-1')
    response = client.get_item(TableName=nameTable, Key={nameAttribute: {"S": value}})
    print(response.get('Item'))
    item = response['Item']
    item['AvailableProducts'] = {'L': []}
    response = client.put_item(TableName=nameTable, Item=item)
    print(response)

def update_investmentReturnParameters(Id): 
    nameTable = 'Barigui.Services.Investment_Parameters'
    nameAttribute = 'Id'
    value = f'{Id}'
    client = boto3.client('dynamodb', region_name='us-east-1')
    response = client.get_item(TableName=nameTable, Key={nameAttribute: {"S": value}})
    print(response.get('Item'))
    item = response['Item']
    item['AvailableProducts'] = {'L': [{"M": {"IsAvailable": {"BOOL": True}, "Type": {"S":"CDB"}}}, {"M": {"IsAvailable": {"BOOL": True}, "Type" : {"S": "LCI"}}}]}
    response = client.put_item(TableName=nameTable, Item=item)
    print(response)

def update_investmentFalseTypeParameter(Id): 
    nameTable = 'Barigui.Services.Investment_Parameters'
    nameAttribute = 'Id'
    value = f'{Id}'
    client = boto3.client('dynamodb', region_name='us-east-1')
    response = client.get_item(TableName=nameTable, Key={nameAttribute: {"S": value}})
    print(response.get('Item'))
    item = response['Item']
    item['AvailableProducts'] = {'L': [{"M": {"IsAvailable": {"BOOL": False}, "Type": {"S":"CDB"}}}, {"M": {"IsAvailable": {"BOOL": False}, "Type" : {"S": "LCI"}}}]}
    response = client.put_item(TableName=nameTable, Item=item)
    print(response)

def update_noAssetsActiveInvestmentFalse(Id): 
    nameTable = 'Barigui.Services.Investment_Asset'
    nameAttribute = 'Id'
    value = f'{Id}'
    client = boto3.client('dynamodb', region_name='us-east-1')
    response = client.get_item(TableName=nameTable, Key={nameAttribute: {"S": value}}) 
    print(response.get('Item'))
    item = response['Item']
    item['Active'] = {"BOOL": False}
    response = client.put_item(TableName=nameTable, Item=item)
    print(response)

def update_noAssetsActiveInvestmentTrue(Id): 
    nameTable = 'Barigui.Services.Investment_Asset'
    nameAttribute = 'Id'
    value = f'{Id}'
    client = boto3.client('dynamodb', region_name='us-east-1')
    response = client.get_item(TableName=nameTable, Key={nameAttribute: {"S": value}}) 
    print(response.get('Item'))
    item = response['Item']
    item['Active'] = {"BOOL": True}
    response = client.put_item(TableName=nameTable, Item=item)
    print(response)

def update_priorityPaperParameters(Priority,DueDay,PaperCode,Vendor): 
    nameTable = 'Barigui.Services.Investment_AssetPriority'
    nameAttribute = 'Priority'
    value = 'Priority'
    client = boto3.client('dynamodb', region_name='us-east-1')
    response = client.put_item(TableName=nameTable, Key={nameAttribute: {"S": value}}) 
    print(response.get('Item'))
    item = response['Item']
    print(item['DueDay'])
    print(item['PaperCode'])
    print(item['Vendor'])
    response = client.put_item(TableName=nameTable, Item=item)
    print(response)

def encrypt_string(hash_string):
    sha_signature = \
        hashlib.sha256(hash_string.encode()).hexdigest()
    return sha_signature

if __name__ == '__main__':
    update_investmentReturnParameters('encrypt_string')
