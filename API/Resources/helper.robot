*** Keywords ***
Nova Aba
    [arguments]     ${url}
    Execute Javascript          window.open()    
    Switch Window               locator=NEW
    Go To                       ${url}

Valida compliance
    [Arguments]  ${consulta}  ${itemDaLista}  ${numeroItem}  ${nomeValidar}    
    ${compliance}          Set Variable                           ${consulta[${itemDaLista}]['Compliance']}
    log                    ${compliance}
    ${bcCorreios}          Set Variable                           ${compliance[${numeroItem}]}
    ${validador}           Set Variable                           ${bcCorreios['Type']}
    Should Be True         "${validador}"=="${nomeValidar}"

New Token DynamoDB
    Abrir Navegador  https://bancobari.awsapps.com/start#/
    Wait Until Element Is Visible         id=mainFrame                120 second
    Input Text                            id=wdc_username             inserir_username
    Input Text                            id=wdc_password             2zsAFZpATebk8SZ    
    Click Element                         id=wdc_login_button
    ${bodyreturn}  Email Verification                    no-reply@login.awsapps.com        Verification Code:
    Log  ${bodyreturn}
    ${teste}  Fetch From Right        ${bodyreturn}     40px;">
    ${teste1}  Fetch From Left        ${teste}          </div><div
    log  ${teste1}
    Wait Until Element Is Visible         xpath=//*[@id="wdc_mfacode"]         20 second 
    Input Text                            xpath=//*[@id="wdc_mfacode"]         ${teste1}
    Click Element                         xpath=//*[@id="wdc_login_button"]/span/span/button
    Wait Until Element Is Visible         xpath=//*[@id="app-03e8643328913682"]        20 second
    Discard Email  no-reply@login.awsapps.com  Verification Code:
    Click Element                         xpath=//*[@id="app-03e8643328913682"]
    Click Element                         xpath=//*[@id="ins-07f2eb6bec3f867f"]/div/div
    Wait Until Element Is Visible         xpath=//a[@title='DynamoDBRW']/../span[2]/a      20 second
    Click Element                         xpath=//a[@title='DynamoDBRW']/../span[2]/a
    Wait Until Element Is Visible         xpath=//div[@class='dialog-body']
    Wait Until Element Is Visible         xpath=//div[4][@class='ng-tns-c18-10']      60 second
    Wait Until Element Is Visible  xpath=//*[@id="cli-cred-file-code"]/div[2]  60 second
    ${resultextract2}  Get Text            xpath=//*[@id="cli-cred-file-code"]/div[2]
    Wait Until Element Is Visible  xpath=//*[@id="cli-cred-file-code"]/div[3]  60 second
    ${resultextract4}  Get Text            xpath=//*[@id="cli-cred-file-code"]/div[3]
    Wait Until Element Is Visible  xpath=//*[@id="cli-cred-file-code"]/div[4]  60 second
    ${resultextract6}  Get Text            xpath=//*[@id="cli-cred-file-code"]/div[4]
   
    Log  ${resultextract2}
    ${stripped2}=	Strip String  ${resultextract2}
    Log  ${resultextract4}
    ${stripped4}=	Strip String  ${resultextract4}
    Log  ${resultextract6}
    ${stripped6}=	Strip String  ${resultextract6}
    ${defaultText}  Set Variable  [default]
    ${string}  catenate  ${defaultText}${\n}${stripped2}${\n}${stripped4}${\n}${stripped6}${\n}
    Remove File  C:\\Users\\Prime Control\\.aws\\credentials
    Append To File  C:\\Users\\Prime Control\\.aws\\credentials  ${string}
    Close Browser

Gera Token Cadastro
    Abrir conexao   https://api.${server}.bancobari.com.br
    ${HEADERS}  Create Dictionary
    ...  Content-Type=application/x-www-form-urlencoded
    ${PARAMS}  Create Dictionary
    ...  grant_type=client_credentials
    ...  client_id=onboarding
    ...  client_secret=7sjJhLEyKIIz
    Set Test Variable  ${HEADERS}
    ${RESPOSTAREQUEST}     Post Request     newsession      auth/connect/token
    ...     data=${PARAMS}
    ...     headers=${HEADERS}
    Log                          ${RESPOSTAREQUEST.text}
    ${json_value}  Set Variable  ${RESPOSTAREQUEST.json()}
    ${token}  Catenate  Bearer  ${json_value['access_token']}
    Set Test Variable  ${token}
    [Return]        ${token}

Gera Token Customer
    Abrir conexao   https://api.${server}.bancobari.com.br
    ${HEADERS}  Create Dictionary
    ...  Content-Type=application/x-www-form-urlencoded
    ${PARAMS}  Create Dictionary
    ...  grant_type=client_credentials
    ...  client_id=customer
    ...  client_secret=cu5Rpu32i7S
    Set Test Variable  ${HEADERS}
    ${RESPOSTAREQUEST}     Post Request     newsession      auth/connect/token
    ...     data=${PARAMS}
    ...     headers=${HEADERS}
    Log                          ${RESPOSTAREQUEST.text}
    ${json_value}  Set Variable  ${RESPOSTAREQUEST.json()}
    ${token}  Catenate  Bearer  ${json_value['access_token']}
    Set Test Variable  ${token}
    [Return]        ${token}

Gera Token Investimentos
    [Arguments]  ${cpf}  ${senha}

    Abrir conexao   https://api.${server}.bancobari.com.br
    ${HEADERS}  Create Dictionary
    ...  Content-Type=application/x-www-form-urlencoded
    ${PARAMS}  Create Dictionary
    ...  username=${cpf}
    ...  password=${senha}
    ...  client_id=mobile
    ...  client_secret=mH8341Kop
    ...  grant_type=password
    Set Test Variable  ${HEADERS}
    ${RESPOSTAREQUEST}     Post Request     newsession      auth/connect/token
    ...     data=${PARAMS}
    ...     headers=${HEADERS}
    Log                          ${RESPOSTAREQUEST.text}
    ${json_value}  Set Variable  ${RESPOSTAREQUEST.json()}
    ${token}  Catenate  Bearer  ${json_value['access_token']}
    Set Test Variable  ${token}
    [Return]  ${token}

Email Verification
    [Arguments]  ${sender}  ${content}
    Open Mailbox    host=imap.gmail.com    user=eduardo.brusamolin@baritecnologia.com.br    password=bari@2020   port=993 
    ${LATEST}    Wait For Email    sender=${sender}    timeout=12000
    ${body}    Get Email Body  ${LATEST}
    Log  ${body}
    Should Contain    ${body}    ${content}
    Close Mailbox
    [Return]  ${body}

Consult Balance User
    [Arguments]     ${guid}     ${token}
    ${PARAMS}       Create Dictionary            
    ${HEADERS}      Create Dictionary            accept=application/json  User-Agent=${guid}  Authorization=${token}  
    Abrir conexao   https://api.${server}.bancobari.com.br
    ${RESPOSTAREQUEST}       Get Request         new session         investment/v1/customers/balance
    ...                                          data=${PARAMS}
    ...                                          headers=${HEADERS}
    Set Test Variable  ${RESPOSTAREQUEST}
    Log     ${RESPOSTAREQUEST.text}

Result Suitability User
    [Arguments]     ${guid}     ${token}
    ${PARAMS}       Create Dictionary            
    ${HEADERS}      Create Dictionary            accept=application/json  User-Agent=${guid}  Authorization=${token}  
    Abrir conexao   https://api.${server}.bancobari.com.br
    ${RESPOSTAREQUEST}     Get Request           new session         investment/v1/customers/suitability
    ...                                          data=${PARAMS}
    ...                                          headers=${HEADERS}
    Set Test Variable  ${RESPOSTAREQUEST}

List Of Papers
    [Arguments]     ${guid}     ${token}    ${type}
    ${PARAMS}       Create Dictionary            
    ${HEADERS}      Create Dictionary            accept=application/json  type=${type}  User-Agent=${guid}  Authorization=${token}  
    Abrir conexao   https://api.${server}.bancobari.com.br
    ${RESPOSTAREQUEST}     Get Request           new session         investment/v1/assets?type=${type}
    ...                                          data=${PARAMS}
    ...                                          headers=${HEADERS}
    Set Test Variable  ${RESPOSTAREQUEST}

Choose Investment Type
    [Arguments]     ${guid}     ${token}      ${Id}
    ${PARAMS}       Create Dictionary            
    ${HEADERS}      Create Dictionary            accept=application/json  User-Agent=${guid}  Authorization=${token}  
    Abrir conexao   https://api.${server}.bancobari.com.br
    ${RESPOSTAREQUEST}     Get Request           new session         investment/v1/assets/${Id}
    ...                                          data=${PARAMS}
    ...                                          headers=${HEADERS}
    Set Test Variable  ${RESPOSTAREQUEST}

Redemption Investments
    [Arguments]     ${guid}    ${token}    ${itemsPerPage}   ${Page}    ${type}
    ${PARAMS}       Create Dictionary            
    ${HEADERS}      Create Dictionary            accept=application/json  User-Agent=${guid}  Authorization=${token}  
    Abrir conexao   https://api.${server}.bancobari.com.br
    ${RESPOSTAREQUEST}     Get Request           new session         investment/v1/custody?withdrawalAvailable=${type}&itemsPerPage=${itemsPerPage}&Page=${Page}
    ...                                          data=${PARAMS}
    ...                                          headers=${HEADERS}
    Set Test Variable  ${RESPOSTAREQUEST}
    Log    ${RESPOSTAREQUEST.text}

View Details Investments Redemption
    [Arguments]     ${guid}    ${token}    ${id}
    ${PARAMS}       Create Dictionary            
    ${HEADERS}      Create Dictionary            accept=application/json  User-Agent=${guid}  Authorization=${token}  
    Abrir conexao   https://api.${server}.bancobari.com.br
    ${RESPOSTAREQUEST}     Get Request           new session         investment/v1/custody/${id}
    ...                                          data=${PARAMS}
    ...                                          headers=${HEADERS}
    Set Test Variable  ${RESPOSTAREQUEST}
    Log     ${RESPOSTAREQUEST.text} 

Suitability Shipping
    [Arguments]     ${guid}    ${token}     ${json_string}
    Abrir conexao   https://api.${server}.bancobari.com.br               
    ${HEADERS}      Create Dictionary            accept=application/json  User-Agent=${guid}  Authorization=${token}  Content-Type=application/json-patch+json  
    ${RESPOSTAREQUEST}     Put Request           new session         investment/v1/customers/suitability
    ...                                          data=${json_string}
    ...                                          headers=${HEADERS}
    Set Test Variable  	${RESPOSTAREQUEST}

Shipping Amount Apply
    [Arguments]     ${guid}    ${token}   ${Id}    ${json_string}
    Abrir conexao   https://api.${server}.bancobari.com.br
    ${HEADERS}      Create Dictionary            accept=application/json   User-Agent=${guid}   Authorization=${token}   Content-Type=application/json-patch+json  
    ${RESPOSTAREQUEST}     Post Request          new session         investment/v1/assets/${Id}/apply
    ...                                          data=${json_string}
    ...                                          headers=${HEADERS}
    Set Test Variable  	${RESPOSTAREQUEST}

Shipping Transaction Value
    [Arguments]     ${guid}     ${token}     ${Id}      ${json_string}   
    Abrir conexao   https://api.${server}.bancobari.com.br     
    ${HEADERS}      Create Dictionary            User-Agent=${guid}   Authorization=${token}   Content-Type=application/json
    ${RESPOSTAREQUEST}     Post Request          new session         investment/v1/transactions
    ...                                          data=${json_string}
    ...                                          headers=${HEADERS}
    Set Test Variable  	${RESPOSTAREQUEST}

Investment Parameters
    [Arguments]     ${guid}     ${token}
    ${PARAMS}       Create Dictionary            
    ${HEADERS}      Create Dictionary            accept=application/json  User-Agent=${guid}  Authorization=${token}  
    Abrir conexao   https://api.${server}.bancobari.com.br
    ${RESPOSTAREQUEST}     Get Request           new session         investment/v1/products
    ...                                          data=${PARAMS}
    ...                                          headers=${HEADERS}
    Set Test Variable  ${RESPOSTAREQUEST}

Redemption Investments History
    [Arguments]     ${guid}  ${token}  ${id}  ${filter}  ${Page}  ${itemsPerPage}    
    ${PARAMS}       Create Dictionary            
    ${HEADERS}      Create Dictionary            accept=application/json  User-Agent=${guid}  Authorization=${token}  
    Abrir conexao   https://api.${server}.bancobari.com.br
    ${RESPOSTAREQUEST}     Get Request           new session         investment/v1/custody/${id}/history?filter=${filter}&itemsPerPage=${itemsPerPage}
    ...                                          data=${PARAMS}
    ...                                          headers=${HEADERS}
    Set Test Variable  ${RESPOSTAREQUEST}
    Log    ${RESPOSTAREQUEST.text}

Customers Profile
    [Arguments]     ${guid}    ${token}
    ${PARAMS}       Create Dictionary            
    ${HEADERS}      Create Dictionary            accept=application/json  User-Agent=${guid}  Authorization=${token}  
    Abrir conexao   https://api.${server}.bancobari.com.br
    ${RESPOSTAREQUEST}     Get Request           new session         investment/v1/customers/profile
    ...                                          data=${PARAMS}
    ...                                          headers=${HEADERS}
    Set Test Variable  ${RESPOSTAREQUEST}
    Log     ${RESPOSTAREQUEST.text}