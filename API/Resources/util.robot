*** Settings ***
Library     SeleniumLibrary

*** Keywords ***
Nova Aba
    [arguments]     ${url}
    Execute Javascript          window.open()    
    Switch Window               locator=NEW
    Go To                       ${url}