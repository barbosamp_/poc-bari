*** Settings ***
Documentation
...            As usuário administrador do backoffice,
...            Want definir a ordem inicial de investimentos,
...            So deixar em destaque os papéis que são mais interessantes para comercialização.
Resource	   ${EXECDIR}/API/Resources/main.resource
Suite Setup    Abrir conexao  https://api.${server}.bancobari.com.br

*** Test Cases ***

Consulta para verificação da ordem definida
    [Tags]  backend  automation
    Given consultado_na_base_os_registros_inseridos
    Then exibir_informacoes_consultadas_na_tabela_na_ordem_registrada

Exibição dos investimentos por ordem definida (CDB)
    [Tags]  backend  automation
    Given identificado_que_existe_investimento_CDB
    When exibido_uma_lista_com_todas_as_opcoes_compativeis_com_perfil
    Then apresenta_informacoes_disponiveis_dos_produtos_com_ordem_definida
    And response_cdb_status_200

Exibição dos investimentos por ordem definida (LCI)
    [Tags]  backend  automation
    Given identificado_que_existe_investimento_LCI
    When informacoes_disponiveis_dos_produtos_com_ordem_definida
    Then response_lci_status_200

*** Variable ***
${cpf}=  08130385961
${senha}=  123Abc

*** Keywords ***

# Case 01 (Table):

consultado_na_base_os_registros_inseridos
    Log  Todos tipos de investimentos com ordem de exibição estarão cadastrados com sua prioridade na tabela Barigui.Services.Investment_AssetPriority.

exibir_informacoes_consultadas_na_tabela_na_ordem_registrada
    ${priorityPapers}  Consulta Dynamo  Barigui.Services.Investment_AssetPriority  Vendor  Lydians
    Log  ${priorityPapers}


# Case 02 (CDB):

identificado_que_existe_investimento_CDB
    Log  Todos tipos de investimentos estarão cadastrados na tabela Barigui.Services.Investment_Asset.

exibido_uma_lista_com_todas_as_opcoes_compativeis_com_perfil
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${type}  Set Variable  CDB
    ${RESPOSTAREQUEST}  List Of Papers  ${guid}  ${token}  ${type}

apresenta_informacoes_disponiveis_dos_produtos_com_ordem_definida
    Log  ${RESPOSTAREQUEST.text}

response_cdb_status_200
    ${STATUSCODE_DESEJADO}  Set Variable  200
    Log  ${RESPOSTAREQUEST}
    Should Be Equal As Strings  ${RESPOSTAREQUEST.status_code}  ${STATUSCODE_DESEJADO}


# Case 03 (LCI):

identificado_que_existe_investimento_LCI
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${type}  Set Variable  LCI
    ${RESPOSTAREQUEST}  List Of Papers  ${guid}  ${token}  ${type}

informacoes_disponiveis_dos_produtos_com_ordem_definida
    Log  ${RESPOSTAREQUEST.text}

response_lci_status_200
    ${STATUSCODE_DESEJADO}  Set Variable   200
    Log  ${RESPOSTAREQUEST}
    Should Be Equal As Strings  ${RESPOSTAREQUEST.status_code}  ${STATUSCODE_DESEJADO}