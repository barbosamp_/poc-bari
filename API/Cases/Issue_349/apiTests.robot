*** Settings ***
Documentation
...            As usuário do aplicativo do Banco Bari,
...            Want gostaria de saber o saldo total das minhas aplicações da Virtual,
...            So entender o rendimento do meu dinheiro exibindo o detalhamento do resgate.
Default Tags   Release
Resource	   ${EXECDIR}/API/Resources/main.resource
Suite Setup    Abrir conexao  https://api.${server}.bancobari.com.br

*** Test Cases ***

Verificação do saldo do usuário da Virtual
    [Tags]  backend  automation
    Given usuario_do_aplicativo_do_Banco_Bari_que_tem_flag_habilitada_para_investimentos
    When sera_consultado_o_saldo_de_aplicacao_do_colaborador_para_aplicacoes_Virtual
    Then retorno_sera_apresentado_pelos_valores_de_aplicacao
    And response_statuscode_200

Exibir resumo do investimento com detalhes da aplicação com possibilidade de resgate (Virtual)
    [Tags]  backend  automation
    Given exibir_informacoes_de_investimentos_referente_a_Virtual
    When consulta_id_para_obter_informacoes_de_detalhamento_do_produto
    And exibir_detalhamento_sobre_o_papel_selecionado_nos_meus_investimentos
    Then response_statuscode_de_200

*** Variable ***
${cpf}=  08130385961
${senha}=  123Abc

*** Keywords ***

# Case 01 (Saldo Virtual):

usuario_do_aplicativo_do_Banco_Bari_que_tem_flag_habilitada_para_investimentos
    Log  O usuário possui Conta e tem acesso a investimentos, com dados na tabela Barigui.Services.FeatureFlag_Customerscopes e Barigui.Services.Investment_Customer.
    ${hash256}  Encrypt String  ${cpf}
    ${Customerscopes}  Consulta Dynamo  Barigui.Services.FeatureFlag_Customerscopes  CustomerId  ${hash256}
    Log  ${Customerscopes}
    Log  ${Customerscopes[0]['Scopes'][3]}
    
    ${accountNumber}  Consulta Dynamo  Barigui.Services.Investment_Customer  CustomerId  ${cpf}
    Log  ${accountNumber}
    Log  ${accountNumber[0]['Account']}

sera_consultado_o_saldo_de_aplicacao_do_colaborador_para_aplicacoes_Virtual
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${RESPOSTAREQUEST}  Consult Balance User  ${guid}  ${token}

retorno_sera_apresentado_pelos_valores_de_aplicacao
    Log  ${RESPOSTAREQUEST}
    Log  ${RESPOSTAREQUEST.json()}
    ${resultLCjson}  Set Variable   ${RESPOSTAREQUEST.json()}
    ${resultType}  Set Variable  ${resultLCjson['byType'][3]['type']}
    ${resultInvestmentValue}  Set Variable  ${resultLCjson['byType'][3]['investmentValue']}
    ${resultGrossEarnings}  Set Variable  ${resultLCjson['byType'][3]['grossEarnings']}
    ${resultBalance}  Set Variable  ${resultLCjson['byType'][3]['balance']}

    Log  ${resultType}
    Should Be True  '${resultType}' == 'CRI'
    Log  ${resultInvestmentValue}
    Log  ${resultGrossEarnings}
    Log  ${resultBalance}

response_statuscode_200
    ${STATUSCODE_DESEJADO}   Set Variable   200
    Should Be Equal As Strings    ${RESPOSTAREQUEST.status_code}    ${STATUSCODE_DESEJADO}


# Case 02 (Detalhamento do produto Virtual):

exibir_informacoes_de_investimentos_referente_a_Virtual
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${itemsPerPage}  Set Variable  5
    ${Page}  Set Variable  0
    ${type}  Set Variable  false
    ${RESPOSTAREQUEST}  Redemption Investments  ${guid}  ${token}  ${itemsPerPage}  ${Page}  ${type}

consulta_id_para_obter_informacoes_de_detalhamento_do_produto
    Log  ${RESPOSTAREQUEST.text}
    ${custodyId}  Set Variable   ${RESPOSTAREQUEST.json()}
    ${id}  Set Variable  ${custodyId['items'][0]['id']}
    Set Test Variable  ${id}
    ${RESPOSTAREQUEST}  View Details Investments Redemption  ${guid}  ${token}  ${id}

exibir_detalhamento_sobre_o_papel_selecionado_nos_meus_investimentos
    Log  ${RESPOSTAREQUEST.json()}
    ${resultItem}  Set Variable   ${RESPOSTAREQUEST.json()}
    ${resultDescription}  Set Variable  ${resultItem['description']}
    ${resultLiquidity}  Set Variable  ${resultItem['liquidity']}
    
    Log  ${resultDescription}
    Log  ${resultLiquidity}

response_statuscode_de_200
    ${STATUSCODE_DESEJADO}  Set Variable   200
    Log  ${RESPOSTAREQUEST}
    Should Be Equal As Strings  ${RESPOSTAREQUEST.status_code}  ${STATUSCODE_DESEJADO}