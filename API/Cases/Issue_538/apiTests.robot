*** Settings ***
Documentation
...            As usuário do aplicativo do Banco Bari,
...            Want gostaria de saber o saldo total das minhas aplicações da LC,
...            So entender o rendimento do meu dinheiro podendo realizar o resgate.
Default Tags   Release
Resource	   ${EXECDIR}/API/Resources/main.resource
Suite Setup    Abrir conexao  https://api.${server}.bancobari.com.br

*** Test Cases ***

Verificação do saldo do usuário da LC
    [Tags]  backend  automation
    Given usuario_do_aplicativo_do_Banco_Bari_que_tem_flag_habilitada_para_investimentos
    When sera_consultado_o_saldo_de_aplicacao_do_colaborador_para_aplicacoes_LC
    Then retorno_sera_apresentado_pelos_valores_de_aplicacao
    And response_statuscode_200

Exibir resumo do investimento com detalhes da aplicação com possibilidade de resgate LC
    [Tags]  backend  automation
    Given exibir_informacoes_de_investimentos_referente_ao_resgate_LC
    When consulta_id_para_obter_informacoes_de_detalhamento_do_produto
    And exibir_detalhamento_sobre_o_papel_selecionado_para_resgate
    Then response_statuscode_de_200

Enviar valores de resgate para investimento (Withdraw)
    [Tags]  backend  automation
    Given consulta_papel_para_resgate
    And exibir_id_do_papel_selecionado_para_resgate
    When enviar_informacoes_de_resgate_withdraw
    And response_status_204
    Then verifica_balance_do_usuario_withdraw
    And verifica_envio_da_nota_withdraw

*** Variable ***
${cpf}=  08130385961
${senha}=  123Abc

*** Keywords ***

# Case 01 (Saldo LC):

usuario_do_aplicativo_do_Banco_Bari_que_tem_flag_habilitada_para_investimentos
    Log  O usuário possui Conta e tem acesso a investimentos, com dados na tabela Barigui.Services.FeatureFlag_Customerscopes e Barigui.Services.Investment_Customer.
    ${hash256}  Encrypt String  ${cpf}
    ${Customerscopes}  Consulta Dynamo  Barigui.Services.FeatureFlag_Customerscopes  CustomerId  ${hash256}
    Log  ${Customerscopes}
    Log  ${Customerscopes[0]['Scopes'][3]}
    
    ${accountNumber}  Consulta Dynamo  Barigui.Services.Investment_Customer  CustomerId  ${cpf}
    Log  ${accountNumber}
    Log  ${accountNumber[0]['Account']}

sera_consultado_o_saldo_de_aplicacao_do_colaborador_para_aplicacoes_LC
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${RESPOSTAREQUEST}  Consult Balance User  ${guid}  ${token}

retorno_sera_apresentado_pelos_valores_de_aplicacao
    Log  ${RESPOSTAREQUEST}
    Log  ${RESPOSTAREQUEST.json()}
    ${resultLCjson}  Set Variable   ${RESPOSTAREQUEST.json()}
    ${resultType}  Set Variable  ${resultLCjson['byType'][2]['type']}
    ${resultInvestmentValue}  Set Variable  ${resultLCjson['byType'][2]['investmentValue']}
    ${resultGrossEarnings}  Set Variable  ${resultLCjson['byType'][2]['grossEarnings']}
    ${resultBalance}  Set Variable  ${resultLCjson['byType'][2]['balance']}

    Log  ${resultType}
    Should Be True  '${resultType}' == 'LC'
    Log  ${resultInvestmentValue}
    Log  ${resultGrossEarnings}
    Log  ${resultBalance}

response_statuscode_200
    ${STATUSCODE_DESEJADO}   Set Variable   200
    Should Be Equal As Strings    ${RESPOSTAREQUEST.status_code}    ${STATUSCODE_DESEJADO}


# Case 02 (Detalhamento do produto LC):

exibir_informacoes_de_investimentos_referente_ao_resgate_LC
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${itemsPerPage}  Set Variable  5
    ${Page}  Set Variable  0
    ${type}  Set Variable  true
    ${RESPOSTAREQUEST}  Redemption Investments  ${guid}  ${token}  ${itemsPerPage}  ${Page}  ${type}

consulta_id_para_obter_informacoes_de_detalhamento_do_produto
    Log  ${RESPOSTAREQUEST.text}
    ${custodyId}  Set Variable   ${RESPOSTAREQUEST.json()}
    ${id}  Set Variable  ${custodyId['items'][4]['id']}
    Set Test Variable  ${id}
    ${RESPOSTAREQUEST}  View Details Investments Redemption  ${guid}  ${token}  ${id}

exibir_detalhamento_sobre_o_papel_selecionado_para_resgate
    Log  ${RESPOSTAREQUEST.json()}
    ${resultItem}  Set Variable   ${RESPOSTAREQUEST.json()}
    ${resultDescription}  Set Variable  ${resultItem['description']}
    ${resultLiquidity}  Set Variable  ${resultItem['liquidity']}
    
    Log  ${resultDescription}
    Log  ${resultLiquidity}

response_statuscode_de_200
    ${STATUSCODE_DESEJADO}  Set Variable   200
    Log  ${RESPOSTAREQUEST}
    Should Be Equal As Strings  ${RESPOSTAREQUEST.status_code}  ${STATUSCODE_DESEJADO}


# Case 03 (Withdraw LC):

consulta_papel_para_resgate
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${itemsPerPage}  Set Variable  5
    ${Page}  Set Variable  0
    ${type}  Set Variable  true
    ${RESPOSTAREQUEST}  Redemption Investments  ${guid}  ${token}  ${itemsPerPage}  ${Page}  ${type}

exibir_id_do_papel_selecionado_para_resgate
    ${custodyId}  Set Variable  ${RESPOSTAREQUEST.json()}
    ${id}  Set Variable  ${custodyId['items'][4]['id']}
    Set Test Variable  ${id}

enviar_informacoes_de_resgate_withdraw

    ${json_string}=  catenate

    ...	 {	
    ...   "type": "Withdraw",
    ...   "value": 1000,
    ...   "valueId": "${id}"
    ...	 }

    Log  ${json_string}
    ${RESPOSTAREQUEST}  Shipping Transaction Value  ${guid}  ${token}  ${Id}  ${json_string}

response_status_204
    Sleep  10
    ${STATUSCODE_DESEJADO}   Set Variable  204
    Log  ${RESPOSTAREQUEST}
    Should Be Equal As Strings  ${RESPOSTAREQUEST.status_code}  ${STATUSCODE_DESEJADO}

verifica_balance_do_usuario_withdraw
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${RESPOSTAREQUEST}  Consult Balance User  ${guid}  ${token}

verifica_envio_da_nota_withdraw
    ${body}  Email Verification  no-reply@qa.bancobari.com.br  Resgate Renda Fixa