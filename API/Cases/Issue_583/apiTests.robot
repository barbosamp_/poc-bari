*** Settings ***
Documentation
...            As usuário do Banco Bari que possui investimentos disponíveis para resgate,
...            Want visualizar detalhamento das informações sobre o papel escolhido,
...            So incluido o tipo de papel referenciado pelo campo type.
Default Tags   Release
Resource	   ${EXECDIR}/API/Resources/main.resource
Suite Setup    Abrir conexao  https://api.${server}.bancobari.com.br

*** Test Cases ***

Exibir resumo do investimento com inclusão do campo type (True)
    [Tags]  backend  automation
    Given exibir_informacoes_de_investimentos_referente_ao_resgate_true
    When consulta_id_para_obter_informacoes_de_detalhamento_do_resgate
    And exibir_detalhamento_sobre_o_papel_selecionado_para_resgate_disponivel
    Then response_devera_ter_statuscode_de_200

Exibir resumo do investimento com inclusão do campo type (False)
    [Tags]  backend  automation
    Given exibir_informacoes_de_investimentos_referente_ao_resgate_false
    When consulta_pelo_id_para_obter_informacoes_de_detalhamento_do_resgate
    And exibir_detalhamento_sobre_o_papel_selecionado_para_resgate_nao_disponviel
    Then response_statuscode_200

*** Variable ***
${cpf}=  08130385961
${senha}=  123Abc

*** Keywords ***

# Case 01 (True):

exibir_informacoes_de_investimentos_referente_ao_resgate_true
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${itemsPerPage}  Set Variable  1
    ${Page}  Set Variable  0
    ${type}  Set Variable  true
    ${RESPOSTAREQUEST}  Redemption Investments  ${guid}  ${token}  ${itemsPerPage}  ${Page}  ${type}

consulta_id_para_obter_informacoes_de_detalhamento_do_resgate
    Log  ${RESPOSTAREQUEST.text}
    ${custodyId}  Set Variable   ${RESPOSTAREQUEST.json()}
    ${id}  Set Variable  ${custodyId['items'][0]['id']}
    Set Test Variable  ${id}
    ${RESPOSTAREQUEST}  View Details Investments Redemption  ${guid}  ${token}  ${id}

exibir_detalhamento_sobre_o_papel_selecionado_para_resgate_disponivel
    Log  ${RESPOSTAREQUEST.json()}
    ${resultItem}  Set Variable   ${RESPOSTAREQUEST.json()}
    ${resultDescription}  Set Variable  ${resultItem['description']}
    ${resultLiquidity}  Set Variable  ${resultItem['liquidity']}
    ${resultProfitability}  Set Variable  ${resultItem['profitability']}
    ${resultType}  Set Variable  ${resultItem['type']}
    Log  ${resultDescription}
    Log  ${resultLiquidity}
    Log  ${resultProfitability}
    Log  ${resultType}

response_devera_ter_statuscode_de_200
    ${STATUSCODE_DESEJADO}  Set Variable   200
    Log  ${RESPOSTAREQUEST}
    Should Be Equal As Strings  ${RESPOSTAREQUEST.status_code}  ${STATUSCODE_DESEJADO}


# Case 02 (False):

exibir_informacoes_de_investimentos_referente_ao_resgate_false
    ${guid}  Gera Guid 
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${itemsPerPage}  Set Variable  2
    ${Page}  Set Variable  0
    ${type}  Set Variable  false
    ${RESPOSTAREQUEST}  Redemption Investments  ${guid}  ${token}  ${itemsPerPage}  ${Page}  ${type}  

consulta_pelo_id_para_obter_informacoes_de_detalhamento_do_resgate
    ${custodyId}  Set Variable   ${RESPOSTAREQUEST.json()}
    ${id}  Set Variable  ${custodyId['items'][0]['id']}
    Set Test Variable  ${id}
    ${RESPOSTAREQUEST}  View Details Investments Redemption  ${guid}  ${token}  ${id}

exibir_detalhamento_sobre_o_papel_selecionado_para_resgate_nao_disponviel
    Log  ${RESPOSTAREQUEST.json()}
    ${resultItem}  Set Variable   ${RESPOSTAREQUEST.json()}
    ${resultDescription}  Set Variable  ${resultItem['description']}
    ${resultLiquidity}  Set Variable  ${resultItem['liquidity']}
    ${resultProfitability}  Set Variable  ${resultItem['profitability']}
    ${resultType}  Set Variable  ${resultItem['type']}
    Log  ${resultDescription}
    Log  ${resultLiquidity}
    Log  ${resultProfitability}
    Log  ${resultType}

response_statuscode_200
    ${STATUSCODE_DESEJADO}  Set Variable   200
    Log  ${RESPOSTAREQUEST}
    Should Be Equal As Strings  ${RESPOSTAREQUEST.status_code}  ${STATUSCODE_DESEJADO}