*** Settings ***
Documentation
...            As usuário do aplicativo do Banco Bari com acesso a área de Investimentos,
...            Want gostaria de saber quais são as opções de investimento,
...            So será emitido títulos de CDB e LCI.
Default Tags   Sprint32
Resource	   ${EXECDIR}/API/Resources/main.resource
Suite Setup    Abrir conexao  https://api.${server}.bancobari.com.br

*** Test Cases ***

Consulta de opçoes de investimento
    [Tags]  backend  automation
    Given identificado_que_existe_tipo_de_investimento
    When exibido_uma_lista_com_todas_as_opcoes_de_investimento_compativeis_com_perfil_CDB
    Then apresenta_mais_informacoes_sobre_o_produto_CDB
    When exibido_uma_lista_com_todas_as_opcoes_de_investimento_compativeis_com_perfil_LCI
    Then apresenta_mais_informacoes_sobre_o_produto_LCI
    When exibido_uma_lista_com_todas_as_opcoes_de_investimento_compativeis_com_perfil_CRI
    Then apresenta_mais_informacoes_sobre_o_produto_CRI
    And response_devera_ter_status_200

Produto inexistente
    [Tags]  backend  automation
    Given identificado_que_nao_existe_tipo_investimento
    When consulta_para_obter_informacoes_do_tipo_de_investimento_desejado
    Then ausencia_das_informacoes_sobre_o_produto
    And response_para_ausencia_devera_ter_status_200

*** Variable ***
${cpf}=  08130385961
${senha}=  123Abc

*** Keywords ***

# Case 01 (Papeis válidos):

identificado_que_existe_tipo_de_investimento
    Log  Todos tipos de investimentos estarão cadastrados na tabela Barigui.Services.Investment_Asset.
    ${assetsResult}  Consulta Dynamo Attribute  Barigui.Services.Investment_Asset  Id
    Log  ${assetsResult}

exibido_uma_lista_com_todas_as_opcoes_de_investimento_compativeis_com_perfil_CDB
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${type}  Set Variable  CDB
    ${RESPOSTAREQUEST}  List Of Papers  ${guid}  ${token}  ${type}

apresenta_mais_informacoes_sobre_o_produto_CDB
    Log  ${RESPOSTAREQUEST.text}

exibido_uma_lista_com_todas_as_opcoes_de_investimento_compativeis_com_perfil_LCI
    ${type}  Set Variable  LCI
    ${RESPOSTAREQUEST}  List Of Papers  ${guid}  ${token}  ${type}

apresenta_mais_informacoes_sobre_o_produto_LCI
    Log  ${RESPOSTAREQUEST.text}

exibido_uma_lista_com_todas_as_opcoes_de_investimento_compativeis_com_perfil_CRI
    ${type}  Set Variable  CRI
    ${RESPOSTAREQUEST}  List Of Papers  ${guid}  ${token}  ${type}

apresenta_mais_informacoes_sobre_o_produto_CRI
    Log  ${RESPOSTAREQUEST.text}

response_devera_ter_status_200
    ${STATUSCODE_DESEJADO}  Set Variable  200
    Log  ${RESPOSTAREQUEST}
    Should Be Equal As Strings  ${RESPOSTAREQUEST.status_code}  ${STATUSCODE_DESEJADO}


# Case 02 (Papeis inexistentes):

identificado_que_nao_existe_tipo_investimento
    Log  Todos tipos de investimentos estarão cadastrados na tabela Barigui.Services.Investment_Asset, caso não esteja, não será apresentado.

consulta_para_obter_informacoes_do_tipo_de_investimento_desejado
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable   ${guid}
    Set Test Variable   ${token}
    ${type}  Set Variable   abc
    ${RESPOSTAREQUEST}  List Of Papers  ${guid}  ${token}  ${type}

ausencia_das_informacoes_sobre_o_produto
    Log  ${RESPOSTAREQUEST.text}

response_para_ausencia_devera_ter_status_200
    ${STATUSCODE_DESEJADO}   Set Variable   200
    Log  ${RESPOSTAREQUEST}
    Should Be Equal As Strings  ${RESPOSTAREQUEST.status_code}  ${STATUSCODE_DESEJADO}