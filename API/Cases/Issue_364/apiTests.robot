*** Settings ***
Documentation
...            As usuário que possui investimentos no Bari,
...            Want ter um visualização segmentada do meu saldo,
...            So saber em quais tipos de investimento estão rendendo mais.
Default Tags   Release
Resource	   ${EXECDIR}/API/Resources/main.resource
Suite Setup    Abrir conexao  https://api.${server}.bancobari.com.br

*** Test Cases ***

Verificação do saldo do usuário (Aplicações de CDB, LCI, LC e CRI)
    [Tags]  backend  automation
    Given usuario_do_aplicativo_do_Banco_Bari_tem_acesso_e_aplicacoes_em_investimentos_1
    When sera_consultado_o_saldo_de_suas_aplicacoes_cdb_lci_lc_cri
    Then retorno_sera_apresentado_pelos_principais_valores_de_aplicacao_total_e_detalhados_por_produtos
    And resultados_esperados_geral

Verificação do saldo do usuário (Saldo zerado)
    [Tags]  backend  automation
    Given consultado_o_saldo_de_suas_aplicacoes
    When retorno_sera_apresentado_com_valores_principais_zerados
    Then resultados_esperados_zerados

Verificação do saldo do usuário (Somente aplicações de CDB)
    [Tags]  backend  automation
    Given usuario_do_aplicativo_do_Banco_Bari_tem_acesso_e_aplicacoes_em_investimentos_3
    When sera_consultado_o_saldo_de_suas_aplicacoes_cdb
    Then retorno_sera_apresentado_pelos_principais_valores_de_aplicacao_total_e_detalhados_por_produto_cdb
    And resultado_esperado_cdb

Verificação do saldo do usuário (Somente aplicações de LCI)
    [Tags]  backend  automation
    Given usuario_do_aplicativo_do_Banco_Bari_tem_acesso_e_aplicacoes_em_investimentos_4
    When sera_consultado_o_saldo_de_suas_aplicacoes_lci
    Then retorno_sera_apresentado_pelos_principais_valores_de_aplicacao_total_e_detalhados_por_produto_lci
    And resultado_esperado_lci

Verificação do saldo do usuário (CDB e LCI)
    [Tags]  backend  automation
    Given usuario_do_aplicativo_do_Banco_Bari_tem_acesso_e_aplicacoes_em_investimentos_5
    When sera_consultado_o_saldo_de_suas_aplicacoes_cdb_e_lci
    Then retorno_sera_apresentado_pelos_principais_valores_de_aplicacao_total_e_detalhados_pelos_produtos
    And resultado_esperado_em_ambas_aplicacoes

*** Variable ***
${cpf}=  08130385961
${senha}=  123Abc

*** Keywords ***

# Case 01 (Saldo CDB, LCI, LC e CRI):

usuario_do_aplicativo_do_Banco_Bari_tem_acesso_e_aplicacoes_em_investimentos_1
    Log  O usuário possui Conta e tem acesso a investimentos, com dados na tabela Barigui.Services.FeatureFlag_Customerscopes (Flag: Investments).
    ${hash256}  Encrypt String  ${cpf}
    ${Customerscopes}  Consulta Dynamo  Barigui.Services.FeatureFlag_Customerscopes  CustomerId  ${hash256}
    Log  ${Customerscopes}
    Log  ${Customerscopes[0]['Scopes'][3]}

    ${accountNumber}  Consulta Dynamo  Barigui.Services.Investment_Customer  CustomerId  ${cpf}
    Log  ${accountNumber}
    Log  ${accountNumber[0]['Account']}

sera_consultado_o_saldo_de_suas_aplicacoes_cdb_lci_lc_cri
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${RESPOSTAREQUEST}  Consult Balance User  ${guid}  ${token}

retorno_sera_apresentado_pelos_principais_valores_de_aplicacao_total_e_detalhados_por_produtos
    Log  ${RESPOSTAREQUEST.text}

resultados_esperados_geral
    ${STATUSCODE_DESEJADO}   Set Variable   200
    Log  ${RESPOSTAREQUEST}
    Should Be Equal As Strings  ${RESPOSTAREQUEST.status_code}  ${STATUSCODE_DESEJADO}


# Case 02 (Saldo zerado):

consultado_o_saldo_de_suas_aplicacoes
    ${cpf}  Set Variable  15378906466
    ${senha}  Set Variable  123Abc
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${RESPOSTAREQUEST}  Consult Balance User  ${guid}  ${token}

retorno_sera_apresentado_com_valores_principais_zerados
    Log  ${RESPOSTAREQUEST.text}
    Set Test Variable  ${customerInvestments}  ${RESPOSTAREQUEST.json()}
    Set Test Variable  ${customerInvestmentValue}  ${customerInvestments['investmentValue']}
    Set Test Variable  ${customerGrossEarnings}  ${customerInvestments['grossEarnings']}
    Set Test Variable  ${customerBalance}  ${customerInvestments['balance']}

resultados_esperados_zerados
    Log  ${customerInvestmentValue}
    Log  ${customerGrossEarnings}
    Log  ${customerBalance}
    ${STATUSCODE_DESEJADO}   Set Variable   200
    Log  ${RESPOSTAREQUEST}
    Should Be Equal As Strings  ${RESPOSTAREQUEST.status_code}  ${STATUSCODE_DESEJADO}


# Case 03 (Saldo somente com CDB):

usuario_do_aplicativo_do_Banco_Bari_tem_acesso_e_aplicacoes_em_investimentos_3
    Log  O usuário possui Conta e tem acesso a investimentos, com dados na tabela Barigui.Services.FeatureFlag_Customerscopes (Flag: Investments).
    ${cpf}  Set Variable  09699387998
    ${hash256}  Encrypt String  ${cpf}
    ${Customerscopes}  Consulta Dynamo  Barigui.Services.FeatureFlag_Customerscopes  CustomerId  ${hash256}
    Log  ${Customerscopes}
    Log  ${Customerscopes[0]['Scopes'][4]}

    ${accountNumber}  Consulta Dynamo  Barigui.Services.Investment_Customer  CustomerId  ${cpf}
    Log  ${accountNumber}
    Log  ${accountNumber[0]['Account']}

sera_consultado_o_saldo_de_suas_aplicacoes_cdb
    ${cpf}  Set Variable  09699387998
    ${senha}  Set Variable  123Abc
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${RESPOSTAREQUEST}  Consult Balance User  ${guid}  ${token}

retorno_sera_apresentado_pelos_principais_valores_de_aplicacao_total_e_detalhados_por_produto_cdb
    Log  ${RESPOSTAREQUEST.text}
    Set Test Variable  ${customerInvestments}  ${RESPOSTAREQUEST.json()}
    Set Test Variable  ${customerType}  ${customerInvestments['byType'][0]['type']}
    Set Test Variable  ${customerInvestmentsValue}  ${customerInvestments['byType'][0]['investmentValue']}
    Set Test Variable  ${customerGrossEarnings}  ${customerInvestments['byType'][0]['grossEarnings']}
    Set Test Variable  ${customerBalance}  ${customerInvestments['byType'][0]['balance']}

resultado_esperado_cdb
    Log  ${customerType}
    Log  ${customerInvestmentsValue}
    Log  ${customerGrossEarnings}
    Log  ${customerBalance}
    ${STATUSCODE_DESEJADO}   Set Variable   200
    Log  ${RESPOSTAREQUEST}
    Should Be Equal As Strings  ${RESPOSTAREQUEST.status_code}  ${STATUSCODE_DESEJADO}


# Case 04 (Saldo somente com LCI):

usuario_do_aplicativo_do_Banco_Bari_tem_acesso_e_aplicacoes_em_investimentos_4
    Log  O usuário possui Conta e tem acesso a investimentos, com dados na tabela Barigui.Services.FeatureFlag_Customerscopes (Flag: Investments).
    ${cpf}  Set Variable  08884857945
    ${hash256}  Encrypt String  ${cpf}
    ${Customerscopes}  Consulta Dynamo  Barigui.Services.FeatureFlag_Customerscopes  CustomerId  ${hash256}
    Log  ${Customerscopes}
    Log  ${Customerscopes[0]['Scopes'][4]}

    ${accountNumber}  Consulta Dynamo  Barigui.Services.Investment_Customer  CustomerId  ${cpf}
    Log  ${accountNumber}
    Log  ${accountNumber[0]['Account']}

sera_consultado_o_saldo_de_suas_aplicacoes_lci
    ${cpf}  Set Variable  08884857945
    ${senha}  Set Variable  123Abc
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${RESPOSTAREQUEST}  Consult Balance User  ${guid}  ${token}

retorno_sera_apresentado_pelos_principais_valores_de_aplicacao_total_e_detalhados_por_produto_lci
    Log  ${RESPOSTAREQUEST.text}
    Set Test Variable  ${customerInvestments}  ${RESPOSTAREQUEST.json()}
    Set Test Variable  ${customerType}  ${customerInvestments['byType'][0]['type']}
    Set Test Variable  ${customerInvestmentsValue}  ${customerInvestments['byType'][0]['investmentValue']}
    Set Test Variable  ${customerGrossEarnings}  ${customerInvestments['byType'][0]['grossEarnings']}
    Set Test Variable  ${customerBalance}  ${customerInvestments['byType'][0]['balance']}

resultado_esperado_lci
    Log  ${customerType}
    Log  ${customerInvestmentsValue}
    Log  ${customerGrossEarnings}
    Log  ${customerBalance}
    ${STATUSCODE_DESEJADO}   Set Variable   200
    Log  ${RESPOSTAREQUEST}
    Should Be Equal As Strings  ${RESPOSTAREQUEST.status_code}  ${STATUSCODE_DESEJADO}


# Case 05 (CDB e LCI):

usuario_do_aplicativo_do_Banco_Bari_tem_acesso_e_aplicacoes_em_investimentos_5
    Log  O usuário possui Conta e tem acesso a investimentos, com dados na tabela Barigui.Services.FeatureFlag_Customerscopes (Flag: Investments).
    ${cpf}  Set Variable  09478206982
    ${hash256}  Encrypt String  ${cpf}
    ${Customerscopes}  Consulta Dynamo  Barigui.Services.FeatureFlag_Customerscopes  CustomerId  ${hash256}
    Log  ${Customerscopes}
    Log  ${Customerscopes[0]['Scopes'][4]}

    ${accountNumber}  Consulta Dynamo  Barigui.Services.Investment_Customer  CustomerId  ${cpf}
    Log  ${accountNumber}
    Log  ${accountNumber[0]['Account']}

sera_consultado_o_saldo_de_suas_aplicacoes_cdb_e_lci
    ${cpf}  Set Variable  09478206982
    ${senha}  Set Variable  123Abc
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${RESPOSTAREQUEST}  Consult Balance User  ${guid}  ${token}

retorno_sera_apresentado_pelos_principais_valores_de_aplicacao_total_e_detalhados_pelos_produtos
    Log  ${RESPOSTAREQUEST.text}

resultado_esperado_em_ambas_aplicacoes
    ${STATUSCODE_DESEJADO}   Set Variable   200
    Log  ${RESPOSTAREQUEST}
    Should Be Equal As Strings  ${RESPOSTAREQUEST.status_code}  ${STATUSCODE_DESEJADO}