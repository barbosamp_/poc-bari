*** Settings ***
Documentation
...            As usuário do Banco Bari que possui investimentos disponíveis para resgate antes do vencimento,
...            Want saber mais informações sobre o papel escolhido com taxas de IGPM e IPCA,
...            So decidir se vou solicitar o resgate.
Default Tags   Release
Resource	   ${EXECDIR}/API/Resources/main.resource
Suite Setup    Abrir conexao  https://api.${server}.bancobari.com.br

*** Test Cases ***

Exibir resumo do investimento com detalhes da aplicação para CRI com taxa de IGPM
    [Tags]  backend  automation
    Given exibir_informacoes_de_investimentos_referente_ao_resgate_igpm
    When consulta_pelo_id_para_obter_detalhamento_do_resgate
    And exibir_detalhamento_sobre_o_papel_selecionado
    Then response_statuscode_200

Exibir resumo do investimento com detalhes da aplicação para CRI com taxa de IPCA
    [Tags]  backend  automation
    Given exibir_informacoes_de_investimentos_referente_ao_resgate_ipca
    When consulta_pelo_id_para_obter_informacoes_de_detalhamento_do_resgate
    And exibir_detalhamento_sobre_o_papel_selecionado_para_resgate
    Then response_status_200

*** Variable ***
${cpf}=  08130385961
${senha}=  123Abc

*** Keywords ***

# Case 01 (IGPM):

exibir_informacoes_de_investimentos_referente_ao_resgate_igpm
    ${guid}  Gera Guid 
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${itemsPerPage}  Set Variable  2
    ${Page}  Set Variable  0
    ${type}  Set Variable  false
    ${RESPOSTAREQUEST}  Redemption Investments  ${guid}  ${token}  ${itemsPerPage}  ${Page}  ${type}  

consulta_pelo_id_para_obter_detalhamento_do_resgate
    ${custodyId}  Set Variable   ${RESPOSTAREQUEST.json()}
    ${id}  Set Variable  ${custodyId['items'][0]['id']}
    Set Test Variable  ${id}
    ${RESPOSTAREQUEST}  View Details Investments Redemption  ${guid}  ${token}  ${id}

exibir_detalhamento_sobre_o_papel_selecionado
    Log  ${RESPOSTAREQUEST.json()}
    ${resultItem}  Set Variable   ${RESPOSTAREQUEST.json()}
    ${resultDescription}  Set Variable  ${resultItem['description']}
    ${resultLiquidity}  Set Variable  ${resultItem['liquidity']}
    ${resultProfitability}  Set Variable  ${resultItem['profitability']}
    
    Log  ${resultDescription}
    Log  ${resultLiquidity}
    Log  ${resultProfitability}

response_statuscode_200
    ${STATUSCODE_DESEJADO}  Set Variable   200
    Log  ${RESPOSTAREQUEST}
    Should Be Equal As Strings  ${RESPOSTAREQUEST.status_code}  ${STATUSCODE_DESEJADO}


# Case 02 (IPCA):

exibir_informacoes_de_investimentos_referente_ao_resgate_ipca
    ${guid}  Gera Guid 
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${itemsPerPage}  Set Variable  2
    ${Page}  Set Variable  0
    ${type}  Set Variable  false
    ${RESPOSTAREQUEST}  Redemption Investments  ${guid}  ${token}  ${itemsPerPage}  ${Page}  ${type}  

consulta_pelo_id_para_obter_informacoes_de_detalhamento_do_resgate
    ${custodyId}  Set Variable   ${RESPOSTAREQUEST.json()}
    ${id}  Set Variable  ${custodyId['items'][1]['id']}
    Set Test Variable  ${id}
    ${RESPOSTAREQUEST}  View Details Investments Redemption  ${guid}  ${token}  ${id}

exibir_detalhamento_sobre_o_papel_selecionado_para_resgate
    Log  ${RESPOSTAREQUEST.json()}
    ${resultItem}  Set Variable   ${RESPOSTAREQUEST.json()}
    ${resultDescription}  Set Variable  ${resultItem['description']}
    ${resultLiquidity}  Set Variable  ${resultItem['liquidity']}
    ${resultProfitability}  Set Variable  ${resultItem['profitability']}
    
    Log  ${resultDescription}
    Log  ${resultLiquidity}
    Log  ${resultProfitability}

response_status_200
    ${STATUSCODE_DESEJADO}  Set Variable   200
    Log  ${RESPOSTAREQUEST}
    Should Be Equal As Strings  ${RESPOSTAREQUEST.status_code}  ${STATUSCODE_DESEJADO}