*** Settings ***
Documentation
...            As como usuario do Banco Bari,
...            Want gostaria de ser notificado caso não tenha saldo suficiente no momento
...            exato da aplicação para Lydians e Virtual,
...            So poder realizar investimentos condizentes com meu saldo.
Default Tags   Release
Resource	   ${EXECDIR}/API/Resources/main.resource
Suite Setup    Abrir conexao  https://api.${server}.bancobari.com.br

*** Test Cases ***

Enviar valores de aplicações e verificação de saldo (Apply - CDB, Virtual)
    [Tags]  backend  automation
    Given consulta_por_papel_registrado_na_base_de_dados_(cdb)
    When enviar_informacoes_de_aplicacoes_apply_(cdb)
    Then response_devera_ter_status_de_400_(cdb)

Enviar valores de aplicações e verificação de saldo (Apply - LCI, Virtual)
    [Tags]  backend  automation
    Given consulta_por_papel_registrado_base_de_dados_(lci)
    When enviar_informacoes_de_aplicacoes_apply_(lci)
    Then response_status_de_400_(lci)

Enviar valores de aplicações e verificação de saldo (Apply - CDB, Lydians)
    [Tags]  backend  automation
    Given consulta_por_papel_registrado_na_base_de_dados
    When enviar_informacoes_de_aplicacoes_apply
    Then response_devera_ter_status_de_400


*** Variable ***
${cpf}=  08130385961
${senha}=  123Abc

*** Keywords ***

# Case 01 (Apply - CDB, Virtual):

consulta_por_papel_registrado_na_base_de_dados_(cdb)
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${type}  Set Variable  CDB
    ${RESPOSTAREQUEST}  List Of Papers  ${guid}  ${token}  ${type}

enviar_informacoes_de_aplicacoes_apply_(cdb)
    Log  ${RESPOSTAREQUEST.text} 
    Set Test Variable  ${Id}  ${RESPOSTAREQUEST.json()}
    Set Test Variable  ${Id}  ${Id['items'][1]['id']}
    ${RESPOSTAREQUEST}  Choose Investment Type  ${guid}  ${token}  ${Id}

    ${json_string}=  catenate

    ...	 {	
    ...   "type": "Apply",
    ...   "value": 100000,
    ...   "valueId": "${Id}"
    ...	 }
    
    Log  ${json_string}
    ${RESPOSTAREQUEST}  Shipping Transaction Value  ${guid}  ${token}  ${Id}  ${json_string}

response_devera_ter_status_de_400_(cdb)
    Log  ${RESPOSTAREQUEST.text}
    ${STATUSCODE_DESEJADO}  Set Variable  400
    Should Be Equal As Strings  ${RESPOSTAREQUEST.status_code}  ${STATUSCODE_DESEJADO}

verifica_balance_do_usuario_apply_include_(cdb)
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${RESPOSTAREQUEST}  Consult Balance User  ${guid}  ${token}


# Case 02 (Apply - LCI, Virtual):

consulta_por_papel_registrado_base_de_dados_(lci)
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${type}  Set Variable  LCI
    ${RESPOSTAREQUEST}  List Of Papers  ${guid}  ${token}  ${type}

enviar_informacoes_de_aplicacoes_apply_(lci)
    Log  ${RESPOSTAREQUEST.text} 
    Set Test Variable  ${Id}  ${RESPOSTAREQUEST.json()}
    Set Test Variable  ${Id}  ${Id['items'][1]['id']}
    ${RESPOSTAREQUEST}  Choose Investment Type  ${guid}  ${token}  ${Id}

    ${json_string}=  catenate

    ...	 {	
    ...   "type": "Apply",
    ...   "value": 20000,
    ...   "valueId": "${Id}"
    ...	 }
    
    Log  ${json_string}
    ${RESPOSTAREQUEST}  Shipping Transaction Value  ${guid}  ${token}  ${Id}  ${json_string}

response_status_de_400_(lci)
    Log  ${RESPOSTAREQUEST.text}
    ${STATUSCODE_DESEJADO}  Set Variable  400
    Should Be Equal As Strings  ${RESPOSTAREQUEST.status_code}  ${STATUSCODE_DESEJADO}


# Case 03 (Apply - CDB, Lydians):

consulta_por_papel_registrado_na_base_de_dados
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${type}  Set Variable  CDB
    ${RESPOSTAREQUEST}  List Of Papers  ${guid}  ${token}  ${type}

enviar_informacoes_de_aplicacoes_apply
    Log  ${RESPOSTAREQUEST.text} 
    Set Test Variable  ${Id}  ${RESPOSTAREQUEST.json()}
    Set Test Variable  ${Id}  ${Id['items'][0]['id']}
    ${RESPOSTAREQUEST}  Choose Investment Type  ${guid}  ${token}  ${Id}

    ${json_string}=  catenate

    ...	 {	
    ...   "type": "Apply",
    ...   "value": 10000,
    ...   "valueId": "${Id}"
    ...	 }
    
    Log  ${json_string}
    ${RESPOSTAREQUEST}  Shipping Transaction Value  ${guid}  ${token}  ${Id}  ${json_string}

response_devera_ter_status_de_400
    Log  ${RESPOSTAREQUEST.text}
    ${STATUSCODE_DESEJADO}  Set Variable  400
    Should Be Equal As Strings  ${RESPOSTAREQUEST.status_code}  ${STATUSCODE_DESEJADO}