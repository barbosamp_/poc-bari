*** Settings ***
Documentation
...            As usuário do aplicativo do Banco Bari,
...            Want gostaria de saber o saldo total das minhas aplicações,
...            So entender o rendimento do meu dinheiro.
Default Tags   Sprint31
Resource	   ${EXECDIR}/API/Resources/main.resource
Suite Setup    Abrir conexao  https://api.${server}.bancobari.com.br

*** Test Cases ***

Verificação do saldo do usuário
    [Tags]  backend  automation
    Given usuario_do_aplicativo_do_Banco_Bari_que_esta_na_tela_principal_de_investimento
    When sera_consultado_o_saldo_de_aplicacao_do_colaborador
    Then retorno_sera_apresentado_pelos_valores_de_Total_Bruto_Inicial_de_Investimentos_e_Rendimento

Verificação do saldo do usuário zerado
    [Tags]  backend  automation
    Given usuario_do_aplicativo_do_Banco_Bari_que_esta_no_dashboard_de_investimentos
    When sera_consultado_o_saldo_do_colaborador
    Then retorno_sera_apresentado_pelos_valores_zerados_para_Total_Bruto_Inicial_de_Investimentos_e_Rendimento

*** Variable ***
${cpf}=  08130385961
${senha}=  123Abc

*** Keywords ***

# Case 01 (Saldo):

usuario_do_aplicativo_do_Banco_Bari_que_esta_na_tela_principal_de_investimento
    Log  O usuário possui Conta e tem acesso a investimentos, com dados na tabela Barigui.Services.FeatureFlag_Customerscopes e Barigui.Services.Investment_Customer.
    ${hash256}  Encrypt String  ${cpf}
    ${Customerscopes}  Consulta Dynamo  Barigui.Services.FeatureFlag_Customerscopes  CustomerId  ${hash256}
    Log  ${Customerscopes}
    Log  ${Customerscopes[0]['Scopes'][3]}
    
    ${accountNumber}  Consulta Dynamo  Barigui.Services.Investment_Customer  CustomerId  ${cpf}
    Log  ${accountNumber}
    Log  ${accountNumber[0]['Account']}

sera_consultado_o_saldo_de_aplicacao_do_colaborador
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${RESPOSTAREQUEST}  Consult Balance User  ${guid}  ${token}

retorno_sera_apresentado_pelos_valores_de_Total_Bruto_Inicial_de_Investimentos_e_Rendimento
    ${STATUSCODE_DESEJADO}   Set Variable   200
    Log  ${RESPOSTAREQUEST}
    Log  ${RESPOSTAREQUEST.text}
    Should Be Equal As Strings    ${RESPOSTAREQUEST.status_code}    ${STATUSCODE_DESEJADO}


# Case 02 (Saldo zerado):

usuario_do_aplicativo_do_Banco_Bari_que_esta_no_dashboard_de_investimentos
    Log  O usuário possui Conta e tem acesso a investimentos, com dados na tabela Barigui.Services.FeatureFlag_Customerscopes (Flag: Investments).
    ${cpf}  Set Variable  08884857945
    ${hash256}  Encrypt String  ${cpf}
    ${Customerscopes}  Consulta Dynamo  Barigui.Services.FeatureFlag_Customerscopes  CustomerId  ${hash256}
    Log  ${Customerscopes}
    Log  ${Customerscopes[0]['Scopes'][2]}

    ${accountNumber}  Consulta Dynamo  Barigui.Services.Investment_Customer  CustomerId  ${cpf}
    Log  ${accountNumber}
    Log  ${accountNumber[0]['Account']}

sera_consultado_o_saldo_do_colaborador
    ${cpf}  Set Variable  10146495950
    ${senha}  Set Variable  123Abc
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${RESPOSTAREQUEST}  Consult Balance User  ${guid}  ${token}

retorno_sera_apresentado_pelos_valores_zerados_para_Total_Bruto_Inicial_de_Investimentos_e_Rendimento
    ${STATUSCODE_DESEJADO}   Set Variable   200
    Log  ${RESPOSTAREQUEST}
    Log  ${RESPOSTAREQUEST.text}
    Should Be Equal As Strings    ${RESPOSTAREQUEST.status_code}    ${STATUSCODE_DESEJADO}