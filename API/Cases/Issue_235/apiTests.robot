*** Settings ***
Documentation
...            As usuário do Banco Bari que possui investimentos disponíveis para resgate antes do vencimento,
...            Want visualizar todas as minhas aplicações que podem ser resgatadas antes do vencimento,
...            So decidir se quero realizar algum resgate.
Default Tags   Sprint33
Resource	   ${EXECDIR}/API/Resources/main.resource
Suite Setup    Abrir conexao  https://api.${server}.bancobari.com.br

*** Test Cases ***

Enviar valores de aplicações para investimento (Apply - CDB)
    [Tags]  backend  automation
    Given consulta_por_papel_registrado_na_base_de_dados
    When enviar_informacoes_de_aplicacoes_apply
    And response_devera_ter_status_de_204
    Then verifica_balance_do_usuario_apply_include_cdb
    And verifica_envio_da_nota_apply_cdb

Enviar valores de aplicações para investimento (Apply - LCI)
    [Tags]  backend  automation
    Given consulta_por_papel_registrado_base_de_dados
    When enviar_informacoes_de_aplicacoes_apply_lci
    And response_status_de_204
    Then verifica_balance_do_usuario_apply_include_lci
    And verifica_envio_da_nota_apply_lci

Enviar valores de resgate para investimento (Withdraw)
    [Tags]  backend  automation
    Given consulta_papel_para_resgate
    And exibir_id_do_papel_selecionado_para_resgate
    When enviar_informacoes_de_resgate_withdraw
    And response_status_204
    Then verifica_balance_do_usuario_withdraw
    And verifica_envio_da_nota_withdraw

Enviar valores de resgate para investimento (Response 400 - Apply)
    [Tags]  backend  automation
    Given consulta_papel_resgate
    When enviar_informacoes_de_aplicacoes_apply_400
    Then response_status_400

Enviar valores de resgate para investimento (Response 400 - Withdraw)
    [Tags]  backend  automation
    Given consulta_papel_resgate_withdraw
    And exibir_id_do_papel_selecionado_para_resgate_withdraw
    When enviar_informacoes_de_aplicacoes_withdraw_400
    Then response_devera_ter_status_400

*** Variable ***
${cpf}=  08130385961
${senha}=  123Abc

*** Keywords ***

# Case 01 (Apply - CDB):

consulta_por_papel_registrado_na_base_de_dados
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${type}  Set Variable  CDB
    ${RESPOSTAREQUEST}  List Of Papers  ${guid}  ${token}  ${type}

enviar_informacoes_de_aplicacoes_apply
    Log  ${RESPOSTAREQUEST.text} 
    Set Test Variable  ${Id}  ${RESPOSTAREQUEST.json()}
    Set Test Variable  ${Id}  ${Id['items'][0]['id']}
    ${RESPOSTAREQUEST}  Choose Investment Type  ${guid}  ${token}  ${Id}

    ${json_string}=  catenate

    ...	 {	
    ...   "type": "Apply",
    ...   "value": 120000,
    ...   "valueId": "${Id}"
    ...	 }
    
    Log  ${json_string}
    ${RESPOSTAREQUEST}  Shipping Transaction Value  ${guid}  ${token}  ${Id}  ${json_string}

response_devera_ter_status_de_204
    ${STATUSCODE_DESEJADO}  Set Variable  204
    Should Be Equal As Strings  ${RESPOSTAREQUEST.status_code}  ${STATUSCODE_DESEJADO}

verifica_balance_do_usuario_apply_include_cdb
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${RESPOSTAREQUEST}  Consult Balance User  ${guid}  ${token}

verifica_envio_da_nota_apply_cdb
    Sleep  10
    ${body}  Email Verification  no-reply@${server}.bancobari.com.br  Renda Fixa


# Case 02 (Apply - LCI):

consulta_por_papel_registrado_base_de_dados
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${type}  Set Variable  LCI
    ${RESPOSTAREQUEST}  List Of Papers  ${guid}  ${token}  ${type}

enviar_informacoes_de_aplicacoes_apply_lci
    Log  ${RESPOSTAREQUEST.text} 
    Set Test Variable  ${Id}  ${RESPOSTAREQUEST.json()}
    Set Test Variable  ${Id}  ${Id['items'][1]['id']}
    ${RESPOSTAREQUEST}  Choose Investment Type  ${guid}  ${token}  ${Id}

    ${json_string}=  catenate

    ...	 {	
    ...   "type": "Apply",
    ...   "value": 200000,
    ...   "valueId": "${Id}"
    ...	 }
    
    Log  ${json_string}
    ${RESPOSTAREQUEST}  Shipping Transaction Value  ${guid}  ${token}  ${Id}  ${json_string}

response_status_de_204
    ${STATUSCODE_DESEJADO}  Set Variable  204
    Should Be Equal As Strings  ${RESPOSTAREQUEST.status_code}  ${STATUSCODE_DESEJADO}

verifica_balance_do_usuario_apply_include_lci
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${RESPOSTAREQUEST}  Consult Balance User  ${guid}  ${token}

verifica_envio_da_nota_apply_lci
    Sleep  10
    ${body}  Email Verification  no-reply@${server}.bancobari.com.br  Renda Fixa


# Case 03 (Withdraw):

consulta_papel_para_resgate
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${itemsPerPage}  Set Variable  10
    ${Page}  Set Variable  12
    ${type}  Set Variable  true
    ${RESPOSTAREQUEST}  Redemption Investments  ${guid}  ${token}  ${itemsPerPage}  ${Page}  ${type}

exibir_id_do_papel_selecionado_para_resgate
    ${custodyId}  Set Variable  ${RESPOSTAREQUEST.json()}
    ${id}  Set Variable  ${custodyId['items'][0]['id']}
    Set Test Variable  ${id}

enviar_informacoes_de_resgate_withdraw

    ${json_string}=  catenate

    ...	 {	
    ...   "type": "Withdraw",
    ...   "value": 10000,
    ...   "valueId": "${id}"
    ...	 }

    Log  ${json_string}
    ${RESPOSTAREQUEST}  Shipping Transaction Value  ${guid}  ${token}  ${Id}  ${json_string}

response_status_204
    Sleep  10
    ${STATUSCODE_DESEJADO}   Set Variable  204
    Log  ${RESPOSTAREQUEST}
    Should Be Equal As Strings  ${RESPOSTAREQUEST.status_code}  ${STATUSCODE_DESEJADO}

verifica_balance_do_usuario_withdraw
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${RESPOSTAREQUEST}  Consult Balance User  ${guid}  ${token}

verifica_envio_da_nota_withdraw
    ${body}  Email Verification  no-reply@${server}.bancobari.com.br  Resgate Renda Fixa


# Case 04 (Validação do response 400 - Apply):

consulta_papel_resgate
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${type}  Set Variable  CDB
    ${RESPOSTAREQUEST}  List Of Papers  ${guid}  ${token}  ${type}

enviar_informacoes_de_aplicacoes_apply_400
    Log  ${RESPOSTAREQUEST.text} 
    Set Test Variable  ${Id}  ${RESPOSTAREQUEST.json()}
    Set Test Variable  ${Id}  ${Id['items'][0]['id']}
    ${RESPOSTAREQUEST}  Choose Investment Type  ${guid}  ${token}  ${Id}

    ${json_string}=  catenate

    ...	 {	
    ...   "type": "Apply",
    ...   "value": 100,
    ...   "valueId": "${Id}"
    ...	 }
    
    Log  ${json_string}
    ${RESPOSTAREQUEST}  Shipping Transaction Value  ${guid}  ${token}  ${Id}  ${json_string}

response_status_400
    ${STATUSCODE_DESEJADO}  Set Variable  400
    Log  ${RESPOSTAREQUEST.text}
    Should Be Equal As Strings  ${RESPOSTAREQUEST.status_code}  ${STATUSCODE_DESEJADO}


# Case 05 (Validação do response 400 - Withdraw): 

consulta_papel_resgate_withdraw
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${itemsPerPage}  Set Variable  2
    ${Page}  Set Variable  0
    ${type}  Set Variable  true
    ${RESPOSTAREQUEST}  Redemption Investments  ${guid}  ${token}  ${itemsPerPage}  ${Page}  ${type}

exibir_id_do_papel_selecionado_para_resgate_withdraw
    ${custodyId}  Set Variable  ${RESPOSTAREQUEST.json()}
    ${id}  Set Variable  ${custodyId['items'][0]['id']}
    Set Test Variable  ${id}

enviar_informacoes_de_aplicacoes_withdraw_400
    ${json_string}=  catenate

    ...	 {	
    ...   "type": "Withdraw",
    ...   "value": 500000000000,
    ...   "valueId": "${id}"
    ...	 }

    Log  ${json_string}
    ${RESPOSTAREQUEST}  Shipping Transaction Value  ${guid}  ${token}  ${Id}  ${json_string}

response_devera_ter_status_400
    ${STATUSCODE_DESEJADO}  Set Variable  400
    Log  ${RESPOSTAREQUEST.text}
    Should Be Equal As Strings  ${RESPOSTAREQUEST.status_code}  ${STATUSCODE_DESEJADO}