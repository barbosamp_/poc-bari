*** Settings ***
Documentation
...            As usuário do aplicativo do Banco Bari com acesso a área de Investimentos,
...            Want gostaria de saber mais informações sobre o papel escolhido,
...            So direcionado para a tela Detalhes do Investimento,
...            And poderá fazer uma aplicação no papel selecionado ou voltar para a lista de produtos.
Default Tags   Sprint32
Resource	   ${EXECDIR}/API/Resources/main.resource
Suite Setup    Abrir conexao  https://api.${server}.bancobari.com.br

*** Test Cases ***

Consulta do produto detail CDB
    [Tags]  backend  automation
    Given identificado_que_existe_tipo_de_investimento_CDB
    When escolhido_o_detalhamento_do_papel_selecionado_CDB
    Then apresenta_o_detalhamento_do_produto_escolhido_detail_CDB
    And response_devera_ter_status_200

Consulta do produto detail LCI
    [Tags]  backend  automation
    Given identificado_que_existe_tipo_de_investimento_LCI
    When escolhido_o_detalhamento_do_papel_selecionado_LCI
    Then apresenta_o_detalhamento_do_produto_escolhido_detail_LCI
    And response_status_200

Consulta do produto detail CRI
    [Tags]  backend  automation
    Given identificado_que_existe_tipo_de_investimento_CRI
    When escolhido_o_detalhamento_do_papel_selecionado_CRI
    Then apresenta_o_detalhamento_do_produto_escolhido_detail_CRI
    And response_status_igual_a_200

*** Variable ***
${cpf}=  09478206982
${senha}=  123Abc

*** Keywords ***

# Case 01 (CDB):

identificado_que_existe_tipo_de_investimento_CDB
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable   ${guid}
    Set Test Variable   ${token}
    ${type}  Set Variable   CDB
    ${RESPOSTAREQUEST}  List Of Papers  ${guid}  ${token}  ${type}

escolhido_o_detalhamento_do_papel_selecionado_CDB
    Log  ${RESPOSTAREQUEST.text} 
    Set Test Variable  ${Id}  ${RESPOSTAREQUEST.json()}
    Set Test Variable  ${Id}  ${Id['items'][0]['id']}
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${RESPOSTAREQUEST}  Choose Investment Type  ${guid}  ${token}  ${Id}

apresenta_o_detalhamento_do_produto_escolhido_detail_CDB
    Log  ${RESPOSTAREQUEST.text}
    Set Test Variable  ${result}  ${RESPOSTAREQUEST.json()}
    Set Test Variable  ${descriptionPaper}  ${result['description']}
    Set Test Variable  ${resultLiquity}  ${result['liquidity']}
    Set Test Variable  ${resultLiquity}  ${result['profitability']}
    Set Test Variable  ${resultLiquity}  ${result['paperVendor']}

response_devera_ter_status_200
    ${STATUSCODE_DESEJADO}  Set Variable  200
    Log  ${RESPOSTAREQUEST}
    Should Be Equal As Strings  ${RESPOSTAREQUEST.status_code}  ${STATUSCODE_DESEJADO}


# Case 02 (LCI):

identificado_que_existe_tipo_de_investimento_LCI
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable   ${guid}
    Set Test Variable   ${token}
    ${type}  Set Variable   LCI
    ${RESPOSTAREQUEST}  List Of Papers  ${guid}  ${token}  ${type}

escolhido_o_detalhamento_do_papel_selecionado_LCI
    Log  ${RESPOSTAREQUEST.text} 
    Set Test Variable  ${Id}  ${RESPOSTAREQUEST.json()}
    Set Test Variable  ${Id}  ${Id['items'][0]['id']}
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${RESPOSTAREQUEST}  Choose Investment Type  ${guid}  ${token}  ${Id}

apresenta_o_detalhamento_do_produto_escolhido_detail_LCI
    Log  ${RESPOSTAREQUEST.text}
    Set Test Variable  ${result}  ${RESPOSTAREQUEST.json()}
    Set Test Variable  ${descriptionPaper}  ${result['description']}
    Set Test Variable  ${resultLiquity}  ${result['liquidity']}
    Set Test Variable  ${resultLiquity}  ${result['profitability']}
    Set Test Variable  ${resultLiquity}  ${result['paperVendor']}

response_status_200
    ${STATUSCODE_DESEJADO}  Set Variable  200
    Log  ${RESPOSTAREQUEST}
    Should Be Equal As Strings  ${RESPOSTAREQUEST.status_code}  ${STATUSCODE_DESEJADO}


# Case 03 (CRI):

identificado_que_existe_tipo_de_investimento_CRI
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable   ${guid}
    Set Test Variable   ${token}
    ${type}  Set Variable   CRI
    ${RESPOSTAREQUEST}  List Of Papers  ${guid}  ${token}  ${type}

escolhido_o_detalhamento_do_papel_selecionado_CRI
    Log  ${RESPOSTAREQUEST.text} 
    Set Test Variable  ${Id}  ${RESPOSTAREQUEST.json()}
    Set Test Variable  ${Id}  ${Id['items'][0]['id']}
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${RESPOSTAREQUEST}  Choose Investment Type  ${guid}  ${token}  ${Id}

apresenta_o_detalhamento_do_produto_escolhido_detail_CRI
    Log  ${RESPOSTAREQUEST.text}
    Set Test Variable  ${result}  ${RESPOSTAREQUEST.json()}
    Set Test Variable  ${descriptionPaper}  ${result['description']}
    Set Test Variable  ${resultLiquity}  ${result['liquidity']}
    Set Test Variable  ${resultLiquity}  ${result['profitability']}
    Set Test Variable  ${resultLiquity}  ${result['paperVendor']}

response_status_igual_a_200
    ${STATUSCODE_DESEJADO}  Set Variable  200
    Log  ${RESPOSTAREQUEST}
    Should Be Equal As Strings  ${RESPOSTAREQUEST.status_code}  ${STATUSCODE_DESEJADO}