*** Settings ***
Documentation
...            As como usuario do Banco Bari,
...            Want gostaria poder realizar a recompra dos meus papeis sem liquidez
...            So receber o dinheiro com rendimento nesses papeis.
Default Tags   Release
Resource	   ${EXECDIR}/API/Resources/main.resource
Suite Setup    Abrir conexao  https://api.${server}.bancobari.com.br

*** Test Cases ***

Enviar recompra de papeis sem liquidez
    [Tags]  backend  automation
    Given enviar_informacoes_de_recompra_para_papeis_sem_liquidez
    When response_devera_ter_status_de_204
    Then registro_de_solicitacao_efetuada_na_tabela
    And verificacao_email_nota_solicitada

Verificar situação da recompra já solicitada (48h)
    [Tags]  backend  automation
    Given identificado_solicitacao_ja_existente_em_registro
    When verificacao_do_customers_profile
    Then validacao_do_parametro_earlyWithdrawAllowed


*** Variable ***
${cpf}=  08130385961
${senha}=  123Abc

*** Keywords ***

# Case 01 (Early Withdraw):

enviar_informacoes_de_recompra_para_papeis_sem_liquidez
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    ${Id}  Set Variable   
    Set Test Variable  ${guid}
    Set Test Variable  ${token}

    ${json_string}=  catenate

    ...	 {	
    ...   "type": "EarlyWithdraw",
    ...   "value": 174426,
    ...   "valueId": "${Id}"
    ...	 }
    
    Log  ${json_string}
    ${RESPOSTAREQUEST}  Shipping Transaction Value  ${guid}  ${token}  ${Id}  ${json_string}

response_devera_ter_status_de_204
    Log  ${RESPOSTAREQUEST}
    ${STATUSCODE_DESEJADO}  Set Variable  204
    Should Be Equal As Strings  ${RESPOSTAREQUEST.status_code}  ${STATUSCODE_DESEJADO}

registro_de_solicitacao_efetuada_na_tabela
    ${Customerscopes}  Consulta Dynamo  Barigui.Services.Investment_EarlyWithdrawRequest  CustomerId  ${cpf}
    Log  ${Customerscopes}
    Log  ${Customerscopes[0]['Id']}
    Log  ${Customerscopes[0]['CustomerId']}

verificacao_email_nota_solicitada
    Sleep  08
    ${body}  Email Verification  no-reply@qa.bancobari.com.br  Foi solicitada recompra antecipada.


# Case 02 (Early Withdraw Pending):

identificado_solicitacao_ja_existente_em_registro
    ${Customerscopes}  Consulta Dynamo  Barigui.Services.Investment_EarlyWithdrawRequest  CustomerId  ${cpf}
    Log  ${Customerscopes}
    Log  ${Customerscopes[0]['Id']}
    Log  ${Customerscopes[0]['CustomerId']}

verificacao_do_customers_profile
    ${guid}  Gera Guid 
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${RESPOSTAREQUEST}  Customers Profile  ${guid}  ${token}

validacao_do_parametro_earlyWithdrawAllowed
    Log  ${RESPOSTAREQUEST.json()}
    ${resultEarlyWithdraw}  Set Variable   ${RESPOSTAREQUEST.json()}
    ${resultDescription}  Set Variable  ${resultEarlyWithdraw['earlyWithdrawDeniedDescription']}
    ${result}  Set Variable  ${resultEarlyWithdraw['earlyWithdrawIsAllowed']}
    Should Be Equal As Strings  ${result}  False
    Log  ${resultDescription}
    Log  ${result}