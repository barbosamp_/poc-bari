*** Settings ***
Documentation
...            As usuário do aplicativo do Banco Bari com acesso a área de Investimentos,
...            Want gostaria de receber um aviso sobre "horário limite pra aplicação excedido" para papeis da Virtual,
...            And saber diferenciar quais os horários de papeis primários e secundários,
...            So para poder programar meus investimentos.
Default Tags   Release
Resource	   ${EXECDIR}/API/Resources/main.resource
Suite Setup    Abrir conexao  https://api.${server}.bancobari.com.br

*** Test Cases ***

Janela de investimentos Virtual para papeis primários (LCI)
    [Tags]  backend  automation

    Given alterado_parametros_de_horario_na_base_referente_a_janela
    And identificado_que_existe_tipo_de_investimento_LCI
    When escolhido_o_detalhamento_do_papel_selecionado_LCI
    Then apresenta_o_detalhamento_do_produto_escolhido_detail_LCI
    And response_status_200

Janela de investimentos Virtual para papeis primários (CDB)
    [Tags]  backend  automation
    Given identificado_que_existe_tipo_de_investimento_CDB
    When escolhido_o_detalhamento_do_papel_selecionado_CDB
    And apresenta_o_detalhamento_do_produto_escolhido_detail_CDB
    Then response_status_igual_a_200
    And retorno_dos_parametros_de_horario_na_base_referente_a_janela

*** Variable ***
${cpf}=  08130385961
${senha}=  123Abc

*** Keywords ***

# Case 01 (Assets, primário - LCI):

alterado_parametros_de_horario_na_base_referente_a_janela
    Update WindowTransactionVirtual  E05CD577-988F-41CE-97AB-B9745E2E3597
    
identificado_que_existe_tipo_de_investimento_LCI
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable   ${guid}
    Set Test Variable   ${token}
    ${type}  Set Variable   LCI
    ${RESPOSTAREQUEST}  List Of Papers  ${guid}  ${token}  ${type}

escolhido_o_detalhamento_do_papel_selecionado_LCI
    Log  ${RESPOSTAREQUEST.text} 
    Set Test Variable  ${Id}  ${RESPOSTAREQUEST.json()}
    Set Test Variable  ${Id}  ${Id['items'][0]['id']}
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${RESPOSTAREQUEST}  Choose Investment Type  ${guid}  ${token}  ${Id}

apresenta_o_detalhamento_do_produto_escolhido_detail_LCI
    Log  ${RESPOSTAREQUEST.text}
    Set Test Variable  ${result}  ${RESPOSTAREQUEST.json()}
    Set Test Variable  ${descriptionPaper}  ${result['description']}
    Set Test Variable  ${resultLiquity}  ${result['liquidity']}
    Set Test Variable  ${resultLiquity}  ${result['profitability']}
    Set Test Variable  ${resultLiquity}  ${result['transactionWindowDescription']}

response_status_200
    ${STATUSCODE_DESEJADO}  Set Variable  200
    Log  ${RESPOSTAREQUEST}
    Should Be Equal As Strings  ${RESPOSTAREQUEST.status_code}  ${STATUSCODE_DESEJADO}


# Case 02 (Assets, primário - CDB):

identificado_que_existe_tipo_de_investimento_CDB
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable   ${guid}
    Set Test Variable   ${token}
    ${type}  Set Variable   CDB
    ${RESPOSTAREQUEST}  List Of Papers  ${guid}  ${token}  ${type}

escolhido_o_detalhamento_do_papel_selecionado_CDB
    Log  ${RESPOSTAREQUEST.text} 
    Set Test Variable  ${Id}  ${RESPOSTAREQUEST.json()}
    Set Test Variable  ${Id}  ${Id['items'][2]['id']}
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${RESPOSTAREQUEST}  Choose Investment Type  ${guid}  ${token}  ${Id}

apresenta_o_detalhamento_do_produto_escolhido_detail_CDB
    Log  ${RESPOSTAREQUEST.text}
    Set Test Variable  ${result}  ${RESPOSTAREQUEST.json()}
    Set Test Variable  ${descriptionPaper}  ${result['description']}
    Set Test Variable  ${resultLiquity}  ${result['liquidity']}
    Set Test Variable  ${resultLiquity}  ${result['profitability']}
    Set Test Variable  ${resultLiquity}  ${result['transactionWindowDescription']}

response_status_igual_a_200
    ${STATUSCODE_DESEJADO}  Set Variable  200
    Log  ${RESPOSTAREQUEST}
    Should Be Equal As Strings  ${RESPOSTAREQUEST.status_code}  ${STATUSCODE_DESEJADO}

retorno_dos_parametros_de_horario_na_base_referente_a_janela
    Update ResetWindowTransactionVirtual  E05CD577-988F-41CE-97AB-B9745E2E3597