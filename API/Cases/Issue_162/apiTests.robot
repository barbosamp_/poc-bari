*** Settings ***
Documentation
...            As usuário já escolheu um investimento definindo o valor que será aplicado,
...            Want será direcionado para a tela de Resumo do Investimento,
...            So ao confirmar a aplicação, investimento será realizado e nota será enviada.
Default Tags   Sprint32
Resource	   ${EXECDIR}/API/Resources/main.resource
Suite Setup    Abrir conexao  https://api.${server}.bancobari.com.br

*** Test Cases ***

Envio das informações de opções de investimento
    [Tags]  backend  automation
    Given identificado_que_existe_tipo_de_investimento
    When envio_do_value_conforme_o_tipo_do_investimento_identificado
    Then response_devera_ser_204
    And verifica_envio_da_nota
    And sera_registrado_o_value_inserido
    And verifica_balance_do_usuario

Verificação do reenvio das informações dos guids registrados na base
    [Tags]  backend  automation
    Given identificado_que_existe_tipo_investimento
    When envio_do_value_conforme_o_tipo_do_investimento_com_saldo_insuficiente_ou_menor
    Then response_devera_ser_400

*** Keywords ***

# Observação Importante: Essa API será substituida pela /v1/transaction.

# Case 01 (Valor aplicado pela Apply):

identificado_que_existe_tipo_de_investimento
    ${guid}         Gera Guid
    ${token}        Gera Token Investimentos
    Set Test Variable   ${guid}
    Set Test Variable   ${token}
    ${type}  Set Variable   CDB
    ${RESPOSTAREQUEST}       List Of Papers    ${guid}     ${token}     ${type}

envio_do_value_conforme_o_tipo_do_investimento_identificado
    Log     ${RESPOSTAREQUEST.text} 
    Set Test Variable  ${Id}   ${RESPOSTAREQUEST.json()}
    Set Test Variable  ${Id}   ${Id['items'][2]['id']}
    ${RESPOSTAREQUEST}       Choose Investment Type    ${guid}     ${token}      ${Id}

   ${json_string}=  catenate

    ...   {
    ...    "value": 100000,
    ...    "operationId": "${guid}"
    ...   }

    log  ${json_string}
    ${RESPOSTAREQUEST}     Shipping Amount Apply    ${guid}     ${token}     ${Id}      ${json_string}

response_devera_ser_204
    ${STATUSCODE_DESEJADO}   Set Variable   204
    Log     ${RESPOSTAREQUEST}
    Should Be Equal As Strings    ${RESPOSTAREQUEST.status_code}    204

verifica_envio_da_nota
    ${body}   Email Verification   no-reply@qa.bancobari.com.br   Renda Fixa

sera_registrado_o_value_inserido
    ${idGuid}   Consulta Dynamo     Barigui.Services.Investment_Transaction  Id    ${guid}
    log  ${idGuid}
    Should Be Equal as Strings  ${idGuid[0]['Id']}  ${guid}

verifica_balance_do_usuario
    Set Test Variable   ${guid}
    Set Test Variable   ${token}
    ${RESPOSTAREQUEST}       Consult Balance User    ${guid}     ${token}


# Case 02 (Saldo menor ou insuficiente):

identificado_que_existe_tipo_investimento
    ${guid}         Gera Guid
    ${token}        Gera Token Investimentos
    Set Test Variable   ${guid}
    Set Test Variable   ${token}
    ${type}  Set Variable   CDB
    ${RESPOSTAREQUEST}       List Of Papers    ${guid}     ${token}     ${type}

envio_do_value_conforme_o_tipo_do_investimento_com_saldo_insuficiente_ou_menor
    Log     ${RESPOSTAREQUEST.text} 
    Set Test Variable  ${Id}   ${RESPOSTAREQUEST.json()}
    Set Test Variable  ${Id}   ${Id['items'][0]['id']}
    ${RESPOSTAREQUEST}       Choose Investment Type    ${guid}     ${token}      ${Id}

   ${json_string}=  catenate

    ...   {
    ...    "value": 100000,
    ...    "operationId": "${guid}"
    ...   }

    log  ${json_string}
    ${RESPOSTAREQUEST}     Shipping Amount Apply    ${guid}     ${token}     ${Id}       ${json_string}

response_devera_ser_400
    ${STATUSCODE_DESEJADO}   Set Variable   400
    Log     ${RESPOSTAREQUEST}
    Log     ${RESPOSTAREQUEST.text}
    Should Be Equal As Strings    ${RESPOSTAREQUEST.status_code}    400