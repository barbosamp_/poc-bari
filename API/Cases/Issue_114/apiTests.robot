*** Settings ***
Documentation
...            As usuário do aplicativo do Banco Bari,
...            Want primeira vez de acesso a área de Investimento,
...            So irá apresentar a Introdução de Suitability,
...            And direcionar colaborador para realização do teste.
Default Tags   Sprint31
Resource	   ${EXECDIR}/API/Resources/main.resource
Suite Setup    Abrir conexao  https://api.${server}.bancobari.com.br

*** Test Cases ***

Usuário realizou o teste de Suitability_Conservador
    [Tags]  backend  automation
    Given usuario_realizou_o_teste_de_suitability_conservador
    When preencheu_todas_respostas_conforme_solicitado_conservador
    Then response_devera_ter_status_200_para_conservador
    And verificado_envio_informacoes_suitability_na_base_de_dados_moderado

Usuário realizou o teste de Suitability_Moderado
    [Tags]  backend  automation
    Given usuario_realizou_o_teste_de_suitability_moderado
    When preencheu_todas_respostas_conforme_solicitado_moderado
    Then response_devera_ter_status_200_para_moderado
    And verificado_envio_informacoes_suitability_na_base_de_dados_moderado

Usuário realizou o teste de Suitability_Agressivo
    [Tags]  backend  automation
    Given usuario_realizou_o_teste_de_suitability_agressivo
    When preencheu_todas_respostas_conforme_solicitado_agressivo
    Then response_devera_ter_status_200_para_agressivo
    And verificado_envio_informacoes_suitability_na_base_de_dados_moderado

Usuário não realizou o teste de Suitability
    [Tags]  backend  automation
    Given usuario_iniciou_o_teste_de_Suitability
    When nao_preencheu_todas_respostas_conforme_solicitado
    Then response_devera_ter_status_400


*** Variable ***
${cpf}=  08130385961
${senha}=  123Abc

*** Keywords ***

# Case 01 (Conservador):

usuario_realizou_o_teste_de_suitability_conservador
    Log  Usuário tem acesso a Account e Investments e consta na tabela Barigui.Services.Investiment_CustomerSuitability após conclusão do teste.

preencheu_todas_respostas_conforme_solicitado_conservador
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable   ${guid}
    Set Test Variable   ${token}

   ${json_string}=  catenate

    ...  {
    ...     "questions": [
    ...      	{
    ...         "id": "AssetIncrease",
    ...         "value": "Secure"
    ...      	},
    ...      	{
    ...         "id": "AssetTerm",
    ...         "value": "LesThanOneYear"
    ...      	},
    ...      	{
    ...         "id": "StrategyOnMarketFluctuation",
    ...         "value": "Sell"
    ...      	},
    ...      	{
    ...         "id": "AssetWithdrawalNeed",
    ...         "value": "Undefined"
    ...      	},
    ...      	{
    ...         "id": "EducationLevel",
    ...         "value": "UncompletedDegree"
    ...      	},
    ...      	{
    ...         "id": "AssetsKnowledgement",
    ...         "subquestions": [
    ...         {
    ...             "id": "ProductsOrFixedIncome",
    ...             "value": "Never"
    ...         },
    ...         {
    ...             "id": "MultimarketFunds",
    ...             "value": "Never"
    ...         },
    ...         {
    ...             "id": "StockOrFunds",
    ...             "value": "Never"
    ...         },
    ...         {
    ...             "id": "CRIOrCRA",
    ...             "value": "Never"
    ...         },
    ...         {
    ...             "id": "LCIOrLCA",
    ...             "value": "Never"
    ...         }
    ...         ]
    ...      	},
    ...      	{
    ...       "id": "InvestingExperience",
    ...       "value": "Low"
    ...      }
    ...    ]
    ...  }
    
    Log  ${json_string}
    ${RESPOSTAREQUEST}  Suitability Shipping  ${guid}  ${token}  ${json_string}
    
response_devera_ter_status_200_para_conservador
    ${STATUSCODE_DESEJADO}   Set Variable   200
    Log  ${RESPOSTAREQUEST}
    Log  ${RESPOSTAREQUEST.text}
    Should Be Equal As Strings  ${RESPOSTAREQUEST.status_code}  200

verificado_envio_informacoes_suitability_na_base_de_dados_conservador
    ${registerSuitability}  Consulta Dynamo  Barigui.Services.Investment_CustomerSuitability  CustomerId  ${cpf}
    Log  ${registerSuitability}


# Case 02 (Moderado):

usuario_realizou_o_teste_de_suitability_moderado
    Log  Usuário tem acesso a Account e Investments e consta na tabela Barigui.Services.Investiment_CustomerSuitability após conclusão do teste.

preencheu_todas_respostas_conforme_solicitado_moderado
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable   ${guid}
    Set Test Variable   ${token}

   ${json_string}=  catenate

    ...		{
    ...  	"questions": [
    ...    		{
    ...      	"id": "AssetIncrease",
    ...      	"value": "Hazardous"
    ...    		},
    ...			{
    ...      	"id": "AssetTerm",
    ...      	"value": "LesThanOneYear"
    ...    		},
    ...			{
    ...      	"id": "StrategyOnMarketFluctuation",
    ...      	"value": "Buy"
    ...   	 	},
    ...			{
    ...      	"id": "AssetWithdrawalNeed",
    ...      	"value": "Undefined"
    ...    		},
    ...			{
    ...      	"id": "EducationLevel",
    ...      	"value": "HighSchool"
    ...    		},
    ...			{
    ...			"id": "AssetsKnowledgement",
    ...			"subquestions": [
    ...			{
    ...				"id": "ProductsOrFixedIncome",
    ...				"value": "Never"
    ...			},
    ...			{
    ...				"id": "MultimarketFunds",
    ...				"value": "Never"
    ...			},
    ...			{
    ...				"id": "StockOrFunds",
    ...				"value": "Never"
    ...			},
    ...			{
    ...				"id": "CRIOrCRA",
    ...				"value": "Never"
    ...			},
    ...			{
    ...				"id": "LCIOrLCA",
    ...				"value": "Never"
    ...			}
    ...			]
    ...			},
    ...			{
    ...      "id": "InvestingExperience",
    ...      "value": "Low"
    ...      }
    ...    ]
    ...	 }
    
    Log  ${json_string}
    ${RESPOSTAREQUEST}  Suitability Shipping  ${guid}  ${token}  ${json_string}
    
response_devera_ter_status_200_para_moderado
    ${STATUSCODE_DESEJADO}  Set Variable  200
    Log  ${RESPOSTAREQUEST}
    Log  ${RESPOSTAREQUEST.text}
    Should Be Equal As Strings  ${RESPOSTAREQUEST.status_code}    200

verificado_envio_informacoes_suitability_na_base_de_dados_moderado
    ${registerSuitability}  Consulta Dynamo  Barigui.Services.Investment_CustomerSuitability  CustomerId  ${cpf}
    Log  ${registerSuitability}


# Case 03 (Agressivo):

usuario_realizou_o_teste_de_suitability_agressivo
    Log  Usuário tem acesso a Account e Investments e consta na tabela Barigui.Services.Investiment_CustomerSuitability após conclusão do teste.

preencheu_todas_respostas_conforme_solicitado_agressivo
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable   ${guid}
    Set Test Variable   ${token}

   ${json_string}=  catenate

	...     {
    ...     "questions": [
    ...         {
    ...         "id": "AssetIncrease",
    ...         "value": "Hazardous"
    ...         },
    ...         {
    ...         "id": "AssetTerm",
    ...         "value": "LesThanOneYear"
    ...         },
    ...         {
    ...         "id": "StrategyOnMarketFluctuation",
    ...         "value": "Buy"
    ...         },
    ...         {
    ...         "id": "AssetWithdrawalNeed",
    ...         "value": "Undefined"
    ...         },
    ...         {
    ...         "id": "EducationLevel",
    ...         "value": "PosDegree"
    ...         },
    ...         {
    ...         "id": "AssetsKnowledgement",
    ...         "subquestions": [
    ...         {
    ...             "id": "ProductsOrFixedIncome",
    ...             "value": "Never"
    ...         },
    ...         {
    ...             "id": "MultimarketFunds",
    ...             "value": "Never"
    ...         },
    ...         {
    ...             "id": "StockOrFunds",
    ...             "value": "Never"
    ...         },
    ...         {
    ...             "id": "CRIOrCRA",
    ...             "value": "Never"
    ...         },
    ...         {
    ...             "id": "LCIOrLCA",
    ...             "value": "Never"
    ...         }
    ...         ]
    ...         },
    ...         {
    ...        "id": "InvestingExperience",
    ...        "value": "High"
    ...        }
    ...     ]
    ...  }
    
    Log  ${json_string}
    ${RESPOSTAREQUEST}  Suitability Shipping  ${guid}  ${token}  ${json_string}

response_devera_ter_status_200_para_agressivo
    ${STATUSCODE_DESEJADO}   Set Variable   200
    Log  ${RESPOSTAREQUEST}
    Log  ${RESPOSTAREQUEST.text}
    Should Be Equal As Strings  ${RESPOSTAREQUEST.status_code}    200

verificado_envio_informacoes_suitability_na_base_de_dados_agressivo
    ${registerSuitability}  Consulta Dynamo  Barigui.Services.Investment_CustomerSuitability  CustomerId  ${cpf}
    Log  ${registerSuitability}


# Case 04 (Informações pendentes no envio - CPF sem Suitability):

usuario_iniciou_o_teste_de_Suitability
    Log  Usuário tem acesso a Account e Investments, mas não respondeu questionário de Suitability por completo.

nao_preencheu_todas_respostas_conforme_solicitado
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable   ${guid}
    Set Test Variable   ${token}

   ${json_string}=  catenate

 	...  {
    ...     "questions": [
    ...         {
    ...         "id": "AssetIncrease",
    ...         "value": "Hazardous"
    ...         },
    ...         {
    ...         "id": "AssetTerm",
    ...         "value": "LesThanOneYear"
    ...         },
    ...         {
    ...         "id": "StrategyOnMarketFluctuation",
    ...         "value": "Buy"
    ...         },
    ...         {
    ...         "id": "AssetWithdrawalNeed",
    ...         "value": "Undefined"
    ...         },
    ...         {
    ...         "id": "EducationLevel",
    ...         "value": "PosDegree"
    ...         }
    ...     ]
    ...  }

    Log  ${json_string}
    ${RESPOSTAREQUEST}  Suitability Shipping  ${guid}  ${token}  ${json_string}

response_devera_ter_status_400
    ${STATUSCODE_DESEJADO}  Set Variable  400
    Log  ${RESPOSTAREQUEST}
    Log  ${RESPOSTAREQUEST.text}
    Should Be Equal As Strings  ${RESPOSTAREQUEST.status_code}  400