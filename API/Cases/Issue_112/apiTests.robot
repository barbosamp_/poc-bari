*** Settings ***
Documentation
...            As usuário com conta ativa no aplicativo do Banco Bari,
...            Want acionar no CTA Fazer Teste que está na tela de Introdução ao Suitability,
...            So terá que responder todas as perguntas do teste, que são um total de 07 perguntas,
...            And para que o seu perfil de investidor seja identificado.
Default Tags   Sprint31
Resource	   ${EXECDIR}/API/Resources/main.resource
Suite Setup    Abrir conexao  https://api.${server}.bancobari.com.br

*** Test Cases ***

Verificação do envio das informações preenchidas no questionário de Suitability
    [Tags]  backend  automation
    Given usuario_com_conta_ativa_com_acesso_a_investimentos_no_aplicativo_do_Banco_Bari 
    When enviou_todo_questionario_preenchido_com_as_informacoes_das_sete_perguntas
    Then retorno_do_response_tera_status_de_200

Verificação do não envio das informações preenchidas no questionário de Suitability
    [Tags]  backend  automation
    Given usuario_conta_ativa_com_acesso_a_investimentos_no_aplicativo_do_Banco_Bari 
    When nao_enviou_todo_questionario_preenchido_com_as_informacoes_das_sete_perguntas
    Then retorno_do_response_tera_status_de_204

*** Variable ***
${cpf}=  08130385961
${senha}=  123Abc

*** Keywords ***

# Case 01 (Envio realizado):

usuario_com_conta_ativa_com_acesso_a_investimentos_no_aplicativo_do_Banco_Bari
    Log  O usuário possui Conta e tem acesso a investimentos, com dados na tabela Barigui.Services.FeatureFlag_Customerscopes (Flag: Investments).
    ${hash256}  Encrypt String  ${cpf}
    ${Customerscopes}  Consulta Dynamo  Barigui.Services.FeatureFlag_Customerscopes  CustomerId  ${hash256}
    Log  ${Customerscopes}
    Log  ${Customerscopes[0]['Scopes'][3]}

    ${accountNumber}  Consulta Dynamo  Barigui.Services.Investment_Customer  CustomerId  ${cpf}
    Log  ${accountNumber}
    Log  ${accountNumber[0]['Account']}

enviou_todo_questionario_preenchido_com_as_informacoes_das_sete_perguntas
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable   ${guid}
    Set Test Variable   ${token}
    ${RESPOSTAREQUEST}  Result Suitability User  ${guid}  ${token}

retorno_do_response_tera_status_de_200
    ${STATUSCODE_DESEJADO}   Set Variable   200
    Log  ${RESPOSTAREQUEST}
    Log  ${RESPOSTAREQUEST.text}
    Should Be Equal As Strings  ${RESPOSTAREQUEST.status_code}  ${STATUSCODE_DESEJADO}


# Case 02 (Enviado não realizado):

usuario_conta_ativa_com_acesso_a_investimentos_no_aplicativo_do_Banco_Bari 
    Log  O usuário é um Customer, possui Conta e tem acesso a Investimentos.

nao_enviou_todo_questionario_preenchido_com_as_informacoes_das_sete_perguntas
    ${cpf}  Set Variable  08884857945
    ${senha}  Set Variable  123Abc
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable   ${guid}
    Set Test Variable   ${token}
    ${RESPOSTAREQUEST}  Result Suitability User  ${guid}   ${token}

retorno_do_response_tera_status_de_204
    ${STATUSCODE_DESEJADO}  Set Variable  204
    Log  ${RESPOSTAREQUEST}
    Log  ${RESPOSTAREQUEST.text}
    Should Be Equal As Strings  ${RESPOSTAREQUEST.status_code}  ${STATUSCODE_DESEJADO}
