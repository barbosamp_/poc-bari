*** Settings ***
Documentation
...            As usuário do Banco Bari que já realizou um resgate ou mais,
...            Want acessar os detalhes da aplicação que quer visualizar o valor que já foi resgatado,
...            So entender o saldo da aplicação.
Default Tags   Release
Resource	   ${EXECDIR}/API/Resources/main.resource
Suite Setup    Abrir conexao  https://api.${server}.bancobari.com.br

*** Test Cases ***

Exibição de informações (history) para mais de um resgate realizado referente a custody
    [Tags]  backend  automation
    Given consulta_do_papel_com_possibilidade_de_resgate
    When exibir_id_para_selecao_de_history_referente_a_custody_com_mais_de_um_resgate_parcial
    Then verificar_informacoes_de_resgates_ja_realizados_para_determinado_custody
    And exibir_informacoes_de_historys
    And response_statuscode_200

Exibição de informação (history) para apenas um resgate realizado referente a custody
    [Tags]  backend  automation
    Given consulta_do_papel_com_possibilidade_resgate
    When exibir_id_para_selecao_de_history_referente_a_custody_com_apenas_um_resgate_parcial
    Then verificar_informacao_de_resgate_realizado_para_determinado_custody
    And exibir_informacoes_de_history_detail
    And response_statuscd_200

Exibição de lista vazia para history custody
    [Tags]  backend  automation
    Given consulta_do_papel_para_resgate
    When exibir_id_para_selecao_de_history_referente_a_custody_sem_valores
    Then verificar_valores_zerados_para_determinado_custody
    And response_listavazia_statuscode_200

Validação para inserção de custody (id) inválido
    [Tags]  backend  automation
    Given inserir_id_invalido_para_selecao_de_history_referente_a_custody
    Then response_statuscode_400

*** Variable ***
${cpf}=  08130385961
${senha}=  123Abc

*** Keywords ***

# Case 01:

consulta_do_papel_com_possibilidade_de_resgate
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${itemsPerPage}  Set Variable  10
    ${Page}  Set Variable  6
    ${type}  Set Variable  true
    ${RESPOSTAREQUEST}  Redemption Investments  ${guid}  ${token}  ${itemsPerPage}  ${Page}  ${type}

exibir_id_para_selecao_de_history_referente_a_custody_com_mais_de_um_resgate_parcial
    Log  ${RESPOSTAREQUEST.text}
    ${custodyId}  Set Variable   ${RESPOSTAREQUEST.json()}
    ${id}  Set Variable  ${custodyId['items'][1]['id']}
    Set Test Variable  ${id}

verificar_informacoes_de_resgates_ja_realizados_para_determinado_custody
    ${itemsPerPage}  Set Variable  10
    ${Page}  Set Variable  6
    ${filter}  Set Variable  Withdraw
    ${RESPOSTAREQUEST}  Redemption Investments History  ${guid}  ${token}  ${id}  ${filter}  ${Page}  ${itemsPerPage}

exibir_informacoes_de_historys
    ${custodyHistoryResult}  Set Variable   ${RESPOSTAREQUEST.json()}
    ${resultFinalHistory}  Set Variable  ${custodyHistoryResult['items']}
    Log  ${resultFinalHistory}

response_statuscode_200
    ${STATUSCODE_DESEJADO}  Set Variable  200
    Log  ${RESPOSTAREQUEST}
    Should Be Equal As Strings  ${RESPOSTAREQUEST.status_code}  200


# Case 02:

consulta_do_papel_com_possibilidade_resgate
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${itemsPerPage}  Set Variable  10
    ${Page}  Set Variable  6
    ${type}  Set Variable  true
    ${RESPOSTAREQUEST}  Redemption Investments  ${guid}  ${token}  ${itemsPerPage}  ${Page}  ${type}

exibir_id_para_selecao_de_history_referente_a_custody_com_apenas_um_resgate_parcial
    Log  ${RESPOSTAREQUEST.text}
    ${custodyId}  Set Variable   ${RESPOSTAREQUEST.json()}
    ${id}  Set Variable  ${custodyId['items'][3]['id']}
    Set Test Variable  ${id}

verificar_informacao_de_resgate_realizado_para_determinado_custody
    ${itemsPerPage}  Set Variable  10
    ${Page}  Set Variable  6
    ${filter}  Set Variable  Withdraw
    ${RESPOSTAREQUEST}  Redemption Investments History  ${guid}  ${token}  ${id}  ${filter}  ${Page}  ${itemsPerPage}

exibir_informacoes_de_history_detail
    ${custodyHistoryResult}  Set Variable   ${RESPOSTAREQUEST.json()}
    ${type[0]}  Set Variable  ${custodyHistoryResult['items'][0]['type']}
    ${date[0]}  Set Variable  ${custodyHistoryResult['items'][0]['date']}
    ${value[0]}  Set Variable  ${custodyHistoryResult['items'][0]['value']}
    Log  ${type[0]}
    Log  ${date[0]}
    Log  ${value[0]}

response_statuscd_200
    ${STATUSCODE_DESEJADO}  Set Variable  200
    Log  ${RESPOSTAREQUEST}
    Should Be Equal As Strings  ${RESPOSTAREQUEST.status_code}  200


# Case 03:

consulta_do_papel_para_resgate
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${itemsPerPage}  Set Variable  10
    ${Page}  Set Variable  22
    ${type}  Set Variable  true
    ${RESPOSTAREQUEST}  Redemption Investments  ${guid}  ${token}  ${itemsPerPage}  ${Page}  ${type}

exibir_id_para_selecao_de_history_referente_a_custody_sem_valores
    Log  ${RESPOSTAREQUEST.text}
    ${custodyId}  Set Variable   ${RESPOSTAREQUEST.json()}
    ${id}  Set Variable  ${custodyId['items'][0]['id']}
    Set Test Variable  ${id}

verificar_valores_zerados_para_determinado_custody
    ${itemsPerPage}  Set Variable  10
    ${Page}  Set Variable  0
    ${filter}  Set Variable  Withdraw
    ${RESPOSTAREQUEST}  Redemption Investments History  ${guid}  ${token}  ${id}  ${filter}  ${Page}  ${itemsPerPage}

response_listavazia_statuscode_200
    ${custodyHistoryResult}  Set Variable   ${RESPOSTAREQUEST.json()}
    ${resultFinalHistory}  Set Variable  ${custodyHistoryResult['items']}
    Log  ${resultFinalHistory}
    ${STATUSCODE_DESEJADO}  Set Variable  200
    Log  ${RESPOSTAREQUEST}
    Should Be Equal As Strings  ${RESPOSTAREQUEST.status_code}  200


# Case 04:

inserir_id_invalido_para_selecao_de_history_referente_a_custody
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${id}  Set Variable  4c594449414e53-20578
    ${itemsPerPage}  Set Variable  10
    ${Page}  Set Variable  0
    ${filter}  Set Variable  Withdraw
    ${RESPOSTAREQUEST}  Redemption Investments History  ${guid}  ${token}  ${id}  ${filter}  ${Page}  ${itemsPerPage}

response_statuscode_400
    ${STATUSCODE_DESEJADO}  Set Variable  400
    Log  ${RESPOSTAREQUEST}
    Should Be Equal As Strings  ${RESPOSTAREQUEST.status_code}  400