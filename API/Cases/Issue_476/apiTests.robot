*** Settings ***
Documentation
...            As criado novo endpoint (melhoria) para os produtos de investimentos,
...            Want melhoria atrelada as informações da tabela Barigui.Services.Investment_Parameters,
...            So definir a visibilidade da listagem de investimentos.
Resource	   ${EXECDIR}/API/Resources/main.resource
Suite Setup    Abrir conexao  https://api.${server}.bancobari.com.br

*** Test Cases ***

Verificação para listagem dos produtos com response 200
    [Tags]  backend  automation
    Given verificacao_para_listagem_dos_produtos
    When exibido_informacoes_atraves_do_ID_cadastrado_na_tabela
    Then response_devera_retornar_200
    And verificar_dados_na_tabela_Parameters

Verificação para listagem dos produtos com response 200 (Lista vazia)
    [Tags]  backend  automation
    Given removido_informacao_na_tabela_Investment_Parameters
    When verificacao_do_response_para_listagem_dos_produtos_vazia
    Then response_devera_retornar_200_vazia
    And cadastro_novamente_info_tabela_referente_listagem_de_produtos
    And verificar_dados_inseridos_na_tabela_Parameters

Verificação para listagem dos produtos com type false
    [Tags]  backend  automation
    Given atualizacao_availableProducts_type_false_in_dynamodb
    When verificacao_do_response_para_listagem
    Then response_devera_retornar_200_type_false
    And cadastro_novamente_info_tabela_referente_listagem_normalizada

*** Variable ***
${cpf}=  08130385961
${senha}=  123Abc

*** Keywords ***

# Case 01:

verificacao_para_listagem_dos_produtos
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${RESPOSTAREQUEST}  Investment Parameters  ${guid}  ${token}

exibido_informacoes_atraves_do_ID_cadastrado_na_tabela
    Log  ${RESPOSTAREQUEST.text}
    ${returnResponseListProducts}  Set Variable  ${RESPOSTAREQUEST.json()}
    ${typeCDB}  Set Variable  ${returnResponseListProducts['availableProducts'][0]['type']}
    Set Test Variable  ${typeCDB}
    ${isAvailableCDB}  Set Variable  ${returnResponseListProducts['availableProducts'][0]['isAvailable']}
    Set Test Variable  ${isAvailableCDB}
    ${typeLCI}  Set Variable  ${returnResponseListProducts['availableProducts'][1]['type']}
    Set Test Variable  ${typeLCI}
    ${isAvailableLCI}  Set Variable  ${returnResponseListProducts['availableProducts'][1]['isAvailable']}
    Set Test Variable  ${isAvailableLCI}

response_devera_retornar_200
    ${STATUSCODE_DESEJADO}  Set Variable  200
    Log  ${RESPOSTAREQUEST}
    Should Be Equal As Strings  ${RESPOSTAREQUEST.status_code}  ${STATUSCODE_DESEJADO}

verificar_dados_na_tabela_Parameters
    ${resultParametersDynamo}  Consulta Dynamo  Barigui.Services.Investment_Parameters  Id  E05CD577-988F-41CE-97AB-B9745E2E3597
    ${returnDynamoListProducts}  Set Variable  ${RESPOSTAREQUEST.json()}
    ${typeCDBDynamo}  Set Variable  ${returnDynamoListProducts['availableProducts'][0]['type']}
    ${isAvailableCDBDynamo}  Set Variable  ${returnDynamoListProducts['availableProducts'][0]['isAvailable']}
    ${typeLCIDynamo}  Set Variable  ${returnDynamoListProducts['availableProducts'][1]['type']}
    ${isAvailableLCIDynamo}  Set Variable  ${returnDynamoListProducts['availableProducts'][1]['isAvailable']}
    Should Be True  '${typeCDB}' == '${typeCDBDynamo}'
    Should Be True  '${isAvailableCDB}' == '${isAvailableCDBDynamo}'
    Should Be True  '${typeLCI}' == '${typeLCIDynamo}'
    Should Be True  '${isAvailableLCI}' == '${isAvailableLCIDynamo}'


# Case 02:

removido_informacao_na_tabela_Investment_Parameters
    Update InvestmentParameters  E05CD577-988F-41CE-97AB-B9745E2E3597

verificacao_do_response_para_listagem_dos_produtos_vazia
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${RESPOSTAREQUEST}  Investment Parameters  ${guid}  ${token}

response_devera_retornar_200_vazia
    ${STATUSCODE_DESEJADO}  Set Variable  200
    Log  ${RESPOSTAREQUEST}
    Log  ${RESPOSTAREQUEST.text}
    Should Be Equal As Strings  ${RESPOSTAREQUEST.status_code}  ${STATUSCODE_DESEJADO}

cadastro_novamente_info_tabela_referente_listagem_de_produtos
    Update InvestmentReturnParameters  E05CD577-988F-41CE-97AB-B9745E2E3597

verificar_dados_inseridos_na_tabela_Parameters
    ${resultAllParameters}  Consulta Dynamo  Barigui.Services.Investment_Parameters  Id  E05CD577-988F-41CE-97AB-B9745E2E3597
    Log  ${resultAllParameters}


# Case 03:

atualizacao_availableProducts_type_false_in_dynamodb
    Update InvestmentFalseTypeParameter  E05CD577-988F-41CE-97AB-B9745E2E3597

verificacao_do_response_para_listagem
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${RESPOSTAREQUEST}  Investment Parameters  ${guid}  ${token}

response_devera_retornar_200_type_false
    ${STATUSCODE_DESEJADO}  Set Variable  200
    Log  ${RESPOSTAREQUEST}
    Log  ${RESPOSTAREQUEST.text}
    ${resultAllParameters}  Consulta Dynamo  Barigui.Services.Investment_Parameters  Id  E05CD577-988F-41CE-97AB-B9745E2E3597
    Log  ${resultAllParameters}
    Should Be Equal As Strings  ${RESPOSTAREQUEST.status_code}  ${STATUSCODE_DESEJADO}

cadastro_novamente_info_tabela_referente_listagem_normalizada
    Update InvestmentReturnParameters  E05CD577-988F-41CE-97AB-B9745E2E3597


