*** Settings ***
Documentation
...            As como usuario do Banco Bari,
...            Want gostaria poder realizar aplicações nos papeis da Virtual,
...            sendo eles CDB, LCI e CRI,
...            So poder visualizar meus investimentos.
Default Tags   Release
Resource	   ${EXECDIR}/API/Resources/main.resource
Suite Setup    Abrir conexao  https://api.${server}.bancobari.com.br

*** Test Cases ***

Enviar valores de aplicações para investimento (Apply - CDB, Virtual)
    [Tags]  backend  automation
    Given consulta_por_papel_registrado_na_base_de_dados_(cdb)
    When enviar_informacoes_de_aplicacoes_apply_(cdb)
    And response_devera_ter_status_de_202_(cdb)
    Then verifica_balance_do_usuario_apply_include_(cdb)
    And verifica_envio_da_solicitacao_apply_(cdb)

Enviar valores de aplicações para investimento (Apply - LCI, Virtual)
    [Tags]  backend  automation
    Given consulta_por_papel_registrado_base_de_dados_(lci)
    When enviar_informacoes_de_aplicacoes_apply_(lci)
    And response_status_de_202_(lci)
    Then verifica_balance_do_usuario_apply_include_(lci)
    And verifica_envio_da_solicitacao_apply_(lci)

Enviar valores de aplicações para investimento (Apply - CRI, Virtual)
    [Tags]  backend  automation
    Given consulta_por_papel_registrado_na_base_de_dados_(cri)
    When enviar_informacoes_de_aplicacoes_apply_(cri)
    And response_devera_ter_status_de_202_(cri)
    Then verifica_balance_do_usuario_apply_include_(cri)
    And verifica_envio_da_solicitacao_apply_(cri)


*** Variable ***
${cpf}=  08130385961
${senha}=  123Abc

*** Keywords ***

# Case 01 (Apply - CDB):

consulta_por_papel_registrado_na_base_de_dados_(cdb)
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${type}  Set Variable  CDB
    ${RESPOSTAREQUEST}  List Of Papers  ${guid}  ${token}  ${type}

enviar_informacoes_de_aplicacoes_apply_(cdb)
    Log  ${RESPOSTAREQUEST.text} 
    Set Test Variable  ${Id}  ${RESPOSTAREQUEST.json()}
    Set Test Variable  ${Id}  ${Id['items'][2]['id']}
    ${RESPOSTAREQUEST}  Choose Investment Type  ${guid}  ${token}  ${Id}

    ${json_string}=  catenate

    ...	 {	
    ...   "type": "Apply",
    ...   "value": 888800,
    ...   "valueId": "${Id}"
    ...	 }
    
    Log  ${json_string}
    ${RESPOSTAREQUEST}  Shipping Transaction Value  ${guid}  ${token}  ${Id}  ${json_string}

response_devera_ter_status_de_202_(cdb)
    Log  ${RESPOSTAREQUEST.text}
    ${STATUSCODE_DESEJADO}  Set Variable  202
    Should Be Equal As Strings  ${RESPOSTAREQUEST.status_code}  ${STATUSCODE_DESEJADO}

verifica_balance_do_usuario_apply_include_(cdb)
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${RESPOSTAREQUEST}  Consult Balance User  ${guid}  ${token}

verifica_envio_da_solicitacao_apply_(cdb)
    Sleep  08
    ${body}  Email Verification  no-reply@${server}.bancobari.com.br  CDB


# Case 02 (Apply - LCI):

consulta_por_papel_registrado_base_de_dados_(lci)
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${type}  Set Variable  LCI
    ${RESPOSTAREQUEST}  List Of Papers  ${guid}  ${token}  ${type}

enviar_informacoes_de_aplicacoes_apply_(lci)
    Log  ${RESPOSTAREQUEST.text} 
    Set Test Variable  ${Id}  ${RESPOSTAREQUEST.json()}
    Set Test Variable  ${Id}  ${Id['items'][2]['id']}
    ${RESPOSTAREQUEST}  Choose Investment Type  ${guid}  ${token}  ${Id}

    ${json_string}=  catenate

    ...	 {	
    ...   "type": "Apply",
    ...   "value": 888800,
    ...   "valueId": "${Id}"
    ...	 }
    
    Log  ${json_string}
    ${RESPOSTAREQUEST}  Shipping Transaction Value  ${guid}  ${token}  ${Id}  ${json_string}

response_status_de_202_(lci)
    Log  ${RESPOSTAREQUEST.text}
    ${STATUSCODE_DESEJADO}  Set Variable  202
    Should Be Equal As Strings  ${RESPOSTAREQUEST.status_code}  ${STATUSCODE_DESEJADO}

verifica_balance_do_usuario_apply_include_(lci)
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${RESPOSTAREQUEST}  Consult Balance User  ${guid}  ${token}

verifica_envio_da_solicitacao_apply_(lci)
    Sleep  08
    ${body}  Email Verification  no-reply@${server}.bancobari.com.br  LCI


# Case 03 (Apply - CRI):

consulta_por_papel_registrado_na_base_de_dados_(cri)
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${type}  Set Variable  CRI
    ${RESPOSTAREQUEST}  List Of Papers  ${guid}  ${token}  ${type}

enviar_informacoes_de_aplicacoes_apply_(cri)
    Log  ${RESPOSTAREQUEST.text} 
    Set Test Variable  ${Id}  ${RESPOSTAREQUEST.json()}
    Set Test Variable  ${Id}  ${Id['items'][0]['id']}
    ${RESPOSTAREQUEST}  Choose Investment Type  ${guid}  ${token}  ${Id}

    ${json_string}=  catenate

    ...	 {	
    ...   "type": "Apply",
    ...   "value": 888800,
    ...   "valueId": "${Id}"
    ...	 }
    
    Log  ${json_string}
    ${RESPOSTAREQUEST}  Shipping Transaction Value  ${guid}  ${token}  ${Id}  ${json_string}

response_devera_ter_status_de_202_(cri)
    Log  ${RESPOSTAREQUEST.text}
    ${STATUSCODE_DESEJADO}  Set Variable  202
    Should Be Equal As Strings  ${RESPOSTAREQUEST.status_code}  ${STATUSCODE_DESEJADO}

verifica_balance_do_usuario_apply_include_(cri)
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${RESPOSTAREQUEST}  Consult Balance User  ${guid}  ${token}

verifica_envio_da_solicitacao_apply_(cri)
    Sleep  08
    ${body}  Email Verification  no-reply@${server}.bancobari.com.br  CRI