*** Settings ***
Documentation
...            As usuário do Banco Bari que possui investimentos disponíveis para resgate antes do vencimento,
...            Want saber mais informações sobre o papel escolhido, e visualizar o valor líquido do investimento,
...            So decidir se vou solicitar o resgate.
Default Tags   Sprint33
Resource	   ${EXECDIR}/API/Resources/main.resource
Suite Setup    Abrir conexao  https://api.${server}.bancobari.com.br

*** Test Cases ***

Exibir resumo do investimento com detalhes da aplicação com possibilidade de resgate
    [Tags]  backend  automation
    Given exibir_informacoes_de_investimentos_referente_ao_resgate_com_liquidez_diaria
    When consulta_id_para_obter_informacoes_de_detalhamento_do_resgate
    And exibir_detalhamento_sobre_o_papel_selecionado_para_resgate_disponivel
    Then response_devera_ter_statuscode_de_200

Exibir resumo do investimento com detalhes da aplicação no vencimento
    [Tags]  backend  automation
    Given exibir_info_de_investimentos_referente_ao_resgate
    When consulta_pelo_id_para_obter_informacoes_de_detalhamento_do_resgate_no_vencimento
    And exibir_detalhamento_sobre_o_papel_selecionado_para_resgate_no_vencimento
    Then response_statuscode_200_vencimento

*** Variable ***
${cpf}=  08130385961
${senha}=  123Abc

*** Keywords ***

# Case 01 (Liquidez diária):

exibir_informacoes_de_investimentos_referente_ao_resgate_com_liquidez_diaria
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${itemsPerPage}  Set Variable  5
    ${Page}  Set Variable  0
    ${type}  Set Variable  true
    ${RESPOSTAREQUEST}  Redemption Investments  ${guid}  ${token}  ${itemsPerPage}  ${Page}  ${type}

consulta_id_para_obter_informacoes_de_detalhamento_do_resgate
    Log  ${RESPOSTAREQUEST.text}
    ${custodyId}  Set Variable   ${RESPOSTAREQUEST.json()}
    ${id}  Set Variable  ${custodyId['items'][2]['id']}
    Set Test Variable  ${id}
    ${RESPOSTAREQUEST}  View Details Investments Redemption  ${guid}  ${token}  ${id}

exibir_detalhamento_sobre_o_papel_selecionado_para_resgate_disponivel
    Log  ${RESPOSTAREQUEST.json()}
    ${resultItem}  Set Variable   ${RESPOSTAREQUEST.json()}
    ${resultDescription}  Set Variable  ${resultItem['description']}
    ${resultLiquidity}  Set Variable  ${resultItem['liquidity']}
    ${resultProfitability}  Set Variable  ${resultItem['profitability']}
    Log  ${resultDescription}
    Log  ${resultLiquidity}
    Log  ${resultProfitability}

response_devera_ter_statuscode_de_200
    ${STATUSCODE_DESEJADO}  Set Variable   200
    Log  ${RESPOSTAREQUEST}
    Should Be Equal As Strings  ${RESPOSTAREQUEST.status_code}  ${STATUSCODE_DESEJADO}


# Case 02 (No vencimento):

exibir_info_de_investimentos_referente_ao_resgate
    ${guid}  Gera Guid 
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${itemsPerPage}  Set Variable  10
    ${Page}  Set Variable  0
    ${type}  Set Variable  false
    ${RESPOSTAREQUEST}  Redemption Investments  ${guid}  ${token}  ${itemsPerPage}  ${Page}  ${type}  

consulta_pelo_id_para_obter_informacoes_de_detalhamento_do_resgate_no_vencimento
    ${custodyId}  Set Variable   ${RESPOSTAREQUEST.json()}
    ${id}  Set Variable  ${custodyId['items'][3]['id']}
    Set Test Variable  ${id}
    ${RESPOSTAREQUEST}  View Details Investments Redemption  ${guid}  ${token}  ${id}

exibir_detalhamento_sobre_o_papel_selecionado_para_resgate_no_vencimento
    Log  ${RESPOSTAREQUEST.json()}
    ${resultItem}  Set Variable   ${RESPOSTAREQUEST.json()}
    ${resultDescription}  Set Variable  ${resultItem['description']}
    ${resultLiquidity}  Set Variable  ${resultItem['liquidity']}
    ${resultProfitability}  Set Variable  ${resultItem['profitability']}
    Log  ${resultDescription}
    Log  ${resultLiquidity}
    Log  ${resultProfitability}

response_statuscode_200_vencimento
    ${STATUSCODE_DESEJADO}  Set Variable   200
    Log  ${RESPOSTAREQUEST}
    Should Be Equal As Strings  ${RESPOSTAREQUEST.status_code}  ${STATUSCODE_DESEJADO}