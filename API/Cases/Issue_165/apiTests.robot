*** Settings ***
Documentation
...            As usuário que possui investimentos no Banco Bari,
...            Want visualizar meus investimentos,
...            So ver o rendimento que o meu dinheiro está tendo no Banco Bari e quando será possível resgatar minha aplicação.
Default Tags   Sprint33
Resource	   ${EXECDIR}/API/Resources/main.resource
Suite Setup    Abrir conexao  https://api.${server}.bancobari.com.br

*** Test Cases ***

Exibir informações de investimentos ativos referente ao usuário com possibilidade de resgate (true)
    [Tags]  backend  automation
    Given exibir_informacoes_de_investimentos_referente_ao_resgate_true
    When informacoes_do_papel_disponivel_para_resgate
    Then response_devera_ter_status_de_200

Exibir informações de investimentos ativos sem possibilidade de resgate referente ao usuário (false)
    [Tags]  backend  automation
    Given exibir_informacoes_investimentos_referente_ao_resgate_false
    When informacoes_do_papel_nao_disponivel_para_resgate
    Then response_devera_ter_status_200

*** Variable ***
${cpf}=  08130385961
${senha}=  123Abc

*** Keywords ***

# Case 01 (True - Disponível para resgate):

exibir_informacoes_de_investimentos_referente_ao_resgate_true
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${itemsPerPage}  Set Variable  10
    ${Page}  Set Variable  0
    ${type}  Set Variable  true
    ${RESPOSTAREQUEST}  Redemption Investments  ${guid}  ${token}  ${itemsPerPage}  ${Page}  ${type}

informacoes_do_papel_disponivel_para_resgate
    Log  ${RESPOSTAREQUEST.text}
    Set Test Variable  ${page}  ${RESPOSTAREQUEST.json()}
    Set Test Variable  ${valueWithdrawId}  ${page['items'][0]['id']}
    Set Test Variable  ${valueTitleDescriptionWithdraw}  ${page['items'][0]['description']}
    Set Test Variable  ${valueWithdrawLiquidity}  ${page['items'][0]['liquidity']}
    Set Test Variable  ${valueProfitability}  ${page['items'][0]['profitability']}
    Log  ${valueTitleDescriptionWithdraw}
    Log  ${valueWithdrawLiquidity}
    Log  ${valueProfitability}

response_devera_ter_status_de_200
    ${STATUSCODE_DESEJADO}  Set Variable  200
    Log  ${RESPOSTAREQUEST}
    Should Be Equal As Strings  ${RESPOSTAREQUEST.status_code}  ${STATUSCODE_DESEJADO}


# Case 02: (False - Não disponível para resgate):

exibir_informacoes_investimentos_referente_ao_resgate_false
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${itemsPerPage}  Set Variable  10
    ${Page}  Set Variable  0
    ${type}  Set Variable  false
    ${RESPOSTAREQUEST}  Redemption Investments  ${guid}  ${token}  ${itemsPerPage}  ${Page}  ${type}

informacoes_do_papel_nao_disponivel_para_resgate
    Log  ${RESPOSTAREQUEST.text}
    Set Test Variable  ${page}  ${RESPOSTAREQUEST.json()}
    Set Test Variable  ${valueWithdrawId}  ${page['items'][3]['id']}
    Set Test Variable  ${valueTitleDescriptionWithdraw}  ${page['items'][3]['description']}
    Set Test Variable  ${valueWithdrawLiquidity}  ${page['items'][3]['liquidity']}
    Set Test Variable  ${valueProfitability}  ${page['items'][3]['profitability']}
    Log  ${valueTitleDescriptionWithdraw}
    Log  ${valueWithdrawLiquidity}
    Log  ${valueProfitability}

response_devera_ter_status_200
    ${STATUSCODE_DESEJADO}  Set Variable  200
    Log  ${RESPOSTAREQUEST}
    Should Be Equal As Strings  ${RESPOSTAREQUEST.status_code}  ${STATUSCODE_DESEJADO}
    
