*** Keywords ***

History_Withdraw

    Wait Until Element Is Visible  //androidx.cardview.widget.CardView[1]/android.view.ViewGroup
    Wait Until Element Is Visible  //android.view.ViewGroup/androidx.cardview.widget.CardView

    Start Screen Recording

    Wait Until Element Is Visible  ${pageDashboardInvestimentos.btnViewBalance}  30
    Element Text Should Be  ${pageDashboardInvestimentos.btnViewBalance}  Mostrar saldo
    Click Element  ${pageDashboardInvestimentos.btnViewBalance}

    Verification user balance graphic  ${cpf}  ${senha}

    ${valueFrontInitialInvestment}  Get Text  ${pageDashboardInvestimentos.lblInicitalInvestment}    
    ${str}  Remove String  ${valueFrontInitialInvestment}  R$  .  ,
    ${valueFrontInitialInvestmentStripp}  Strip String  ${str}
    Should Be Equal As Integers   ${valueFrontInitialInvestmentStripp}  ${investmentValueTotal}

    ${valueFrontBalanceTotal}  Get Text  ${pageDashboardInvestimentos.lblViewBalanceValue}
    ${str}  Remove String  ${valueFrontBalanceTotal}  R$  .  ,
    ${valueFrontBalanceTotalStripp}  Strip String  ${str}
    Should Be Equal As Integers  ${valueFrontBalanceTotalStripp}  ${balanceTotal}

    ${valueFrontGrossValueTotal}  Get Text  ${pageDashboardInvestimentos.ViewGrossEarnings}
    ${str}  Remove String  ${valueFrontGrossValueTotal}  R$  .  ,
    ${valueFrontGrossValueTotalStripp}  Strip String  ${str}
    Should Be Equal As Integers  ${valueFrontGrossValueTotalStripp}  ${grossEarningsTotal}

    Wait Until Element Is Visible  ${MainMenu.btnMenuPrincipal}  30
    Click Element  ${MainMenu.btnMenuPrincipal}
    
    Wait Until Element Is Visible  ${MainMenu.btnResgate}  30
    Click Element  ${MainMenu.btnResgate}

    Wait Until Element Is Visible  ${pageInformesRendimentos.lblTitleText}  30
    Element Text Should Be  ${pageInformesRendimentos.lblTitleText}  Resgate

    Wait Until Element Is Visible  //androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[2]  30
    Click Element  //androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[2]

    Wait Until Element Is Visible  ${validationCardsResgate.lblZeroWithdraw}  30
    Element Text Should Be  ${validationCardsResgate.lblZeroWithdraw}  Não foi realizado nenhum resgate parcial.

    Wait Until Element Is Visible  ${pageDetailWithdraw.btnBackDetailWithdrawJanela}  30
    Click Element  ${pageDetailWithdraw.btnBackDetailWithdrawJanela}

    Wait Until Element Is Visible  //androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]  30
    Click Element  //androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]

    Wait Until Element Is Visible  ${pageDetailWithdraw.lblHistoryOperation}  30
    Click Element  ${pageDetailWithdraw.lblHistoryOperation}

    Wait Until Element Is Visible  //android.widget.FrameLayout/android.view.ViewGroup/androidx.cardview.widget.CardView  30
    Wait Until Element Is Visible  //android.view.ViewGroup[5]/android.widget.TextView[1]  30

    Wait Until Element Is Visible  ${pageDetailWithdraw.btnBackDetailWithdrawJanela}  30
    Click Element  ${pageDetailWithdraw.btnBackDetailWithdrawJanela}

    Wait Until Element Is Visible  ${pageWithdraw.lblBackDash}  90
    Click Element  ${pageWithdraw.lblBackDash}

    Wait Until Element Is Visible  ${pageWithdraw.lblBackDash}  90
    Click Element  ${pageWithdraw.lblBackDash}

    Stop Screen Recording


History_My_Investments

    Wait Until Element Is Visible  ${pageHome.iconInvestimentos}  30
    Click Element  ${pageHome.iconInvestimentos}

    Wait Until Element Is Visible  //androidx.cardview.widget.CardView[1]/android.view.ViewGroup
    Wait Until Element Is Visible  //android.view.ViewGroup/androidx.cardview.widget.CardView

    Start Screen Recording

    Wait Until Element Is Visible  ${pageDashboardInvestimentos.btnViewBalance}  30
    Element Text Should Be  ${pageDashboardInvestimentos.btnViewBalance}  Mostrar saldo
    Click Element  ${pageDashboardInvestimentos.btnViewBalance}

    Verification user balance graphic  ${cpf}  ${senha}

    ${valueFrontInitialInvestment}  Get Text  ${pageDashboardInvestimentos.lblInicitalInvestment}    
    ${str}  Remove String  ${valueFrontInitialInvestment}  R$  .  ,
    ${valueFrontInitialInvestmentStripp}  Strip String  ${str}
    Should Be Equal As Integers   ${valueFrontInitialInvestmentStripp}  ${investmentValueTotal}

    ${valueFrontBalanceTotal}  Get Text  ${pageDashboardInvestimentos.lblViewBalanceValue}
    ${str}  Remove String  ${valueFrontBalanceTotal}  R$  .  ,
    ${valueFrontBalanceTotalStripp}  Strip String  ${str}
    Should Be Equal As Integers  ${valueFrontBalanceTotalStripp}  ${balanceTotal}

    ${valueFrontGrossValueTotal}  Get Text  ${pageDashboardInvestimentos.ViewGrossEarnings}
    ${str}  Remove String  ${valueFrontGrossValueTotal}  R$  .  ,
    ${valueFrontGrossValueTotalStripp}  Strip String  ${str}
    Should Be Equal As Integers  ${valueFrontGrossValueTotalStripp}  ${grossEarningsTotal}
    
    Wait Until Element Is Visible  ${pageDashboardInvestimentos.btnMeusInvestmentos}  30
    Element Text Should Be  ${pageDashboardInvestimentos.lblBtnMeusInvestmentos}  Meus investimentos
    Click Element  ${pageDashboardInvestimentos.btnMeusInvestmentos}

    Wait Until Element Is Visible  //android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]
    Click Element  //androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[2]
    
    Wait Until Element Is Visible  ${pageMyInvestments.lblTitleDetalhesDoInvestimento}  30
    Element Text Should Be  ${pageMyInvestments.lblTitleDetalhesDoInvestimento}  Detalhes do investimento
    
    Wait Until Element Is Visible  //androidx.cardview.widget.CardView[1]/android.widget.LinearLayout

    Wait Until Element Is Visible  ${pageDetailWithdraw.btnBackDetailWithdrawJanela}  30
    Click Element  ${pageDetailWithdraw.btnBackDetailWithdrawJanela}

    Wait Until Element Is Visible  //android.view.ViewGroup[3]/android.widget.TextView[1]  30
    Scroll  //android.view.ViewGroup[3]/android.widget.TextView[1]  //android.view.ViewGroup[1]/android.widget.TextView[1]

    Wait Until Element Is Visible  //android.view.ViewGroup[3]/android.widget.TextView[6]  30
    Scroll  //android.view.ViewGroup[3]/android.widget.TextView[6]  //android.view.ViewGroup[2]/android.widget.TextView[1]

    Wait Until Element Is Visible  //android.view.ViewGroup[3]/android.widget.TextView[1]  30
    Scroll  //android.view.ViewGroup[3]/android.widget.TextView[1]  //android.view.ViewGroup[1]/android.widget.TextView[1]

    Wait Until Element Is Visible  //androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[2]
    Click Element  //androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[2]

    Wait Until Element Is Visible  ${pageMyInvestments.lblHistoryInvestment}  30
    Click Element  ${pageMyInvestments.lblHistoryInvestment}

    Wait Until Element Is Visible  //android.widget.FrameLayout/android.view.ViewGroup/androidx.cardview.widget.CardView  30
    Wait Until Element Is Visible  //android.view.ViewGroup[5]/android.widget.TextView[1]  30

    Wait Until Element Is Visible  ${pageDetailWithdraw.btnBackDetailWithdrawJanela}  30
    Click Element  ${pageDetailWithdraw.btnBackDetailWithdrawJanela}

    Stop Screen Recording

    Wait Until Element Is Visible  ${pageDetailWithdraw.btnBackDetailWithdrawJanela}  30
    Click Element  ${pageDetailWithdraw.btnBackDetailWithdrawJanela}

    Wait Until Element Is Visible  ${pageWithdraw.lblBackDash}  90
    Click Element  ${pageWithdraw.lblBackDash}

    Wait Until Element Is Visible  ${pageWithdraw.lblBackDash}  90
    Click Element  ${pageWithdraw.lblBackDash}