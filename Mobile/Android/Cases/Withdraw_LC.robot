*** Keywords ***

Withdraw_Transaction_LC

    Wait Until Element Is Visible  //androidx.cardview.widget.CardView[1]/android.view.ViewGroup
    Wait Until Element Is Visible  //android.view.ViewGroup/androidx.cardview.widget.CardView

    Start Screen Recording

    Wait Until Element Is Visible  ${pageDashboardInvestimentos.btnViewBalance}  30
    Element Text Should Be  ${pageDashboardInvestimentos.btnViewBalance}  Mostrar saldo
    Click Element  ${pageDashboardInvestimentos.btnViewBalance}

    Verification user balance graphic  ${cpf}  ${senha}

    ${valueFrontInitialInvestment}  Get Text  ${pageDashboardInvestimentos.lblInicitalInvestment}    
    ${str}  Remove String  ${valueFrontInitialInvestment}  R$  .  ,
    ${valueFrontInitialInvestmentStripp}  Strip String  ${str}
    Should Be Equal As Integers   ${valueFrontInitialInvestmentStripp}  ${investmentValueTotal}

    ${valueFrontBalanceTotal}  Get Text  ${pageDashboardInvestimentos.lblViewBalanceValue}
    ${str}  Remove String  ${valueFrontBalanceTotal}  R$  .  ,
    ${valueFrontBalanceTotalStripp}  Strip String  ${str}
    Should Be Equal As Integers  ${valueFrontBalanceTotalStripp}  ${balanceTotal}

    ${valueFrontGrossValueTotal}  Get Text  ${pageDashboardInvestimentos.ViewGrossEarnings}
    ${str}  Remove String  ${valueFrontGrossValueTotal}  R$  .  ,
    ${valueFrontGrossValueTotalStripp}  Strip String  ${str}
    Should Be Equal As Integers  ${valueFrontGrossValueTotalStripp}  ${grossEarningsTotal}

    Wait Until Element Is Visible  ${MainMenu.btnMenuPrincipal}  30
    Click Element  ${MainMenu.btnMenuPrincipal}
    
    Wait Until Element Is Visible  ${MainMenu.btnResgate}  30
    Click Element  ${MainMenu.btnResgate}

    Wait Until Element Is Visible  ${pageInformesRendimentos.lblTitleText}  30
    Element Text Should Be  ${pageInformesRendimentos.lblTitleText}  Resgate

    Wait Until Element Is Visible  //android.view.ViewGroup[2]/android.widget.TextView[6]  30
    Scroll  //android.view.ViewGroup[2]/android.widget.TextView[6]  //android.view.ViewGroup[1]/android.widget.TextView[1]

    Click Element  //android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[3]

    Wait Until Element Is Visible  ${validationCardsDetalhesResgate.lblTitleDetailPage}  50
    Element Text Should Be  ${validationCardsDetalhesResgate.lblTitleDetailPage}  Detalhes do resgate

    Wait Until Element Is Visible  //android.widget.LinearLayout/android.view.ViewGroup[2]/android.widget.TextView[3]  50
    Scroll  //android.widget.LinearLayout/android.view.ViewGroup[2]/android.widget.TextView[3]  //android.view.ViewGroup/android.widget.TextView

    Scroll  //android.view.ViewGroup[1]/android.widget.TextView[10]  //android.view.ViewGroup/android.widget.TextView

    Wait Until Element Is Visible  ${validationCardsDetalhesResgate.btnDetailWithdraw}  30
    Click Element  ${validationCardsDetalhesResgate.btnDetailWithdraw}

    Wait Until Element Is Visible  ${validationValorDoResgate.lblTitleValorDoResgate}  30
    Element Text Should Be  ${validationValorDoResgate.lblTitleValorDoResgate}  Valor do resgate
    
    Element Text Should Be  ${validationValorDoResgate.lblTextValorDoResgate}  Qual valor você quer resgatar?
    Element Text Should Be  ${validationValorDoResgate.lblTextSaldoAplicacao}  Saldo da aplicação

    Wait Until Element Is Visible  ${pageValueWithdraw.btnValueMinWithdraw}  30
    Element Text Should Be  ${pageValueWithdraw.btnValueMinWithdraw}  min R$ 0,01

    Wait Until Element Is Visible  ${pageValueWithdraw.btnInsertValueWithdraw}  30
    ${pageValueWithdrawApply}  Set Variable  ${pageValueWithdraw.btnInsertValueWithdraw}
    ${valueWithdrawApplyValue}  Set Variable  7500
    Input Text  ${pageValueWithdrawApply}  ${valueWithdrawApplyValue}

    Wait Until Element Is Visible  ${pageValueWithdraw.btnContinueMaxWithdraw}  30
    Element Text Should Be  ${pageValueWithdraw.btnContinueMaxWithdraw}  Continuar
    Click Element  ${pageValueWithdraw.btnContinueMaxWithdraw}

    Wait Until Element Is Visible  ${validationResumoDoResgate.lblTitleResumoDoResgate}  30
    Element Text Should Be  ${validationResumoDoResgate.lblTitleResumoDoResgate}  Resumo do resgate

    Wait Until Element Is Visible  ${pageResumeWithdraw.btnConfirmWithdraw}  30
    Element Text Should Be  ${pageResumeWithdraw.btnConfirmWithdraw}  Confirmar resgate
    Click Element  ${pageResumeWithdraw.btnConfirmWithdraw}

    Wait Until Element Is Visible  ${validationModalResgate.lblModalResgateSucesso}   30
    Element Text Should Be  ${validationModalResgate.lblModalResgateSucesso}  Resgate realizado com sucesso
    Element Text Should Be  ${validationModalResgate.lblModalResgateParabens}  Parabéns, seu resgate foi realizado e o dinheiro foi transferido para a sua conta Bari.

    Wait Until Element Is Visible  ${pageResumeWithdraw.btnBackWithdraw}  30
    Element Text Should Be  ${pageResumeWithdraw.btnBackWithdraw}  Ok, voltar agora
    Click Element  ${pageResumeWithdraw.btnBackWithdraw}

    Stop Screen Recording


My_Investment_LC

    Start Screen Recording

    Wait Until Element Is Visible  //androidx.cardview.widget.CardView[1]/android.view.ViewGroup
    Wait Until Element Is Visible  //android.view.ViewGroup/androidx.cardview.widget.CardView

    Wait Until Element Is Visible  ${pageDashboardInvestimentos.btnMeusInvestmentos}  30
    Element Text Should Be  ${pageDashboardInvestimentos.lblBtnMeusInvestmentos}  Meus investimentos
    Click Element  ${pageDashboardInvestimentos.btnMeusInvestmentos}

    Wait Until Element Is Visible  //android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]

    Wait Until Element Is Visible  //android.view.ViewGroup[2]/android.widget.TextView[10]  50
    Scroll  //android.view.ViewGroup[2]/android.widget.TextView[10]  //android.view.ViewGroup[1]/android.widget.TextView[1]

    Wait Until Element Is Visible  //android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[3]/android.widget.TextView[1]  50
    Scroll  //android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[3]/android.widget.TextView[1]  //androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[2]/android.widget.TextView[1]

    Wait Until Element Is Visible  //android.view.ViewGroup[2]/android.widget.TextView[1]  50
    Click Element  //android.view.ViewGroup[2]/android.widget.TextView[1]

    Wait Until Element Is Visible  ${pageMyInvestments.lblTitleDetalhesDoInvestimento}  30
    Element Text Should Be  ${pageMyInvestments.lblTitleDetalhesDoInvestimento}  Detalhes do investimento
    
    Wait Until Element Is Visible  //androidx.cardview.widget.CardView[1]/android.widget.LinearLayout

    Wait Until Element Is Visible  //android.widget.LinearLayout[3]/android.widget.LinearLayout/android.widget.TextView[1]  50
    Scroll  //android.widget.LinearLayout[3]/android.widget.LinearLayout/android.widget.TextView[1]  //android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView

    Element Text Should Be  ${pageMyInvestments.lblFrontMyInvestmentsProtecao}  Proteção
    Wait Until Element Is Visible  ${pageMyInvestments.mdlMyInvestmentsFGC}  30
    Click Element       ${pageMyInvestments.mdlMyInvestmentsFGC}

    Wait Until Element Is Visible  ${pageMyInvestments.mdlCloseInvestmentsFGC}  30
    Click Element  ${pageMyInvestments.mdlCloseInvestmentsFGC}

    Wait Until Element Is Visible  ${pageDetailMyInvestment.btnBackDetailInvestment}  30
    Click Element  ${pageDetailMyInvestment.btnBackDetailInvestment}

    Stop Screen Recording