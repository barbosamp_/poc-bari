*** Keywords ***

No_Assets_Investment
    [Arguments]  ${ITEM}

    Start Screen Recording

    Wait Until Element Is Visible  //androidx.cardview.widget.CardView[1]/android.view.ViewGroup
    Wait Until Element Is Visible  //android.view.ViewGroup/androidx.cardview.widget.CardView
    Wait Until Element Is Visible  //android.widget.FrameLayout/android.widget.ImageView

    Wait Until Element Is Visible  ${pageDashboardInvestimentos.btnViewBalance}  30
    Element Text Should Be  ${pageDashboardInvestimentos.btnViewBalance}  Mostrar saldo
    Click Element  ${pageDashboardInvestimentos.btnViewBalance}

    Verification user balance graphic  ${cpf}  ${senha}

    ${valueFrontInitialInvestment}  Get Text  ${pageDashboardInvestimentos.lblInicitalInvestment}    
    ${str}  Remove String  ${valueFrontInitialInvestment}  R$  .  ,
    ${valueFrontInitialInvestmentStripp}  Strip String  ${str}
    Should Be Equal As Integers   ${valueFrontInitialInvestmentStripp}  ${investmentValueTotal}

    ${valueFrontBalanceTotal}  Get Text  ${pageDashboardInvestimentos.lblViewBalanceValue}
    ${str}  Remove String  ${valueFrontBalanceTotal}  R$  .  ,
    ${valueFrontBalanceTotalStripp}  Strip String  ${str}
    Should Be Equal As Integers  ${valueFrontBalanceTotalStripp}  ${balanceTotal}

    ${valueFrontGrossValueTotal}  Get Text  ${pageDashboardInvestimentos.ViewGrossEarnings}
    ${str}  Remove String  ${valueFrontGrossValueTotal}  R$  .  ,
    ${valueFrontGrossValueTotalStripp}  Strip String  ${str}
    Should Be Equal As Integers  ${valueFrontGrossValueTotalStripp}  ${grossEarningsTotal}

    Update NoAssetsActiveInvestmentFalse  98924D48-4B47-4C42-9EC5-54D2AF424881
    Update NoAssetsActiveInvestmentFalse  C26C03D8-D555-4D07-8E81-2C97B5879293

    Element Text Should Be  ${pageDashboardInvestimentos.btnInvestDashboard}  Investir
    Click Element  ${pageDashboardInvestimentos.btnInvestDashboard}

    Wait Until Element Is Visible  ${validationRendaFixa.lblTitlePageRendaFixa}  30
    Element Text Should Be  ${validationRendaFixa.lblTitlePageRendaFixa}  Renda Fixa

    Wait Until Element Is Visible  //android.view.ViewGroup/androidx.cardview.widget.CardView

    Wait Until Element Is Visible  ${pageListOfPapers.lblNoAssetsPapers}  30
    Element Text Should Be  ${pageListOfPapers.lblNoAssetsPapers}  Em breve, teremos novos produtos disponíveis para você investir.

    Wait Until Element Is Visible  ${pageListOfPapers.lblFilterLCI}  30
    Click Element       ${pageListOfPapers.lblFilterLCI}

    Wait Until Element Is Visible  //android.view.ViewGroup/androidx.cardview.widget.CardView

    Wait Until Element Is Visible  ${pageListOfPapers.lblNoAssetsPapers}  30
    Element Text Should Be  ${pageListOfPapers.lblNoAssetsPapers}  	Em breve, teremos novos produtos disponíveis para você investir.

    Wait Until Element Is Visible  ${pageListOfPapers.btnBackListToDashboard}  30
    Click Element  ${pageListOfPapers.btnBackListToDashboard}

    Update NoAssetsActiveInvestmentTrue  98924D48-4B47-4C42-9EC5-54D2AF424881
    Update NoAssetsActiveInvestmentTrue  C26C03D8-D555-4D07-8E81-2C97B5879293

    Wait Until Element Is Visible  //androidx.cardview.widget.CardView/android.view.ViewGroup  30
    Click Element  //androidx.cardview.widget.CardView/android.view.ViewGroup

    Stop Screen Recording