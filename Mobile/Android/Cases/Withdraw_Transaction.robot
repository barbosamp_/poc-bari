*** Keywords ***

Withdraw_Transaction
    [Arguments]  ${ITEM}

    ${idViewGroup}  evaluate  ${ITEM}+1

    Wait Until Element Is Visible  //androidx.cardview.widget.CardView[1]/android.view.ViewGroup
    Wait Until Element Is Visible  //android.view.ViewGroup/androidx.cardview.widget.CardView

    Wait Until Element Is Visible  ${pageDashboardInvestimentos.btnViewBalance}  30
    Element Text Should Be  ${pageDashboardInvestimentos.btnViewBalance}  Mostrar saldo
    Click Element  ${pageDashboardInvestimentos.btnViewBalance}

    Verification user balance graphic  ${cpf}  ${senha}

    ${valueFrontInitialInvestment}  Get Text  ${pageDashboardInvestimentos.lblInicitalInvestment}    
    ${str}  Remove String  ${valueFrontInitialInvestment}  R$  .  ,
    ${valueFrontInitialInvestmentStripp}  Strip String  ${str}
    Should Be Equal As Integers   ${valueFrontInitialInvestmentStripp}  ${investmentValueTotal}

    ${valueFrontBalanceTotal}  Get Text  ${pageDashboardInvestimentos.lblViewBalanceValue}
    ${str}  Remove String  ${valueFrontBalanceTotal}  R$  .  ,
    ${valueFrontBalanceTotalStripp}  Strip String  ${str}
    Should Be Equal As Integers  ${valueFrontBalanceTotalStripp}  ${balanceTotal}

    ${valueFrontGrossValueTotal}  Get Text  ${pageDashboardInvestimentos.ViewGrossEarnings}
    ${str}  Remove String  ${valueFrontGrossValueTotal}  R$  .  ,
    ${valueFrontGrossValueTotalStripp}  Strip String  ${str}
    Should Be Equal As Integers  ${valueFrontGrossValueTotalStripp}  ${grossEarningsTotal}

    Wait Until Element Is Visible  ${MainMenu.btnMenuPrincipal}  30
    Click Element  ${MainMenu.btnMenuPrincipal}
    
    Wait Until Element Is Visible  ${MainMenu.btnResgate}  30
    Click Element  ${MainMenu.btnResgate}

    Wait Until Element Is Visible  ${pageInformesRendimentos.lblTitleText}  30
    Element Text Should Be  ${pageInformesRendimentos.lblTitleText}  Resgate

    View Withdraw  ${ITEM}

    ${valueFrontTitleDescriptionWithdraw}  Get Text  ${validationCardsResgate.lblTitleCardWithdraw}    
    Should Be Equal As Strings   ${valueFrontTitleDescriptionWithdraw}  ${valueTitleDescriptionWithdraw}

    Element Text Should Be  ${validationCardsResgate.lblTitleInvestimentoWithdraw}  Investimento
    ${valueFrontInvestmentWithdraw}  Get Text  ${validationCardsResgate.lblValueInvestimentoWithdraw}
    ${str}  Remove String  ${valueFrontInvestmentWithdraw}  R$  .  ,
    ${valueFrontInvestmentWithdraw}  Strip String  ${str} 
    Should Be Equal As Integers   ${valueFrontInvestmentWithdraw}  ${valueWithdrawInvestmentValue}

    Element Text Should Be  ${validationCardsResgate.lblTitleRentabilidadeWithdraw}  Rentabilidade
    ${valueFrontRentabilidade}  Get Text  ${validationCardsResgate.lblValueRentabilidadeWithdraw}    
    Should Be Equal As Strings   ${valueFrontRentabilidade}  ${valueWithdrawProfitability}

    Element Text Should Be  ${validationCardsResgate.lblTitleRendimentoBrutoWithdraw}  Rendimento bruto
    ${valueFrontRendimentoBruto}  Get Text  ${validationCardsResgate.lblValueRendimentoBrutoWithdraw}
    ${str}  Remove String  ${valueFrontRendimentoBruto}  R$  .  ,
    ${valueFrontRendimentoBruto}  Strip String  ${str} 
    Should Be Equal As Integers   ${valueFrontRendimentoBruto}  ${valueWithdrawGrossEarningsValue}

    Element Text Should Be  ${validationCardsResgate.lblTitleDataAplicacaoWithdraw}  Data da aplicação
    ${valueFrontDataAplicacaoWithdraw}  Get Text  ${validationCardsResgate.lblValueDataAplicacaoWithdraw}
    ${valueWithdrawEmissionDate}  Convert Date  ${valueWithdrawEmissionDate}  datetime  date_format=%Y-%m-%d
    ${valueFrontDataAplicacaoWithdraw}  Convert Date  ${valueFrontDataAplicacaoWithdraw}  datetime  date_format=%d/%m/%Y
    Should Be Equal  ${valueFrontDataAplicacaoWithdraw}  ${valueWithdrawEmissionDate}  

    Element Text Should Be  ${validationCardsResgate.lblTitleVencimentoWithdraw}  Vencimento
    ${ValueFrontVencimentoWithdraw}  Get Text  ${validationCardsResgate.lblValueVencimentoWithdraw}
    ${valueWithdrawMaturityDate}  Convert Date  ${valueWithdrawMaturityDate}  datetime  date_format=%Y-%m-%d
    ${ValueFrontVencimentoWithdraw}  Convert Date  ${ValueFrontVencimentoWithdraw}  datetime  date_format=%d/%m/%Y
    Should Be Equal  ${ValueFrontVencimentoWithdraw}  ${valueWithdrawMaturityDate}

    Element Text Should Be  ${validationCardsResgate.lblTitleResgateWithdraw}  Resgate
    ${valueFrontResgate}  Get Text  ${validationCardsResgate.lblValueResgateWithdraw}    
    Should Be Equal As Strings   ${valueFrontResgate}  ${valueWithdrawLiquidity}

    Click Element  //android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[${idViewGroup}]

    Wait Until Element Is Visible  ${validationCardsDetalhesResgate.lblTitleDetailPage}  50
    Element Text Should Be  ${validationCardsDetalhesResgate.lblTitleDetailPage}  Detalhes do resgate

    View Withdraw Detail  ${ITEM}

    ${valueFrontTitleDetailResgate}  Get Text  ${validationCardsDetalhesResgate.lblTitleStartDetail}    
    Should Be Equal As Strings   ${valueFrontTitleDetailResgate}  ${valueDescriptionDetailWithdraw}

    Element Text Should Be  ${validationCardsDetalhesResgate.lblValueForResgate}  Valor para resgate
    ${valueFrontValueDetail}  Get Text  ${validationCardsDetalhesResgate.lblValueResgate}
    ${str}  Remove String  ${valueFrontValueDetail}  R$  .  ,
    ${valueFrontValueDetail}  Strip String  ${str}     
    Should Be Equal As Strings   ${valueFrontValueDetail}  ${valueNetValueDetailWithdraw}

    Element Text Should Be  ${validationCardsDetalhesResgate.lblDetailRentabilidade}  Rentabilidade
    ${valueFrontDetailCDI}  Get Text  ${validationCardsDetalhesResgate.lblValueDetailCDI}    
    Should Be Equal As Strings   ${valueFrontDetailCDI}  ${valueProfitabilityDetailWithdraw}

    Element Text Should Be  ${validationCardsDetalhesResgate.lblDetailVencimento}  Vencimento
    ${valueDetalhesResgateVencimento}  Get Text  ${validationDetalhamentoInvestimento.lblDIValueVencimento}
    ${valueMaturityDateDetailWithdraw}  Convert Date  ${valueMaturityDateDetailWithdraw}  result_format=%d.%m.%Y
    @{words}  Split String  ${valueDetalhesResgateVencimento}  ${SPACE}
    ${numMes}  Dealing with dates with texts  ${words[2]}
    ${dtCompare}  catenate  ${words[0]}.${numMes}.${words[4]}
    Should Be Equal As Strings  ${valueMaturityDateDetailWithdraw}  ${dtCompare}

    Scroll  ${validationCardsDetalhesResgate.lblDetailSaldoBruto}   ${validationCardsDetalhesResgate.lblTitleStartDetail}

    Wait Until Element Is Visible  ${validationCardsDetalhesResgate.lblDetailValorInvestimento}  30
    Element Text Should Be  ${validationCardsDetalhesResgate.lblDetailValorInvestimento}  Valor do investimento 
    ${valueFrontValueInvestmentDetail}  Get Text  ${validationCardsDetalhesResgate.lblDetailValueValorInvestimento}
    ${str}  Remove String  ${valueFrontValueInvestmentDetail}  R$  .  ,
    ${valueFrontValueInvestmentDetail}  Strip String  ${str}     
    Should Be Equal As Strings   ${valueFrontValueInvestmentDetail}  ${valueInvestmentValueDetailWithdraw}

    Element Text Should Be  ${validationCardsDetalhesResgate.lblDetailRendimentoBruto}  Rendimento bruto
    ${valueFrontGrossEarningsDetail}  Get Text  ${validationCardsDetalhesResgate.lblDetailValueRendimentoBruto}
    ${str}  Remove String  ${valueFrontGrossEarningsDetail}  R$  .  ,
    ${valueFrontGrossEarningsDetail}  Strip String  ${str}
    Convert To Integer  ${valueFrontGrossEarningsDetail}
    Convert To Integer  ${valueGrossEarningsValueDetailWithdraw}
    Should Be Equal As Integers   ${valueFrontGrossEarningsDetail}  ${valueGrossEarningsValueDetailWithdraw}

    Element Text Should Be  ${validationCardsDetalhesResgate.lblDetailSaldoBruto}  Saldo bruto
    ${valueFrontGrossValueDetail}  Get Text  ${validationCardsDetalhesResgate.lblDetailValueSaldoBruto}
    ${str}  Remove String  ${valueFrontGrossValueDetail}  R$  .  ,
    ${valueFrontGrossValueDetail}  Strip String  ${str}     
    Should Be Equal As Strings   ${valueFrontGrossValueDetail}  ${valueGrossValueDetailWithdraw}

    Element Text Should Be  ${validationCardsDetalhesResgate.lblDetailResgateIR}  IR sobre o rendimento
    ${valueFrontDetailIR}  Get Text  ${validationCardsDetalhesResgate.lblDetailValueResgateIR}
    ${str}  Remove String  ${valueFrontDetailIR}  - R$  .  ,
    ${valueFrontDetailIR}  Strip String  ${str}
    Convert To Integer  ${valueFrontDetailIR}
    Convert To Integer  ${valueIRDetailWithdraw}
    Should Be Equal As Integers   ${valueFrontDetailIR}  ${valueIRDetailWithdraw}

    Element Text Should Be  ${validationCardsDetalhesResgate.lblDetailValorIOF}  IOF
    ${valueFrontDetailIOF}  Get Text  ${validationCardsDetalhesResgate.lblDetailValueValorIOF}
    ${str}  Remove String  ${valueFrontDetailIOF}  - R$  .  ,
    ${valueFrontDetailIOF}  Strip String  ${str}
    Convert To Integer  ${valueFrontDetailIOF}
    Convert To Integer  ${valueIOFDetailWithdraw}
    Should Be Equal As Integers   ${valueFrontDetailIOF}  ${valueIOFDetailWithdraw}

    Element Text Should Be  ${validationCardsDetalhesResgate.lblDetailValorLiquidoResgate}  Valor líquido para resgate
    ${valueFrontDetailResgate}  Get Text  ${validationCardsDetalhesResgate.lblDetailValueValorLiquidoResgate}
    ${str}  Remove String  ${valueFrontDetailResgate}  R$  .  ,
    ${valueFrontDetailResgate}  Strip String  ${str}     
    Should Be Equal As Strings   ${valueFrontDetailResgate}  ${valueNetValueDetailWithdraw}

    Wait Until Element Is Visible  ${validationCardsDetalhesResgate.btnDetailWithdraw}  30
    Click Element  ${validationCardsDetalhesResgate.btnDetailWithdraw}

    Wait Until Element Is Visible  ${validationValorDoResgate.lblTitleValorDoResgate}  30
    Element Text Should Be  ${validationValorDoResgate.lblTitleValorDoResgate}  Valor do resgate
    
    Element Text Should Be  ${validationValorDoResgate.lblTextValorDoResgate}  Qual valor você quer resgatar?
    Element Text Should Be  ${validationValorDoResgate.lblTextSaldoAplicacao}  Saldo da aplicação

    Wait Until Element Is Visible  ${pageValueWithdraw.btnValueMinWithdraw}  30
    Element Text Should Be  ${pageValueWithdraw.btnValueMinWithdraw}  min R$ 0,01

    Wait Until Element Is Visible  ${pageValueWithdraw.btnInsertValueWithdraw}  30
    ${pageValueWithdrawApply}  Set Variable  ${pageValueWithdraw.btnInsertValueWithdraw}
    ${valueWithdrawApplyValue}  Set Variable  2500
    Input Text  ${pageValueWithdrawApply}  ${valueWithdrawApplyValue}

    Wait Until Element Is Visible  ${pageValueWithdraw.btnContinueMaxWithdraw}  30
    Element Text Should Be  ${pageValueWithdraw.btnContinueMaxWithdraw}  Continuar
    Click Element  ${pageValueWithdraw.btnContinueMaxWithdraw}

    Wait Until Element Is Visible  ${validationResumoDoResgate.lblTitleResumoDoResgate}  30
    Element Text Should Be  ${validationResumoDoResgate.lblTitleResumoDoResgate}  Resumo do resgate

    ${valueFrontTitleDescriptionWithdraw}  Get Text  ${validationCardsResgate.lblTitleCardWithdraw}    
    Should Be Equal As Strings   ${valueFrontTitleDescriptionWithdraw}  ${valueTitleDescriptionWithdraw}

    Element Text Should Be  ${validationResumoDoResgate.lblResumoValorDoResgate}  Valor do resgate
    ${valueFrontResumoValorResgate}  Get Text  ${validationResumoDoResgate.lblResumoValueResgate}
    ${str}  Remove String  ${valueFrontResumoValorResgate}  R$  .  ,
    ${valueFrontResumoValorResgate}  Strip String  ${str}
    Should Be Equal As Strings  ${valueFrontResumoValorResgate}  ${valueWithdrawApplyValue}

    Element Text Should Be  ${validationResumoDoResgate.lblResumoValorDoInvestimento}  Valor do investimento
    ${valueFrontResumoResgateInvestment}  Get Text  ${validationResumoDoResgate.lblResumoValueValorDoInvestimento}
    ${str}  Remove String  ${valueFrontResumoResgateInvestment}  R$  .  ,
    ${valueFrontResumoResgateInvestment}  Strip String  ${str}
    Should Be Equal As Strings  ${valueFrontResumoResgateInvestment}  ${valueNetValueDetailWithdraw}

    Wait Until Element Is Visible  ${pageResumeWithdraw.btnConfirmWithdraw}  30
    Element Text Should Be  ${pageResumeWithdraw.btnConfirmWithdraw}  Confirmar resgate
    Click Element  ${pageResumeWithdraw.btnConfirmWithdraw}

    Wait Until Element Is Visible  ${validationModalResgate.lblModalResgateSucesso}   30
    Element Text Should Be  ${validationModalResgate.lblModalResgateSucesso}  Resgate realizado com sucesso
    Element Text Should Be  ${validationModalResgate.lblModalResgateParabens}  Parabéns, seu resgate foi realizado e o dinheiro já está em sua Bariconta.

    Wait Until Element Is Visible  ${pageResumeWithdraw.btnBackWithdraw}  30
    Element Text Should Be  ${pageResumeWithdraw.btnBackWithdraw}  Voltar para o início
    Click Element  ${pageResumeWithdraw.btnBackWithdraw}

    Wait Until Element Is Visible  ${pageDashboardInvestimentos.btnViewBalance}  30
