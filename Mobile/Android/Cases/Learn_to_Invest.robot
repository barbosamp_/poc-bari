*** Keywords ***

Learn_to_invest

    Wait Until Element Is Visible  //androidx.cardview.widget.CardView[1]/android.view.ViewGroup
    Wait Until Element Is Visible  //android.view.ViewGroup/androidx.cardview.widget.CardView
    Wait Until Element Is Visible  //android.widget.FrameLayout/android.widget.ImageView

    Start Screen Recording

    Wait Until Element Is Visible  ${pageDashboardInvestimentos.btnViewBalance}  30
    Element Text Should Be  ${pageDashboardInvestimentos.btnViewBalance}  Mostrar saldo

    Verification user balance graphic  ${cpf}  ${senha}

    ${valueFrontInitialInvestment}  Get Text  ${pageDashboardInvestimentos.lblInicitalInvestment}    
    ${str}  Remove String  ${valueFrontInitialInvestment}  R$  .  ,
    ${valueFrontInitialInvestmentStripp}  Strip String  ${str}
    Should Be Equal As Integers   ${valueFrontInitialInvestmentStripp}  ${investmentValueTotal}

    ${valueFrontBalanceTotal}  Get Text  ${pageDashboardInvestimentos.lblViewBalanceValue}
    ${str}  Remove String  ${valueFrontBalanceTotal}  R$  .  ,
    ${valueFrontBalanceTotalStripp}  Strip String  ${str}
    Should Be Equal As Integers  ${valueFrontBalanceTotalStripp}  ${balanceTotal}

    ${valueFrontGrossValueTotal}  Get Text  ${pageDashboardInvestimentos.ViewGrossEarnings}
    ${str}  Remove String  ${valueFrontGrossValueTotal}  R$  .  ,
    ${valueFrontGrossValueTotalStripp}  Strip String  ${str}
    Should Be Equal As Integers  ${valueFrontGrossValueTotalStripp}  ${grossEarningsTotal}

    Scroll  //androidx.cardview.widget.CardView[2]/android.widget.LinearLayout/android.view.ViewGroup/android.widget.TextView  //androidx.cardview.widget.CardView/android.view.ViewGroup/android.widget.TextView[1]

    Wait Until Element Is Visible  ${learnInvestiment.lblLearnInvest}  30
    Click Element  ${learnInvestiment.lblLearnInvest}

    Wait Until Element Is Visible  ${learnInvestiment.lblRendaFixa}  30
    Element Text Should Be  ${learnInvestiment.lblRendaFixa}  O que é Renda Fixa?

    Wait Until Element Is Visible  ${learnInvestiment.lblIconView}  30
    Click Element  ${learnInvestiment.lblIconView}

    Wait Until Element Is Visible  ${learnInvestiment.lblTextRendaFixa}  30
    Element Should Contain Text  ${learnInvestiment.lblTextRendaFixa}  Renda Fixa é uma modalidade de investimento em que você empresta o seu dinheiro, ao governo ou um banco e, em troca, recebe juros.
    Element Should Contain Text  ${learnInvestiment.lblTextRendaFixa}  As condições de prazo e rentabilidade são definidas na hora do investimento, o que garante a previsibilidade de seus ganhos.    

    Wait Until Element Is Visible  ${learnInvestiment.lblIconView}  30
    Click Element  ${learnInvestiment.lblIconView}

    Scroll  //androidx.cardview.widget.CardView[2]/android.view.ViewGroup/android.widget.TextView  //androidx.cardview.widget.CardView[1]/android.view.ViewGroup/android.widget.TextView[1]

    Wait Until Element Is Visible  ${learnInvestiment.lblVantagens}  30
    Element Text Should Be  ${learnInvestiment.lblVantagens}  Vantagens

    Wait Until Element Is Visible  //androidx.cardview.widget.CardView[2]/android.view.ViewGroup/android.widget.ImageButton  30
    Click Element  //androidx.cardview.widget.CardView[2]/android.view.ViewGroup/android.widget.ImageButton

    Wait Until Element Is Visible  ${learnInvestiment.lblTextVantagens1}  30

    Element Should Contain Text  ${learnInvestiment.lblTextVantagens1}  Investimento de baixo risco.
    Element Should Contain Text  ${learnInvestiment.lblTextVantagens2}  Cobertura do Fundo Garantidor de Crédito em alguns produtos.
    Element Should Contain Text  ${learnInvestiment.lblTextVantagens3}  Diversas opções de prazos para resgate.
    Element Should Contain Text  ${learnInvestiment.lblTextVantagens4}  Previsão dos ganhos a partir das condições de rentabilidade.
    Element Should Contain Text  ${learnInvestiment.lblTextVantagens5}  Baixos valores mínimos para aplicação inicial.

    Wait Until Element Is Visible  //androidx.cardview.widget.CardView[2]/android.view.ViewGroup/android.widget.ImageButton  30
    Click Element  //androidx.cardview.widget.CardView[2]/android.view.ViewGroup/android.widget.ImageButton

    Scroll  //androidx.cardview.widget.CardView[3]/android.view.ViewGroup/android.widget.TextView  //androidx.cardview.widget.CardView[2]/android.view.ViewGroup/android.widget.TextView

    Wait Until Element Is Visible  ${learnInvestiment.lblComoEscolher}  30
    Element Text Should Be  ${learnInvestiment.lblComoEscolher}  Como escolher
    
    Wait Until Element Is Visible  //androidx.cardview.widget.CardView/android.view.ViewGroup/android.widget.ImageButton  30
    Click Element  //androidx.cardview.widget.CardView/android.view.ViewGroup/android.widget.ImageButton

    Wait Until Element Is Visible  ${learnInvestiment.lblTextChooseLabelDescription}  30
    Element Text Should Be  ${learnInvestiment.lblTextChooseLabelDescription}  Escolha um produto que atenda as suas necessidades, olhando para a rentabilidade e o vencimento.
    
    Element Text Should Be  ${learnInvestiment.lblTextChooseProfitability}  Rentabilidade:
    Element Text Should Be  ${learnInvestiment.lblTextIncomeTypeLabel}  Produtos de Renda Fixa podem ser:
    Element Text Should Be  ${learnInvestiment.lblTextFirstProfit}  Pré-fixados, quando apresentam percentual fixo de rentabilidade;
    Element Text Should Be  ${learnInvestiment.lblTextSecondProfit}  Pós-fixados, quando seguem algum indicador - geralmente um percentual do CDI - para calcular a rentabilidade;

    Scroll  ${learnInvestiment.lblTextSecondProfit}  ${learnInvestiment.lblTextChooseProfitability}

    Element Text Should Be  ${learnInvestiment.lblTextValidityRescueSubtitle}  Vencimento e Resgate:
    Element Text Should Be  ${learnInvestiment.lblTextValidityRescueDescription}  Produtos de Renda Fixa possuem um prazo de vencimento, quando chega ao final, permite o resgate. Porém, alguns produtos permitem resgates antecipados, que são informados na hora da contratação.

    Scroll  ${learnInvestiment.lblCardsProducts}  ${learnInvestiment.lblTextValidityRescueSubtitle}

    Wait Until Element Is Visible  ${learnInvestiment.lblCardsProducts}  30
    Element Text Should Be  ${learnInvestiment.lblCardsProducts}  Produtos de Renda Fixa
    Element Text Should Be  ${learnInvestiment.lblTextInvestmentTitle}  CDB
    Element Text Should Be  ${learnInvestiment.lblTextInvestmentSubtitle}  Certificado de Depósito Bancário.
    Element Text Should Be  ${learnInvestiment.lblTextInvestmentShortDescription}  É um investimento de Renda Fixa em que você empresta o seu dinheiro a um banco durante um período de tempo e, em troca, recebe juros.

    Element Text Should Be  ${learnInvestiment.lblSeeMore}  Saiba mais
    Click Element  ${learnInvestiment.lblSeeMore}

    Wait Until Element Is Visible  ${learnInvestiment.lblDescriptionTextView}  30
    Swipe    338    1230    362    330

    Element Should Contain Text  ${learnInvestiment.lblDescriptionTextView}  Certificado de Depósito Bancário.
    Element Should Contain Text  ${learnInvestiment.lblDescriptionTextView}  É um investimento de Renda Fixa em que você empresta o seu dinheiro a um banco durante um período de tempo e, em troca, recebe juros.
    Element Should Contain Text  ${learnInvestiment.lblDescriptionTextView}  Esta rentabilidade é definida na hora da aplicação e pode ser um valor fixo, chamado de prefixado, ou seguir um indicador, chamado de pós-fixado.
    Element Should Contain Text  ${learnInvestiment.lblDescriptionTextView}  Tem tributação de Imposto de Renda em uma tabela regressiva, o que traz vantagens para investimentos de prazos mais longos.
    Element Should Contain Text  ${learnInvestiment.lblDescriptionTextView}  São ideais para quem está começando a investir ou tem perfil de investidor moderado.
    Element Should Contain Text  ${learnInvestiment.lblDescriptionTextView}  Para investir em CDB basta escolher um produto que se adeque a sua expectativa de rentabilidade e prazo de resgate.
    
    Element Should Contain Text  ${learnInvestiment.lblAtendimento}  Precisa de ajuda para escolher um investimento?

    Wait Until Element Is Visible  ${learnInvestiment.buttonLearnGeneral}  30
    Click Element  ${learnInvestiment.buttonLearnGeneral}

    Wait Until Element Is Visible  //androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]  30

    Wait Until Element Is Visible  ${learnInvestiment.lblHelperDisclaimer}  30
    Click Element  ${learnInvestiment.lblHelperDisclaimer}

    Wait Until Element Is Visible  ${learnInvestiment.lblRendaFixa}  30

    Wait Until Element Is Visible  ${learnInvestiment.lblIconBack}  30
    Click Element  ${learnInvestiment.lblIconBack}

    Wait Until Element Is Visible  //androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]  30
    Click Element  //androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]

    Wait Until Element Is Visible  ${learnInvestiment.lblHelperDisclaimer}  30
    Click Element  ${learnInvestiment.lblHelperDisclaimer}

    Wait Until Element Is Visible  ${learnInvestiment.lblIconBack}  30
    Click Element  ${learnInvestiment.lblIconBack}

    Wait Until Element Is Visible  //android.widget.LinearLayout/android.view.ViewGroup[1]

    Wait Until Element Is Visible  ${learnInvestiment.lblIconBack}  30
    Click Element  ${learnInvestiment.lblIconBack}

    Wait Until Element Is Visible  ${learnInvestiment.lblHelperDisclaimer}  30
    Click Element  ${learnInvestiment.lblHelperDisclaimer}

    Wait Until Element Is Visible  ${learnInvestiment.lblRendaFixa}  30

    Scroll   //android.widget.RelativeLayout/android.widget.TextView  //androidx.cardview.widget.CardView[2]/android.view.ViewGroup/android.widget.TextView
    
    Wait Until Element Is Visible  //androidx.cardview.widget.CardView[1]/android.view.ViewGroup  30
    Swipe    631    952    94    949

    Wait Until Element Is Visible  ${learnInvestiment.lblCardsProducts}  30
    Element Text Should Be  ${learnInvestiment.lblCardsProducts}  Produtos de Renda Fixa
    Element Text Should Be  ${learnInvestiment.lblTextInvestmentTitle}  LCI
    Element Text Should Be  ${learnInvestiment.lblTextInvestmentSubtitle}  Letra de Crédito Imobiliário.
    Element Text Should Be  ${learnInvestiment.lblTextInvestmentShortDescription}  É um investimento de Renda Fixa em que você empresta seu dinheiro a um banco para que ele invista no setor imobiliário e durante o período deste empréstimo, recebe juros.

    Element Text Should Be  ${learnInvestiment.lblSeeMore}  Saiba mais
    Click Element  ${learnInvestiment.lblSeeMore}

    Wait Until Element Is Visible  ${learnInvestiment.lblDescriptionTextView}  30
    Swipe    338    1230    362    330

    Wait Until Element Is Visible  ${learnInvestiment.lblDescriptionTextView}  30
    Element Should Contain Text  ${learnInvestiment.lblDescriptionTextView}  Letra de Crédito Imobiliário.
    Element Should Contain Text  ${learnInvestiment.lblDescriptionTextView}  É um investimento de Renda Fixa em que você empresta seu dinheiro a um banco para que ele invista no setor imobiliário e durante o período deste empréstimo, recebe juros.
    Element Should Contain Text  ${learnInvestiment.lblDescriptionTextView}  Esta rentabilidade é definida na hora da aplicação e pode ser um valor fixo, chamado de prefixado, ou seguir um indicador, chamado de pós-fixado.
    Element Should Contain Text  ${learnInvestiment.lblDescriptionTextView}  É um investimento de risco baixo e que tem cobertura do Fundo Garantidor de Crédito.
    Element Should Contain Text  ${learnInvestiment.lblDescriptionTextView}  Tem como principal vantagem ser isento de Imposto de Renda.
    Element Should Contain Text  ${learnInvestiment.lblDescriptionTextView}  São ideais para quem está começando a investir ou tem perfil de investidor moderado.
    Element Should Contain Text  ${learnInvestiment.lblDescriptionTextView}  Para investir em LCI basta escolher um produto que se adeque a sua expectativa de rentabilidade e prazo de resgate.

    Wait Until Element Is Visible  ${learnInvestiment.buttonLearnGeneral}  30
    Click Element  ${learnInvestiment.buttonLearnGeneral}

    Wait Until Element Is Visible  //androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]  30

    Wait Until Element Is Visible  ${learnInvestiment.lblHelperDisclaimer}  30
    Click Element  ${learnInvestiment.lblHelperDisclaimer}

    Wait Until Element Is Visible  ${learnInvestiment.lblIconBack}  30
    Click Element  ${learnInvestiment.lblIconBack}

    Wait Until Element Is Visible  ${learnInvestiment.lblIconBack}  30
    Click Element  ${learnInvestiment.lblIconBack}

    Wait Until Element Is Visible  ${learnInvestiment.lblIconBack}  30
    Click Element  ${learnInvestiment.lblIconBack}

    Wait Until Element Is Visible  ${learnInvestiment.lblIconBack}  30
    Click Element  ${learnInvestiment.lblIconBack}

    Stop Screen Recording 