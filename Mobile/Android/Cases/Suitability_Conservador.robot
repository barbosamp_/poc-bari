*** Keywords ***

Suitability_Conservador

    Start Screen Recording

    Wait Until Element Is Visible  ${pageHome.iconInvestimentos}  30
    Click Element  ${pageHome.iconInvestimentos}

    Wait Until Element Is Visible  ${MainMenu.btnMenuPrincipal}  30
    Click Element       ${MainMenu.btnMenuPrincipal}
    
    Wait Until Element Is Visible  ${MainMenu.btnPerfil}  30
    Click Element       ${MainMenu.btnPerfil}

    Wait Until Element Is Visible  ${pagePerfil.perfilDetail}  30

    Wait Until Element Is Visible  ${pagePerfil.btnRefazerTeste}  30
    Element Text Should Be  ${pagePerfil.btnRefazerTeste}  Refazer o teste do perfil
    Click Element       ${pagePerfil.btnRefazerTeste}

    Wait Until Element Is Visible  ${pageQuestions.lblAskingQuestion}  30
    Element Text Should Be  ${pageQuestions.lblAskingQuestion}  Ao investir no Banco Bari, qual seu objetivo referente ao seu patrimônio?
    Element Text Should Be  ${pageQuestions.lblFirstQuestion}  Acumular patrimônio de maneira segura.
    Element Text Should Be  ${pageQuestions.lblSecondQuestion}  Acumular patrimônio mesclando segurança com um pouco mais de risco.
    Element Text Should Be  ${pageQuestions.lblThirdQuestion}  Visando maiores ganhos, admito expor meu patrimônio a possíveis perdas.

    Click Element  ${pageQuestions.btnFirstQuestion}

    Wait Until Element Is Visible  ${pageLogin.btncontinue}  30
    Click Element       ${pageLogin.btncontinue}

    Wait Until Element Is Visible  ${pageQuestions.lblAskingQuestion}  30
    Element Text Should Be  ${pageQuestions.lblAskingQuestion}  Qual prazo que você pretende deixar seu dinheiro aplicado junto ao Banco Bari?
    Element Text Should Be  ${pageQuestions.lblFirstQuestion}  Até 1 ano.
    Element Text Should Be  ${pageQuestions.lblSecondQuestion}  De 1 ano até 5 anos.
    Element Text Should Be  ${pageQuestions.lblThirdQuestion}  Acima de 5 anos.

    Click Element  ${pageQuestions.btnFirstQuestion}

    Wait Until Element Is Visible  ${pageLogin.btncontinue}  30
    Click Element       ${pageLogin.btncontinue}

    Wait Until Element Is Visible  ${pageQuestions.lblAskingQuestion}  30
    Element Text Should Be  ${pageQuestions.lblAskingQuestion}  Caso o mercado oscilasse e seus investimentos perdessem 20% de seu valor, o que você faria?
    Element Text Should Be  ${pageQuestions.lblFirstQuestion}  Venderia toda minha posição.
    Element Text Should Be  ${pageQuestions.lblSecondQuestion}  Manteria minha posição.
    Element Text Should Be  ${pageQuestions.lblThirdQuestion}  Aumentaria minha posição, pois o mercado tem seus altos e baixos.

    Click Element  ${pageQuestions.btnFirstQuestion}

    Wait Until Element Is Visible  ${pageLogin.btncontinue}  30
    Click Element       ${pageLogin.btncontinue}

    Wait Until Element Is Visible  ${pageQuestions.lblAskingQuestion}  30
    Element Text Should Be  ${pageQuestions.lblAskingQuestion}  Qual sua necessidade futura de retiradas dos recursos aplicados junto ao Banco Bari?
    Element Text Should Be  ${pageQuestions.lblFirstQuestion}  A princípio, não tenho necessidade.
    Element Text Should Be  ${pageQuestions.lblSecondQuestion}  Necessito fazer retiradas mensais.
    Element Text Should Be  ${pageQuestions.lblThirdQuestion}  Talvez necessite de recursos após 1 ano.

    Click Element  ${pageQuestions.btnFirstQuestion}

    Wait Until Element Is Visible  ${pageLogin.btncontinue}  30
    Click Element       ${pageLogin.btncontinue}

    Wait Until Element Is Visible  ${pageQuestions.lblAskingQuestion}  30
    Element Text Should Be  ${pageQuestions.lblAskingQuestion}  Qual seu nível de escolaridade? 
    Element Text Should Be  ${pageQuestions.lblFirstQuestion}  Ensino Médio.
    Element Text Should Be  ${pageQuestions.lblSecondQuestion}  Graduação incompleta.
    Element Text Should Be  ${pageQuestions.lblThirdQuestion}  Graduação completa.
    Element Text Should Be  ${pageQuestions.lblFourthQuestion}  Pós Graduação.

    Click Element  ${pageQuestions.btnFirstQuestion}

    Wait Until Element Is Visible  ${pageLogin.btncontinue}  30
    Click Element       ${pageLogin.btncontinue}

    Wait Until Element Is Visible  ${pageQuestions.lblMultipleQuestion}  30
    Element Text Should Be  ${pageQuestions.lblMultipleQuestion}  Dos produtos listados abaixo, quais você nunca aplicou, já aplicou ou atualmente possui aplicação?
    Element Text Should Be  ${pageQuestions.lblFirstMultipleQuestion}  Produtos ou Fundos de Renda Fixa (Títulos Públicos e CDB)?
    Element Text Should Be  ${pageQuestions.lblFirstQuestionFirst}  Nunca apliquei
    Element Text Should Be  ${pageQuestions.lblFirstQuestionSecond}  Já apliquei
    Element Text Should Be  ${pageQuestions.lblFirstQuestionThird}  Tenho aplicado

    Click Element   ${pageQuestions.btnFirstQuestionFirst}

    Element Text Should Be  ${pageQuestions.lblSecondMultipleQuestion}  Fundos Multimercado
    Element Text Should Be  ${pageQuestions.lblSecondQuestionFirst}  Nunca apliquei
    Element Text Should Be  ${pageQuestions.lblSecondQuestionSecond}  Já apliquei
    Element Text Should Be  ${pageQuestions.lblSecondQuestionThird}  Tenho aplicado

    Click Element  ${pageQuestions.btnSecondQuestionFirst}

    Element Text Should Be  ${pageQuestions.lblThirdMultipleQuestion}  Ações ou Fundos de Ações?
    Element Text Should Be  ${pageQuestions.lblThirdQuestionFirst}  Nunca apliquei
    Element Text Should Be  ${pageQuestions.lblThirdQuestionSecond}  Já apliquei

    Scroll  ${pageQuestions.lblThirdQuestionSecond}  ${pageQuestions.lblMultipleQuestion}
    
    Element Text Should Be  ${pageQuestions.lblThirdQuestionThird}  Tenho aplicado

    Click Element  ${pageQuestions.btnThirdQuestionFirst}

    Wait Until Element Is Visible  ${pageQuestions.lblFourthMultipleQuestion}  30
    Element Text Should Be  ${pageQuestions.lblFourthMultipleQuestion}  CRI / CRA
    Element Text Should Be  ${pageQuestions.lblFourthQuestionFirst}  Nunca apliquei
    Element Text Should Be  ${pageQuestions.lblFourthQuestionSecond}  Já apliquei
    Element Text Should Be  ${pageQuestions.lblFourthQuestionThird}  Tenho aplicado

    Click Element  ${pageQuestions.btnFourthQuestionFirst}

    Element Text Should Be  ${pageQuestions.lblFifthMultipleQuestion}  LCI ou LCA
    Element Text Should Be  ${pageQuestions.lblFifthQuestionFirst}  Nunca apliquei
    Element Text Should Be  ${pageQuestions.lblFifthQuestionSecond}  Já apliquei
    Element Text Should Be  ${pageQuestions.lblFifthQuestionThird}  Tenho aplicado

    Click Element  ${pageQuestions.btnFifthQuestionFirst}

    Wait Until Element Is Visible  ${pageLogin.btncontinue}
    Click Element       ${pageLogin.btncontinue}

    Wait Until Element Is Visible  ${pageQuestions.lblAskingQuestion}  30
    Element Text Should Be  ${pageQuestions.lblAskingQuestion}  Qual sua experiência com investimentos no mercado financeiro?
    Element Text Should Be  ${pageQuestions.lblFirstQuestion}  Poupança, CDB, fundos de Renda Fixa e títulos públicos.
    Element Text Should Be  ${pageQuestions.lblSecondQuestion}  Além das anteriores, Renda Fixa atrelada a inflação e fundos multimercados.
    Element Text Should Be  ${pageQuestions.lblThirdQuestion}  Além de todas as anteriores, fundos de ações, ações e derivativos.

    Click Element  ${pageQuestions.btnFirstQuestion}

    Wait Until Element Is Visible  ${pageLogin.btncontinue}  30
    Click Element       ${pageLogin.btncontinue}

    Wait Until Element Is Visible  ${pagePerfil.perfilDetail}  30
    Element Text Should Be  ${pagePerfil.perfilDetail}  Possui baixa tolerância a risco e prioriza investimentos em produtos com liquidez, sempre buscando a preservação do capital e o acúmulo de patromônio de maneira segura.

    Wait Until Element Is Visible  ${pageLogin.btncontinue}  30
    Click Element       ${pageLogin.btncontinue}
