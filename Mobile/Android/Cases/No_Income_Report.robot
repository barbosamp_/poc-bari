*** Keywords ***

No_Income_Report

    Start Screen Recording

    Wait Until Element Is Visible  ${MainMenu.btnMenuPrincipal}  30
    Click Element       ${MainMenu.btnMenuPrincipal}
    
    Wait Until Element Is Visible  ${MainMenu.btnInformesRendimento}  30
    Click Element       ${MainMenu.btnInformesRendimento}

    Wait Until Element Is Visible  ${pageInformesRendimentos.lblTitleText}  30
    Element Text Should Be  ${pageInformesRendimentos.lblTitleText}  Informe de rendimentos

    Wait Until Element Is Visible  ${pageInformesRendimentos.btnBackDetailInvestment}  30
    Element Text Should Be  ${pageInformesRendimentos.msgInformesRendimentos}  Você não realizou nenhum investimento ou ainda não existem Informes de Rendimento disponíveis.
    Click Element       ${pageInformesRendimentos.btnBackDetailInvestment}

