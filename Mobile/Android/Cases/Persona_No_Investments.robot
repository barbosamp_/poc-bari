*** Keywords ***

Suitability_Moderado_No_Investments

    Delete CustomerSuitability  76432015871

    Wait Until Element Is Visible  ${pageHome.iconInvestimentos}  30
    Click Element  ${pageHome.iconInvestimentos}

    Wait Until Element Is Visible  ${validationPersonaNoInvestments.lblTitleInvestimentos}  30
    Element Text Should Be  ${validationPersonaNoInvestments.lblTitleInvestimentos}  Investimentos

    Wait Until Element Is Visible  ${validationPersonaNoInvestments.lblDescricaoPerfilInvestidor}  30
    Element Text Should Be  ${validationPersonaNoInvestments.lblDescricaoPerfilInvestidor}  Para indicar os produtos que mais combinam com você, vamos fazer um teste para descobrir seu perfil de investidor.

    Wait Until Element Is Visible  ${validationPersonaNoInvestments.btnFazerTeste}  30
    Element Text Should Be  ${validationPersonaNoInvestments.btnFazerTeste}  Fazer o teste
    Click Element  ${validationPersonaNoInvestments.btnFazerTeste}

    Wait Until Element Is Visible  ${pageQuestions.lblAskingQuestion}  30

    Click Element  ${pageQuestions.btnSecondQuestion}

    Wait Until Element Is Visible  ${pageLogin.btncontinue}  30
    Click Element  ${pageLogin.btncontinue}

    Wait Until Element Is Visible  ${pageQuestions.lblAskingQuestion}  30
    Click Element  ${pageQuestions.btnSecondQuestion}

    Wait Until Element Is Visible  ${pageLogin.btncontinue}  30
    Click Element  ${pageLogin.btncontinue}

    Wait Until Element Is Visible  ${pageQuestions.lblAskingQuestion}  30
    Click Element  ${pageQuestions.btnSecondQuestion}

    Wait Until Element Is Visible  ${pageLogin.btncontinue}  30
    Click Element  ${pageLogin.btncontinue}

    Wait Until Element Is Visible  ${pageQuestions.lblAskingQuestion}  30
    Click Element  ${pageQuestions.btnSecondQuestion}

    Wait Until Element Is Visible  ${pageLogin.btncontinue}  30
    Click Element  ${pageLogin.btncontinue}

    Wait Until Element Is Visible  ${pageQuestions.lblAskingQuestion}  30
    Click Element  ${pageQuestions.btnThirdQuestion}

    Wait Until Element Is Visible  ${pageLogin.btncontinue}  30
    Click Element  ${pageLogin.btncontinue}

    Wait Until Element Is Visible  ${pageQuestions.lblMultipleQuestion}  30
    Click Element   ${pageQuestions.btnFirstQuestionSecond}

    Wait Until Element Is Visible  ${pageQuestions.btnSecondQuestionFirst}  30
    Click Element  ${pageQuestions.btnSecondQuestionFirst}

    Scroll  ${pageQuestions.lblThirdQuestionSecond}  ${pageQuestions.lblMultipleQuestion}
    
    Click Element  ${pageQuestions.btnThirdQuestionFirst}

    Wait Until Element Is Visible  ${pageQuestions.lblFourthMultipleQuestion}  30
    Click Element  ${pageQuestions.btnFourthQuestionFirst}

    Wait Until Element Is Visible  ${pageQuestions.btnFifthQuestionSecond}  30
    Click Element  ${pageQuestions.btnFifthQuestionSecond}

    Wait Until Element Is Visible  ${pageLogin.btncontinue}  30
    Click Element  ${pageLogin.btncontinue}

    Wait Until Element Is Visible  ${pageQuestions.lblAskingQuestion}  30
    Click Element  ${pageQuestions.btnSecondQuestion}

    Wait Until Element Is Visible  ${pageLogin.btncontinue}  30
    Click Element  ${pageLogin.btncontinue}

    Wait Until Element Is Visible  ${pagePerfil.perfilDetail}  30
    Element Text Should Be  ${pagePerfil.perfilDetail}  Busca rendimentos mais vantajosos tolerando um certo risco mantendo o foco na preservação do capital no longo prazo.

    Wait Until Element Is Visible  ${pageLogin.btncontinue}  30
    Click Element  ${pageLogin.btncontinue}


Validation_dashboard_no_investments

    Wait Until Element Is Visible  //android.view.ViewGroup/androidx.cardview.widget.CardView  30

    Wait Until Element Is Visible  ${validationPersonaNoInvestments.btnComecarInvestir}  30
    Element Text Should Be  ${validationPersonaNoInvestments.btnComecarInvestir}  Começar a investir
    Click Element  ${validationPersonaNoInvestments.btnComecarInvestir}

    Wait Until Element Is Visible  //androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]
    Click Element  //androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]

    Wait Until Element Is Visible  //android.view.ViewGroup[2]/android.widget.TextView[3]  30
    Scroll  //android.view.ViewGroup[2]/android.widget.TextView[3]  //android.widget.LinearLayout/android.view.ViewGroup[1]/android.widget.TextView[1]

    Wait Until Element Is Visible  ${validationPersonaNoInvestments.btnCenterButton}  30
    Element Text Should Be  ${validationPersonaNoInvestments.btnCenterButton}  Atendimento

    Wait Until Element Is Visible  ${learnInvestiment.lblIconBack}  30
    Click Element  ${learnInvestiment.lblIconBack}

    Wait Until Element Is Visible  ${learnInvestiment.lblIconBack}  30
    Click Element  ${learnInvestiment.lblIconBack}

    Wait Until Element Is Visible  //androidx.cardview.widget.CardView/android.widget.RelativeLayout  30
    Click Element  //androidx.cardview.widget.CardView/android.widget.RelativeLayout

    Wait Until Element Is Visible  ${learnInvestiment.lblIconBack}  30
    Click Element  ${learnInvestiment.lblIconBack}

No_Withdraw_Transaction

    Wait Until Element Is Visible  //androidx.cardview.widget.CardView[1]/android.view.ViewGroup
    Wait Until Element Is Visible  //android.view.ViewGroup/androidx.cardview.widget.CardView

    Wait Until Element Is Visible  ${MainMenu.btnMenuPrincipal}  30
    Click Element  ${MainMenu.btnMenuPrincipal}
    
    Wait Until Element Is Visible  ${MainMenu.btnResgate}  30
    Click Element  ${MainMenu.btnResgate}

    Wait Until Element Is Visible  ${pageInformesRendimentos.lblTitleText}  30
    Element Text Should Be  ${pageInformesRendimentos.lblTitleText}  Resgate

    Wait Until Element Is Visible  ${validationPersonaNoInvestments.lblInfoZeroResgate}  30
    Element Text Should Be  ${validationPersonaNoInvestments.lblInfoZeroResgate}  Você não possui investimentos com possibilidade de resgate antes do vencimento.
    
    Wait Until Element Is Visible  ${validationPersonaNoInvestments.lblInfoZeroResgateVencimento}  30
    Element Text Should Be  ${validationPersonaNoInvestments.lblInfoZeroResgateVencimento}  O resgate do seu investimento acontecerá automaticamente no vencimento do prazo e o dinheiro será transferido para sua Bariconta.

    Click Element  ${validationPersonaNoInvestments.btnBackToResgate}


No_Income_Report_Investments

    Wait Until Element Is Visible  //androidx.cardview.widget.CardView[1]/android.view.ViewGroup
    Wait Until Element Is Visible  //android.view.ViewGroup/androidx.cardview.widget.CardView

    Wait Until Element Is Visible  ${MainMenu.btnMenuPrincipal}  30
    Click Element  ${MainMenu.btnMenuPrincipal}
    
    Wait Until Element Is Visible  ${MainMenu.btnInformesRendimento}  30
    Click Element  ${MainMenu.btnInformesRendimento}

    Wait Until Element Is Visible  ${pageInformesRendimentos.lblTitleText}  30
    Element Text Should Be  ${pageInformesRendimentos.lblTitleText}  Informe de rendimentos

    Wait Until Element Is Visible  ${pageInformesRendimentos.btnBackDetailInvestment}  30
    Element Text Should Be  ${pageInformesRendimentos.msgInformesRendimentos}  Você não realizou nenhum investimento ou ainda não existem Informes de Rendimento disponíveis.
    Click Element  ${pageInformesRendimentos.btnBackDetailInvestment}


Perfil_User_Investments

    Wait Until Element Is Visible  //androidx.cardview.widget.CardView[1]/android.view.ViewGroup
    Wait Until Element Is Visible  //android.view.ViewGroup/androidx.cardview.widget.CardView

    Wait Until Element Is Visible  ${MainMenu.btnMenuPrincipal}  30
    Click Element  ${MainMenu.btnMenuPrincipal}
    
    Wait Until Element Is Visible  ${MainMenu.btnPerfilMenu}  30
    Click Element  ${MainMenu.btnPerfilMenu}

    Wait Until Element Is Visible  ${pagePerfilMenu.titlePerfil}  30
    Element Text Should Be  ${pagePerfilMenu.titlePerfil}  Perfil

    Wait Until Element Is Visible  ${pagePerfilMenu.btnBackPerfil}  30
    Click Element  ${pagePerfilMenu.btnBackPerfil}


Attendance_Menu_Install_WhatsApp

    Wait Until Element Is Visible  //androidx.cardview.widget.CardView[1]/android.view.ViewGroup
    Wait Until Element Is Visible  //android.view.ViewGroup/androidx.cardview.widget.CardView

    Start Screen Recording

    Wait Until Element Is Visible  ${MainMenu.btnMenuPrincipal}  30
    Click Element  ${MainMenu.btnMenuPrincipal}
    
    Wait Until Element Is Visible  ${MainMenu.btnCompliance}  30
    Click Element  ${MainMenu.btnCompliance}

    Wait Until Element Is Visible  //android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.TextView  60
    Click Element  //android.widget.LinearLayout[1]/android.widget.LinearLayout/android.widget.TextView

    Wait Until Element Is Visible  //android.widget.LinearLayout/android.widget.Button[1]  60
    Click Element  //android.widget.LinearLayout/android.widget.Button[1]

    Wait Until Element Is Visible  //android.widget.ImageButton[@content-desc="Enviar"]  60
    Click Element  //android.widget.ImageButton[@content-desc="Enviar"]

    Wait Until Element Is Visible  //android.view.ViewGroup[4]/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.TextView  60

    Stop Screen Recording


Attendance_Menu_Desinstall_WhatsApp

    Start Screen Recording

    Wait Until Element Is Visible  ${MainMenu.btnMenuPrincipal}  30
    Click Element  ${MainMenu.btnMenuPrincipal}
    
    Wait Until Element Is Visible  ${MainMenu.btnCompliance}  30
    Click Element  ${MainMenu.btnCompliance}

    Wait Until Element Is Visible  //android.view.View[@content-desc="Compartilhe no WhatsApp"]/android.widget.TextView

    Wait Until Element Is Visible  //android.view.View[@content-desc="INICIAR CONVERSA"]/android.widget.TextView
    Click Element  //android.view.View[@content-desc="INICIAR CONVERSA"]/android.widget.TextView

    Wait Until Element Is Visible  //android.view.View[@content-desc="DOWNLOAD"]
    Click Element  //android.view.View[@content-desc="DOWNLOAD"]

    Stop Screen Recording
