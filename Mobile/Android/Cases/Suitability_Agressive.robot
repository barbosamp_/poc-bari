*** Keywords ***

Suitability_Agressive

    Wait Until Element Is Visible  ${MainMenu.btnMenuPrincipal}  30
    Click Element       ${MainMenu.btnMenuPrincipal}
    
    Wait Until Element Is Visible  ${MainMenu.btnPerfil}  30
    Click Element       ${MainMenu.btnPerfil}

    Wait Until Element Is Visible  ${pagePerfil.perfilDetail}  30

    Wait Until Element Is Visible  ${pagePerfil.btnRefazerTeste}  30
    Click Element       ${pagePerfil.btnRefazerTeste}

    Wait Until Element Is Visible  ${pageQuestions.lblAskingQuestion}  30
    Click Element  ${pageQuestions.btnThirdQuestion}

    Wait Until Element Is Visible  ${pageLogin.btncontinue}  30
    Click Element       ${pageLogin.btncontinue}

    Wait Until Element Is Visible  ${pageQuestions.lblAskingQuestion}  30
    Click Element  ${pageQuestions.btnSecondQuestion}

    Wait Until Element Is Visible  ${pageLogin.btncontinue}  30
    Click Element       ${pageLogin.btncontinue}

    Wait Until Element Is Visible  ${pageQuestions.lblAskingQuestion}  30
    Click Element  ${pageQuestions.btnThirdQuestion}

    Wait Until Element Is Visible  ${pageLogin.btncontinue}  30
    Click Element       ${pageLogin.btncontinue}

    Wait Until Element Is Visible  ${pageQuestions.lblAskingQuestion}  30
    Click Element  ${pageQuestions.btnSecondQuestion}

    Wait Until Element Is Visible  ${pageLogin.btncontinue}  30
    Click Element       ${pageLogin.btncontinue}

    Wait Until Element Is Visible  ${pageQuestions.lblAskingQuestion}  30
    Click Element  ${pageQuestions.btnFourthQuestion}

    Wait Until Element Is Visible  ${pageLogin.btncontinue}  30
    Click Element       ${pageLogin.btncontinue}

    Wait Until Element Is Visible  ${pageQuestions.lblMultipleQuestion}  30
    Click Element   ${pageQuestions.btnFirstQuestionSecond}
    Click Element  ${pageQuestions.btnSecondQuestionFirst}

    Scroll  ${pageQuestions.lblThirdQuestionSecond}  ${pageQuestions.lblMultipleQuestion}
    
    Click Element  ${pageQuestions.btnThirdQuestionFirst}

    Wait Until Element Is Visible  ${pageQuestions.lblFourthMultipleQuestion}  30
    Click Element  ${pageQuestions.btnFourthQuestionFirst}
    Click Element  ${pageQuestions.btnFifthQuestionSecond}

    Wait Until Element Is Visible  ${pageLogin.btncontinue}  30
    Click Element       ${pageLogin.btncontinue}

    Wait Until Element Is Visible  ${pageQuestions.lblAskingQuestion}  50
    Click Element  ${pageQuestions.btnThirdQuestion}

    Wait Until Element Is Visible  ${pageLogin.btncontinue}  30
    Click Element       ${pageLogin.btncontinue}

    Wait Until Element Is Visible  ${pagePerfil.perfilDetail}  30
    Element Text Should Be  ${pagePerfil.perfilDetail}  Busca rendimentos mais vantajosos tolerando um certo risco mantendo o foco na preservação do capital no longo prazo.
    
    Wait Until Element Is Visible  ${pageLogin.btncontinue}  30
    Click Element       ${pageLogin.btncontinue}

    Wait Until Element Is Visible  //androidx.cardview.widget.CardView[1]/android.view.ViewGroup
    Wait Until Element Is Visible  //android.view.ViewGroup/androidx.cardview.widget.CardView

    Stop Screen Recording