*** Keywords ***

My_Investments    
    [Arguments]  ${ITEM}

    ${idViewGroup}  evaluate  ${ITEM}+1

    Wait Until Element Is Visible  ${pageHome.iconInvestimentos}  30
    Click Element  ${pageHome.iconInvestimentos}

    Wait Until Element Is Visible  //androidx.cardview.widget.CardView[1]/android.view.ViewGroup
    Wait Until Element Is Visible  //android.view.ViewGroup/androidx.cardview.widget.CardView

    Wait Until Element Is Visible  ${pageDashboardInvestimentos.btnViewBalance}  30
    Element Text Should Be  ${pageDashboardInvestimentos.btnViewBalance}  Mostrar saldo
    Click Element  ${pageDashboardInvestimentos.btnViewBalance}

    Verification user balance graphic  ${cpf}  ${senha}

    ${valueFrontInitialInvestment}  Get Text  ${pageDashboardInvestimentos.lblInicitalInvestment}    
    ${str}  Remove String  ${valueFrontInitialInvestment}  R$  .  ,
    ${valueFrontInitialInvestmentStripp}  Strip String  ${str}
    Should Be Equal As Integers   ${valueFrontInitialInvestmentStripp}  ${investmentValueTotal}

    ${valueFrontBalanceTotal}  Get Text  ${pageDashboardInvestimentos.lblViewBalanceValue}
    ${str}  Remove String  ${valueFrontBalanceTotal}  R$  .  ,
    ${valueFrontBalanceTotalStripp}  Strip String  ${str}
    Should Be Equal As Integers  ${valueFrontBalanceTotalStripp}  ${balanceTotal}

    ${valueFrontGrossValueTotal}  Get Text  ${pageDashboardInvestimentos.ViewGrossEarnings}
    ${str}  Remove String  ${valueFrontGrossValueTotal}  R$  .  ,
    ${valueFrontGrossValueTotalStripp}  Strip String  ${str}
    Should Be Equal As Integers  ${valueFrontGrossValueTotalStripp}  ${grossEarningsTotal}

    Wait Until Element Is Visible  ${pageDashboardInvestimentos.btnMeusInvestmentos}  30
    Element Text Should Be  ${pageDashboardInvestimentos.lblBtnMeusInvestmentos}  Meus investimentos
    Click Element  ${pageDashboardInvestimentos.btnMeusInvestmentos}

    Wait Until Element Is Visible  //android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]

    View Withdraw False  ${ITEM}

    ${valueFrontTitleDescriptionWithdraw}  Get Text  ${validationCardsResgate.lblTitleCardWithdraw}    
    Should Be Equal As Strings   ${valueFrontTitleDescriptionWithdraw}  ${valueTitleDescriptionWithdraw}

    Element Text Should Be  ${validationCardsResgate.lblTitleInvestimentoWithdraw}  Investimento
    ${valueFrontInvestmentWithdraw}  Get Text  ${validationCardsResgate.lblValueInvestimentoWithdraw}
    ${str}  Remove String  ${valueFrontInvestmentWithdraw}  R$  .  ,
    ${valueFrontInvestmentWithdraw}  Strip String  ${str} 
    Should Be Equal As Integers   ${valueFrontInvestmentWithdraw}  ${valueWithdrawInvestmentValue}

    Element Text Should Be  ${validationCardsResgate.lblTitleRentabilidadeWithdraw}  Rentabilidade
    ${valueFrontRentabilidade}  Get Text  ${validationCardsResgate.lblValueRentabilidadeWithdraw}    
    Should Be Equal As Strings   ${valueFrontRentabilidade}  ${valueWithdrawProfitability}

    Element Text Should Be  ${validationCardsResgate.lblTitleDataAplicacaoWithdraw}  Data da aplicação
    ${valueFrontDataAplicacaoWithdraw}  Get Text  ${validationCardsResgate.lblValueDataAplicacaoWithdraw}
    ${valueWithdrawEmissionDate}  Convert Date  ${valueWithdrawEmissionDate}  datetime  date_format=%Y-%m-%d
    ${valueFrontDataAplicacaoWithdraw}  Convert Date  ${valueFrontDataAplicacaoWithdraw}  datetime  date_format=%d/%m/%Y
    Should Be Equal  ${valueFrontDataAplicacaoWithdraw}  ${valueWithdrawEmissionDate}  

    Element Text Should Be  ${validationCardsResgate.lblTitleVencimentoWithdraw}  Vencimento
    ${ValueFrontVencimentoWithdraw}  Get Text  ${validationCardsResgate.lblValueVencimentoWithdraw}
    ${valueWithdrawMaturityDate}  Convert Date  ${valueWithdrawMaturityDate}  datetime  date_format=%Y-%m-%d
    ${ValueFrontVencimentoWithdraw}  Convert Date  ${ValueFrontVencimentoWithdraw}  datetime  date_format=%d/%m/%Y
    Should Be Equal  ${ValueFrontVencimentoWithdraw}  ${valueWithdrawMaturityDate}

    Click Element  //androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[${idViewGroup}]

    Wait Until Element Is Visible  ${pageMyInvestments.lblTitleDetalhesDoInvestimento}  30
    Element Text Should Be  ${pageMyInvestments.lblTitleDetalhesDoInvestimento}  Detalhes do investimento
    
    Wait Until Element Is Visible  //androidx.cardview.widget.CardView[1]/android.widget.LinearLayout

    View Withdraw Detail False  ${ITEM}

    Element Text Should Be  ${pageMyInvestments.lblTitleInvestments}  Investimento
    ${valueFrontMyDetailInvestments}  Get Text  ${pageMyInvestments.lblValueMyInvestmentsDetailInvestimento}
    ${str}  Remove String  ${valueFrontMyDetailInvestments}  R$  .  ,
    ${valueFrontMyDetailInvestments}  Strip String  ${str} 
    Should Be Equal As Integers   ${valueFrontMyDetailInvestments}  ${valueWithdrawInvestmentValue}

    Element Text Should Be  ${pageMyInvestments.lblFrontRentabilidade}  Rentabilidade
    ${valueFrontMyDetailInvestmentsRentabilidade}  Get Text  ${pageMyInvestments.lblValueMyInvestmentsDetailRentabilidade}    
    Should Be Equal As Strings   ${valueFrontMyDetailInvestmentsRentabilidade}  ${valueWithdrawProfitability}

    Wait Until Element Is Visible  ${pageMyInvestments.lblFrontIOF}  30
    Scroll  ${pageMyInvestments.lblFrontIOF}  ${pageDetailMyInvestment.lblTitle} 

    Element Text Should Be  ${pageMyInvestments.lblFrontDataAp}  Data da aplicação
    ${valueMyInvestmentDtAplicacao}  Get Text  ${pageMyInvestments.lblValueMyInvestmentsDetailDataAp}
    ${valueWithdrawEmissionDate}  Convert Date  ${valueWithdrawEmissionDate}  result_format=%d.%m.%Y
    @{words}  Split String  ${valueMyInvestmentDtAplicacao}  ${SPACE}
    ${numMes}  Dealing with dates with texts  ${words[2]}
    ${dtCompare}  catenate  ${words[0]}.${numMes}.${words[4]}
    Should Be Equal As Strings  ${valueWithdrawEmissionDate}  ${dtCompare}

    Element Text Should Be  ${pageMyInvestments.lblFrontVencimento}  Vencimento
    ${valueMyInvestmentVencimento}  Get Text  ${pageMyInvestments.lblValueMyInvestmentsDetailVencimento}
    ${valueMaturityDateDetailWithdraw}  Convert Date  ${valueMaturityDateDetailWithdraw}  result_format=%d.%m.%Y
    @{words}  Split String  ${valueMyInvestmentVencimento}  ${SPACE}
    ${numMes}  Dealing with dates with texts  ${words[2]}
    ${dtCompare}  catenate  ${words[0]}.${numMes}.${words[4]}
    Should Be Equal As Strings  ${valueMaturityDateDetailWithdraw}  ${dtCompare}

    Element Text Should Be  ${pageMyInvestments.lblFrontResgate}  Liquidez
    ${valueFrontMyDetailInvestmentsResgate}  Get Text  ${pageMyInvestments.lblValueMyInvestmentsDetailResgate}    
    Should Be Equal As Strings   ${valueFrontMyDetailInvestmentsResgate}  ${valueWithdrawLiquidity}

    Element Text Should Be  ${pageMyInvestments.lblFrontIOF}  IOF sobre o rendimento
    ${valueFrontMyDetailInvestmentsIOF}  Get Text  ${pageMyInvestments.lblValueMyInvestmentsDetailIOF}
    ${str}  Remove String  ${valueFrontMyDetailInvestmentsIOF}  R$  .  ,
    ${valueFrontMyDetailInvestmentsIOF}  Strip String  ${str}
    Convert To Integer  ${valueFrontMyDetailInvestmentsIOF}
    Convert To Integer  ${valueIOFDetailWithdraw}
    Should Be Equal As Integers   ${valueFrontMyDetailInvestmentsIOF}  ${valueIOFDetailWithdraw}

    Element Text Should Be  ${pageMyInvestments.lblFrontTaxaAdm}  Taxa administrativa
    ${valueFrontMyDetailInvestmentsTaxaADM}  Get Text  ${pageMyInvestments.lblValueMyInvestmentsDetailTaxa}
    ${str}  Remove String  ${valueFrontMyDetailInvestmentsTaxaADM}  %
    ${valueFrontMyDetailInvestmentsTaxaADM}  Strip String  ${str}
    Should Be Equal As Strings  ${valueFrontMyDetailInvestmentsTaxaADM}  ${valueAdministrativeTax}

    Element Text Should Be  ${pageMyInvestments.lblFrontEmissor}  Emissor
    ${valueFrontMyDetailInvestmentsValueEmissor}  Get Text  ${pageMyInvestments.lblValueMyInvestmentsDetailEmissor}
    Should Be Equal As Strings  ${valueFrontMyDetailInvestmentsValueEmissor}  ${valueIssuer}

    Element Text Should Be  ${pageMyInvestments.lblFrontRating}  Rating
    ${valueFrontMyDetailInvestmentsValueRating}  Get Text  ${pageMyInvestments.lblValueMyInvestmentsDetailRating}
    Should Be Equal As Strings  ${valueFrontMyDetailInvestmentsValueRating}  ${valueRatingValue}

    Wait Until Element Is Visible  ${pageDetailMyInvestment.btnBackDetailInvestment}  30
    Click Element       ${pageDetailMyInvestment.btnBackDetailInvestment}

    Wait Until Element Is Visible  //android.view.ViewGroup[3]/android.widget.TextView[1]  30

    Wait Until Element Is Visible  ${pageMyInvestments.btnBackMyInvestments}  30
    Click Element       ${pageMyInvestments.btnBackMyInvestments}

    Wait Until Element Is Visible  //androidx.cardview.widget.CardView[1]/android.view.ViewGroup
    Wait Until Element Is Visible  //android.view.ViewGroup/androidx.cardview.widget.CardView

    Wait Until Element Is Visible  //android.view.ViewGroup/android.widget.FrameLayout  30
    Click Element       //android.view.ViewGroup/android.widget.FrameLayout