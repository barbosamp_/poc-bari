*** Keywords ***

OrderPaper_Privacy
    [Arguments]  ${ITEM}

    Start Screen Recording

    Wait Until Element Is Visible  ${pageHome.iconInvestimentos}  30
    Click Element  ${pageHome.iconInvestimentos}

    Wait Until Element Is Visible  //androidx.cardview.widget.CardView[1]/android.view.ViewGroup
    Wait Until Element Is Visible  //android.view.ViewGroup/androidx.cardview.widget.CardView
    Wait Until Element Is Visible  //android.widget.FrameLayout/android.widget.ImageView

    Wait Until Element Is Visible  ${pageDashboardInvestimentos.btnViewBalance}  30
    Element Text Should Be  ${pageDashboardInvestimentos.btnViewBalance}  Mostrar saldo
    Click Element  ${pageDashboardInvestimentos.btnViewBalance}

    Verification user balance graphic  ${cpf}  ${senha}

    ${valueFrontInitialInvestment}  Get Text  ${pageDashboardInvestimentos.lblInicitalInvestment}    
    ${str}  Remove String  ${valueFrontInitialInvestment}  R$  .  ,
    ${valueFrontInitialInvestmentStripp}  Strip String  ${str}
    Should Be Equal As Integers   ${valueFrontInitialInvestmentStripp}  ${investmentValueTotal}

    ${valueFrontBalanceTotal}  Get Text  ${pageDashboardInvestimentos.lblViewBalanceValue}
    ${str}  Remove String  ${valueFrontBalanceTotal}  R$  .  ,
    ${valueFrontBalanceTotalStripp}  Strip String  ${str}
    Should Be Equal As Integers  ${valueFrontBalanceTotalStripp}  ${balanceTotal}

    ${valueFrontGrossValueTotal}  Get Text  ${pageDashboardInvestimentos.ViewGrossEarnings}
    ${str}  Remove String  ${valueFrontGrossValueTotal}  R$  .  ,
    ${valueFrontGrossValueTotalStripp}  Strip String  ${str}
    Should Be Equal As Integers  ${valueFrontGrossValueTotalStripp}  ${grossEarningsTotal}
    
    Update PriorityPaperParameters  1  180  101  Lydians
    Update PriorityPaperParameters  2  365  101  Lydians
    Update PriorityPaperParameters  3  180  110  Lydians
    Update PriorityPaperParameters  4  365  110  Lydians
    
    Element Text Should Be  ${pageDashboardInvestimentos.btnInvestDashboard}  Investir
    Click Element  ${pageDashboardInvestimentos.btnInvestDashboard}

    Wait Until Element Is Visible  ${validationRendaFixa.lblTitlePageRendaFixa}  30
    Element Text Should Be  ${validationRendaFixa.lblTitlePageRendaFixa}  Renda Fixa

    ${type}  Set Variable  CDB
    Enable list of papers  ${type}  ${ITEM}

    ${valueFrontTitleCardRendaFixa}  Get Text  ${validationRendaFixa.lblTitleCardRendaFixa}
    Should Be Equal As Strings  ${valueFrontTitleCardRendaFixa}  ${description}

    ${valueFrontRendaFixaValueRentabilidade}  Get Text  ${validationRendaFixa.lblRendaFixaValueRentabilidade}
    Should Be Equal As Strings  ${valueFrontRendaFixaValueRentabilidade}  ${profitability}

    ${valueFrontRendaFixaInvestimentoMin}  Get Text  ${validationRendaFixa.lblRendaFixaValueInvestmentoMin}
    ${str}  Remove String  ${valueFrontRendaFixaInvestimentoMin}  R$  .  ,
    ${valueFrontRendaFixaInvestimentoMin}  Strip String  ${str}
    Should Be Equal As Integers  ${valueFrontRendaFixaInvestimentoMin}  ${minimumInvestmentValue}

    ${valueFrontRendaFixaVencimento}  Get Text  ${validationRendaFixa.lblRendaFixaValueVencimento}
    ${maturityDate}  Convert Date  ${maturityDate}  datetime  date_format=%Y-%m-%d
    ${valueFrontRendaFixaVencimento}  Convert Date  ${valueFrontRendaFixaVencimento}  datetime  date_format=%d/%m/%Y
    Should Be Equal  ${valueFrontRendaFixaVencimento}  ${maturityDate}

    Wait Until Element Is Visible  ${validationRendaFixa.lblTitleCardRendaFixa}  30 

    Wait Until Element Is Visible  ${pageListOfPapers.lblFilterLCI}  30
    Click Element  ${pageListOfPapers.lblFilterLCI}

    Wait Until Element Is Visible  ${validationRendaFixa.lblTitleCardRendaFixa}  50
    Wait Until Element Is Visible  //android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]

    ${type}  Set Variable  LCI
    Enable list of papers  ${type}  ${ITEM}

    ${valueFrontTitleCardRendaFixa}  Get Text  ${validationRendaFixa.lblTitleCardRendaFixa}
    Should Be Equal As Strings  ${valueFrontTitleCardRendaFixa}  ${description}

    ${valueFrontRendaFixaValueRentabilidade}  Get Text  ${validationRendaFixa.lblRendaFixaValueRentabilidade}
    Should Be Equal As Strings  ${valueFrontRendaFixaValueRentabilidade}  ${profitability}

    ${valueFrontRendaFixaInvestimentoMin}  Get Text  ${validationRendaFixa.lblRendaFixaValueInvestmentoMin}
    ${str}  Remove String  ${valueFrontRendaFixaInvestimentoMin}  R$  .  ,
    ${valueFrontRendaFixaInvestimentoMin}  Strip String  ${str}
    Should Be Equal As Integers  ${valueFrontRendaFixaInvestimentoMin}  ${minimumInvestmentValue}

    ${valueFrontRendaFixaVencimento}  Get Text  ${validationRendaFixa.lblRendaFixaValueVencimento}
    ${maturityDate}  Convert Date  ${maturityDate}  datetime  date_format=%Y-%m-%d
    ${valueFrontRendaFixaVencimento}  Convert Date  ${valueFrontRendaFixaVencimento}  datetime  date_format=%d/%m/%Y
    Should Be Equal  ${valueFrontRendaFixaVencimento}  ${maturityDate}

    Wait Until Element Is Visible  ${pageListOfPapers.btnBackListToDashboard}  30
    Click Element  ${pageListOfPapers.btnBackListToDashboard}

    Wait Until Element Is Visible  //androidx.cardview.widget.CardView[1]/android.view.ViewGroup
    Wait Until Element Is Visible  //android.view.ViewGroup/androidx.cardview.widget.CardView
    Wait Until Element Is Visible  //android.widget.FrameLayout/android.widget.ImageView

    Wait Until Element Is Visible  ${pageDashboardInvestimentos.btnViewBalance}  30
    Element Text Should Be  ${pageDashboardInvestimentos.btnViewBalance}  Mostrar saldo

    Stop Screen Recording


