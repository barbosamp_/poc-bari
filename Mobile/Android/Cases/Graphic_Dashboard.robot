*** Keywords ***

Graphic_Persona_Four_Investments

    Start Screen Recording

    Wait Until Element Is Visible  ${pageHome.iconInvestimentos}  30
    Click Element  ${pageHome.iconInvestimentos}

    Wait Until Element Is Visible  //androidx.cardview.widget.CardView[1]/android.view.ViewGroup
    Wait Until Element Is Visible  //android.view.ViewGroup/androidx.cardview.widget.CardView
    Wait Until Element Is Visible  //android.widget.FrameLayout/android.widget.ImageView

    Wait Until Element Is Visible  ${pageDashboardInvestimentos.btnViewBalance}  30
    Element Text Should Be  ${pageDashboardInvestimentos.btnViewBalance}  Mostrar saldo
    Click Element  ${pageDashboardInvestimentos.btnViewBalance}

    Verification user balance graphic  ${cpf}  ${senha}

    ${valueFrontInitialInvestment}  Get Text  ${pageDashboardInvestimentos.lblInicitalInvestment}    
    ${str}  Remove String  ${valueFrontInitialInvestment}  R$  .  ,
    ${valueFrontInitialInvestmentStripp}  Strip String  ${str}
    Should Be Equal As Integers   ${valueFrontInitialInvestmentStripp}  ${investmentValueTotal}

    ${valueFrontBalanceTotal}  Get Text  ${pageDashboardInvestimentos.lblViewBalanceValue}
    ${str}  Remove String  ${valueFrontBalanceTotal}  R$  .  ,
    ${valueFrontBalanceTotalStripp}  Strip String  ${str}
    Should Be Equal As Integers  ${valueFrontBalanceTotalStripp}  ${balanceTotal}

    ${valueFrontGrossValueTotal}  Get Text  ${pageDashboardInvestimentos.ViewGrossEarnings}
    ${str}  Remove String  ${valueFrontGrossValueTotal}  R$  .  ,
    ${valueFrontGrossValueTotalStripp}  Strip String  ${str}
    Should Be Equal As Integers  ${valueFrontGrossValueTotalStripp}  ${grossEarningsTotal}

    Wait Until Element Is Visible  ${pageDashboardInvestimentos.expandCard}  30
    Click Element  ${pageDashboardInvestimentos.expandCard}

    ${valueFrontTitleProductCDB}  Get Text  //androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]/android.widget.TextView[1]
    Should Be Equal As Strings  ${valueFrontTitleProductCDB}  ${customerTypeCDB}

    ${valueFrontInvestmentProductCDB}  Get Text  //androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]/android.widget.TextView[2]
    ${str}  Remove String  ${valueFrontInvestmentProductCDB}  R$  .  ,
    ${valueFrontInvestmentProductCDB}  Strip String  ${str}
    Should Be Equal As Strings  ${valueFrontInvestmentProductCDB}  ${customerBalanceCDB}

    ${valueFrontTitleProductLCI}  Get Text  //androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[2]/android.widget.TextView[1]
    Should Be Equal As Strings  ${valueFrontTitleProductLCI}  ${customerTypeLCI}

    ${valueFrontInvestmentProductLCI}  Get Text  //androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[2]/android.widget.TextView[2]
    ${str}  Remove String  ${valueFrontInvestmentProductLCI}  R$  .  ,
    ${valueFrontInvestmentProductLCI}  Strip String  ${str}
    Should Be Equal As Strings  ${valueFrontInvestmentProductLCI}  ${customerBalanceLCI}

    Wait Until Element Is Visible  //androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]
    Click Element  //androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]

    ${valueFrontSelectInvestmentCDB}  Get Text  //androidx.cardview.widget.CardView/android.view.ViewGroup/android.widget.TextView[2]
    ${str}  Remove String  ${valueFrontSelectInvestmentCDB}  R$  .  ,
    ${valueFrontSelectInvestmentCDB}  Strip String  ${str}
    Should Be Equal As Strings  ${valueFrontSelectInvestmentCDB}  ${customerBalanceCDB}

    ${valueFrontRentabilityCDB}  Get Text  //android.view.ViewGroup/android.widget.TextView[4]
    ${str}  Remove String  ${valueFrontRentabilityCDB}  R$  .  ,
    ${valueFrontRentabilityCDB}  Strip String  ${str}
    Should Be Equal As Strings  ${valueFrontRentabilityCDB}  ${customerGrossEarningsCDB}

    Wait Until Element Is Visible  //androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[2]
    Click Element  //androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[2]

    ${valueFrontSelectInvestmentLCI}  Get Text  //androidx.cardview.widget.CardView/android.view.ViewGroup/android.widget.TextView[2]
    ${str}  Remove String  ${valueFrontSelectInvestmentLCI}  R$  .  ,
    ${valueFrontSelectInvestmentLCI}  Strip String  ${str}
    Should Be Equal As Strings  ${valueFrontSelectInvestmentLCI}  ${customerBalanceLCI}

    ${valueFrontRentabilityLCI}  Get Text  //android.view.ViewGroup/android.widget.TextView[4]
    ${str}  Remove String  ${valueFrontRentabilityLCI}  R$  .  ,
    ${valueFrontRentabilityLCI}  Strip String  ${str}
    Should Be Equal As Strings  ${valueFrontRentabilityLCI}  ${customerGrossEarningsLCI}

    Wait Until Element Is Visible  //androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[3]
    Click Element  //androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[3]

    Wait Until Element Is Visible  //androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[4]
    Click Element  //androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[4]

    Wait Until Element Is Visible  ${pageDashboardInvestimentos.clearSelect}  30
    Click Element  ${pageDashboardInvestimentos.clearSelect}

    Wait Until Element Is Visible  ${pageDashboardInvestimentos.expandCard}  30
    Click Element  ${pageDashboardInvestimentos.expandCard}

    Wait Until Element Is Visible  ${pageDashboardInvestimentos.btnOcultEye}  60
    Click Element  ${pageDashboardInvestimentos.btnOcultEye}

    Wait Until Element Is Visible  ${pageDashboardInvestimentos.btnViewBalance}  30
    Element Text Should Be  ${pageDashboardInvestimentos.btnViewBalance}  Mostrar saldo

    Stop Screen Recording


Graphic_Persona_Investments_CDB

    Start Screen Recording

    Wait Until Element Is Visible  ${pageHome.iconInvestimentos}  30
    Click Element  ${pageHome.iconInvestimentos}

    Wait Until Element Is Visible  //androidx.cardview.widget.CardView[1]/android.view.ViewGroup
    Wait Until Element Is Visible  //android.view.ViewGroup/androidx.cardview.widget.CardView
    Wait Until Element Is Visible  //android.widget.FrameLayout/android.widget.ImageView

    Wait Until Element Is Visible  ${pageDashboardInvestimentos.btnViewBalance}  30
    Element Text Should Be  ${pageDashboardInvestimentos.btnViewBalance}  Mostrar saldo
    Click Element  ${pageDashboardInvestimentos.btnViewBalance}

    ${cpf}  Set Variable  09699387998
    ${senha}  Set Variable  123Abc
    Verification user balance graphic general  ${cpf}  ${senha}

    ${valueFrontInitialInvestment}  Get Text  ${pageDashboardInvestimentos.lblInicitalInvestment}    
    ${str}  Remove String  ${valueFrontInitialInvestment}  R$  .  ,
    ${valueFrontInitialInvestmentStripp}  Strip String  ${str}
    Should Be Equal As Integers   ${valueFrontInitialInvestmentStripp}  ${investmentValueTotal}

    ${valueFrontBalanceTotal}  Get Text  ${pageDashboardInvestimentos.lblViewBalanceValue}
    ${str}  Remove String  ${valueFrontBalanceTotal}  R$  .  ,
    ${valueFrontBalanceTotalStripp}  Strip String  ${str}
    Should Be Equal As Integers  ${valueFrontBalanceTotalStripp}  ${balanceTotal}

    ${valueFrontGrossValueTotal}  Get Text  ${pageDashboardInvestimentos.ViewGrossEarnings}
    ${str}  Remove String  ${valueFrontGrossValueTotal}  R$  .  ,
    ${valueFrontGrossValueTotalStripp}  Strip String  ${str}
    Should Be Equal As Integers  ${valueFrontGrossValueTotalStripp}  ${grossEarningsTotal}

    Wait Until Element Is Visible  ${pageDashboardInvestimentos.expandCard}  30
    Click Element  ${pageDashboardInvestimentos.expandCard}

    ${valueFrontTitleProductCDB}  Get Text  //androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]/android.widget.TextView[1]
    Should Be Equal As Strings  ${valueFrontTitleProductCDB}  ${customerType}

    ${valueFrontInvestmentProductCDB}  Get Text  //androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]/android.widget.TextView[2]
    ${str}  Remove String  ${valueFrontInvestmentProductCDB}  R$  .  ,
    ${valueFrontInvestmentProductCDB}  Strip String  ${str}
    Should Be Equal As Strings  ${valueFrontInvestmentProductCDB}  ${customerBalance}

    Wait Until Element Is Visible  //androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]
    Click Element  //androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]

    ${valueFrontSelectInvestmentCDB}  Get Text  //androidx.cardview.widget.CardView/android.view.ViewGroup/android.widget.TextView[2]
    ${str}  Remove String  ${valueFrontSelectInvestmentCDB}  R$  .  ,
    ${valueFrontSelectInvestmentCDB}  Strip String  ${str}
    Should Be Equal As Strings  ${valueFrontSelectInvestmentCDB}  ${customerBalance}

    ${valueFrontRentabilityCDB}  Get Text  //android.view.ViewGroup/android.widget.TextView[4]
    ${str}  Remove String  ${valueFrontRentabilityCDB}  R$  .  ,
    ${valueFrontRentabilityCDB}  Strip String  ${str}
    Convert To Integer  ${valueFrontRentabilityCDB}
    Convert To Integer  ${customerGrossEarnings}
    Should Be Equal As Integers  ${valueFrontRentabilityCDB}  ${customerGrossEarnings}
    
    Wait Until Element Is Visible  ${pageDashboardInvestimentos.lblInfoNewInvestmentDash}  30
    Element Text Should Be  ${pageDashboardInvestimentos.lblInfoNewInvestmentDash}  O que acha de conhecer novas oportunidades de investimentos?

    Wait Until Element Is Visible  ${pageDashboardInvestimentos.btnKnowMore}  30
    Click Element  ${pageDashboardInvestimentos.btnKnowMore}

    Wait Until Element Is Visible  //android.view.ViewGroup/androidx.cardview.widget.CardView  30
    Wait Until Element Is Visible  ${pageListOfPapers.btnBackListToDashboard}  30
    Click Element  ${pageListOfPapers.btnBackListToDashboard}

    Wait Until Element Is Visible  ${pageDashboardInvestimentos.btnOcultEye}  60
    Click Element  ${pageDashboardInvestimentos.btnOcultEye}

    Wait Until Element Is Visible  ${pageDashboardInvestimentos.btnViewBalance}  30
    Element Text Should Be  ${pageDashboardInvestimentos.btnViewBalance}  Mostrar saldo

    Stop Screen Recording


Graphic_Persona_Investments_LCI

    Start Screen Recording

    Wait Until Element Is Visible  ${pageHome.iconInvestimentos}  30
    Click Element  ${pageHome.iconInvestimentos}

    Wait Until Element Is Visible  //androidx.cardview.widget.CardView[1]/android.view.ViewGroup
    Wait Until Element Is Visible  //android.view.ViewGroup/androidx.cardview.widget.CardView
    Wait Until Element Is Visible  //android.widget.FrameLayout/android.widget.ImageView

    Wait Until Element Is Visible  ${pageDashboardInvestimentos.btnViewBalance}  30
    Element Text Should Be  ${pageDashboardInvestimentos.btnViewBalance}  Mostrar saldo
    Click Element  ${pageDashboardInvestimentos.btnViewBalance}

    ${cpf}  Set Variable  10146495950
    ${senha}  Set Variable  123Abc
    Verification user balance graphic general  ${cpf}  ${senha}

    ${valueFrontInitialInvestment}  Get Text  ${pageDashboardInvestimentos.lblInicitalInvestment}    
    ${str}  Remove String  ${valueFrontInitialInvestment}  R$  .  ,
    ${valueFrontInitialInvestmentStripp}  Strip String  ${str}
    Should Be Equal As Integers   ${valueFrontInitialInvestmentStripp}  ${investmentValueTotal}

    ${valueFrontBalanceTotal}  Get Text  ${pageDashboardInvestimentos.lblViewBalanceValue}
    ${str}  Remove String  ${valueFrontBalanceTotal}  R$  .  ,
    ${valueFrontBalanceTotalStripp}  Strip String  ${str}
    Should Be Equal As Integers  ${valueFrontBalanceTotalStripp}  ${balanceTotal}

    ${valueFrontGrossValueTotal}  Get Text  ${pageDashboardInvestimentos.ViewGrossEarnings}
    ${str}  Remove String  ${valueFrontGrossValueTotal}  R$  .  ,
    ${valueFrontGrossValueTotalStripp}  Strip String  ${str}
    Should Be Equal As Integers  ${valueFrontGrossValueTotalStripp}  ${grossEarningsTotal}

    Wait Until Element Is Visible  ${pageDashboardInvestimentos.expandCard}  30
    Click Element  ${pageDashboardInvestimentos.expandCard}

    ${valueFrontTitleProductLCI}  Get Text  //androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]/android.widget.TextView[1]
    Should Be Equal As Strings  ${valueFrontTitleProductLCI}  ${customerType}

    ${valueFrontInvestmentProductLCI}  Get Text  //androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]/android.widget.TextView[2]
    ${str}  Remove String  ${valueFrontInvestmentProductLCI}  R$  .  ,
    ${valueFrontInvestmentProductLCI}  Strip String  ${str}
    Should Be Equal As Strings  ${valueFrontInvestmentProductLCI}  ${customerBalance}

    Wait Until Element Is Visible  //androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]
    Click Element  //androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]

    ${valueFrontSelectInvestmentLCI}  Get Text  //androidx.cardview.widget.CardView/android.view.ViewGroup/android.widget.TextView[2]
    ${str}  Remove String  ${valueFrontSelectInvestmentLCI}  R$  .  ,
    ${valueFrontSelectInvestmentLCI}  Strip String  ${str}
    Should Be Equal As Strings  ${valueFrontSelectInvestmentLCI}  ${customerBalance}

    ${valueFrontRentabilityLCI}  Get Text  //android.view.ViewGroup/android.widget.TextView[4]
    ${str}  Remove String  ${valueFrontRentabilityLCI}  R$  .  ,
    ${valueFrontRentabilityLCI}  Strip String  ${str}
    Convert To Integer  ${valueFrontRentabilityLCI}
    Convert To Integer  ${customerGrossEarnings}
    Should Be Equal As Integers  ${valueFrontRentabilityLCI}  ${customerGrossEarnings}
    
    Wait Until Element Is Visible  ${pageDashboardInvestimentos.lblInfoNewInvestmentDash}  30
    Element Text Should Be  ${pageDashboardInvestimentos.lblInfoNewInvestmentDash}  O que acha de conhecer novas oportunidades de investimentos?

    Wait Until Element Is Visible  ${pageDashboardInvestimentos.btnKnowMore}  30
    Click Element  ${pageDashboardInvestimentos.btnKnowMore}

    Wait Until Element Is Visible  //android.view.ViewGroup/androidx.cardview.widget.CardView
    Wait Until Element Is Visible  ${pageListOfPapers.btnBackListToDashboard}  30
    Click Element  ${pageListOfPapers.btnBackListToDashboard}

    Wait Until Element Is Visible  ${pageDashboardInvestimentos.btnOcultEye}  60
    Click Element  ${pageDashboardInvestimentos.btnOcultEye}

    Wait Until Element Is Visible  ${pageDashboardInvestimentos.btnViewBalance}  30
    Element Text Should Be  ${pageDashboardInvestimentos.btnViewBalance}  Mostrar saldo

    Stop Screen Recording

Graphic_Persona_No_Investments

    Start Screen Recording

    Wait Until Element Is Visible  ${pageHome.iconInvestimentos}  30
    Click Element  ${pageHome.iconInvestimentos}

    Wait Until Element Is Visible  //androidx.cardview.widget.CardView[1]/android.view.ViewGroup
    Wait Until Element Is Visible  //android.view.ViewGroup/androidx.cardview.widget.CardView
    Wait Until Element Is Visible  //android.widget.FrameLayout/android.widget.ImageView

    ${cpf}  Set Variable  76432015871
    ${senha}  Set Variable  123Abc
    Verification user balance  ${cpf}  ${senha}

    ${valueFrontInitialInvestment}  Get Text  ${pageDashboardInvestimentos.lblInicitalInvestment}    
    ${str}  Remove String  ${valueFrontInitialInvestment}  R$  .  ,
    ${valueFrontInitialInvestmentStripp}  Strip String  ${str}
    Should Be Equal As Integers   ${valueFrontInitialInvestmentStripp}  ${investmentValueTotal}

    ${valueFrontBalanceTotal}  Get Text  ${pageDashboardInvestimentos.lblViewBalanceValue}
    ${str}  Remove String  ${valueFrontBalanceTotal}  R$  .  ,
    ${valueFrontBalanceTotalStripp}  Strip String  ${str}
    Should Be Equal As Integers  ${valueFrontBalanceTotalStripp}  ${balanceTotal}

    ${valueFrontGrossValueTotal}  Get Text  ${pageDashboardInvestimentos.ViewGrossEarnings}
    ${str}  Remove String  ${valueFrontGrossValueTotal}  R$  .  ,
    ${valueFrontGrossValueTotalStripp}  Strip String  ${str}
    Should Be Equal As Integers  ${valueFrontGrossValueTotalStripp}  ${grossEarningsTotal}

    Stop Screen Recording