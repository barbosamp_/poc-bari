*** Keywords ***

Apply_Transaction_Janela
    [Arguments]  ${ITEM}

    ${idViewGroup}  evaluate  ${ITEM}+1

    Update WindowTransaction  07:00  11:00

    Wait Until Element Is Visible  //androidx.cardview.widget.CardView[1]/android.view.ViewGroup
    Wait Until Element Is Visible  //android.view.ViewGroup/androidx.cardview.widget.CardView

    Wait Until Element Is Visible  ${pageDashboardInvestimentos.btnViewBalance}  30
    Element Text Should Be  ${pageDashboardInvestimentos.btnViewBalance}  Mostrar saldo
    Click Element  ${pageDashboardInvestimentos.btnViewBalance}

    Element Text Should Be  ${pageDashboardInvestimentos.btnInvestDashboard}  Investir
    Click Element  ${pageDashboardInvestimentos.btnInvestDashboard}

    Wait Until Element Is Visible  ${pageListOfPapers.lblCardType}  30
    Click Element  //androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[${idViewGroup}]

    Wait Until Element Is Visible  ${pageDetailAssets.lblIOFRendimentoScroll}  50
    Scroll  ${pageDetailAssets.lblIOFRendimentoScroll}   ${pageDetailAssets.lblTitleScroll}

    Wait Until Element Is Visible  ${validationDetalhamentoInvestimento.lblJanelaApplyTitle}  30
    Element Text Should Be  ${validationDetalhamentoInvestimento.lblJanelaApplyTitle}  Horário para investimentos 

    Wait Until Element Is Visible  ${validationDetalhamentoInvestimento.lblJanelaInfoApplyHour}  30
    ${valueFrontJanelaInfoApplyHour}  Get Text  ${validationDetalhamentoInvestimento.lblJanelaInfoApplyHour}
    ${contains}  evaluate  "09:00 às 22:00" in """${valueFrontJanelaInfoApplyHour}"""
    Log  ${contains}
    
    Wait Until Element Is Visible  ${pageDetailAssets.btnBackFilter}  30
    Click Element       ${pageDetailAssets.btnBackFilter}

    Wait Until Element Is Visible  ${pageListOfPapers.lblFilterLCI}  30
    Click Element       ${pageListOfPapers.lblFilterLCI}

    Wait Until Element Is Visible  ${pageListOfPapers.lblCardType}  30
    Click Element  //androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[${idViewGroup}]

    Wait Until Element Is Visible  ${pageDetailAssets.lblIOFRendimentoScroll}  30
    Scroll  ${pageDetailAssets.lblIOFRendimentoScroll}   ${pageDetailAssets.lblTitleScroll}

    Wait Until Element Is Visible  ${validationDetalhamentoInvestimento.lblJanelaApplyTitle}  30
    Element Text Should Be  ${validationDetalhamentoInvestimento.lblJanelaApplyTitle}  Horário para investimentos 

    Wait Until Element Is Visible  ${validationDetalhamentoInvestimento.lblJanelaInfoApplyHour}  30
    ${valueFrontJanelaInfoApplyHour}  Get Text  ${validationDetalhamentoInvestimento.lblJanelaInfoApplyHour}
    ${contains}  evaluate  "09:00 às 22:00" in """${valueFrontJanelaInfoApplyHour}"""
    Log  ${contains}

    Wait Until Element Is Visible  ${pageListOfPapers.btnBackListToDashboard}  30
    Click Element       ${pageListOfPapers.btnBackListToDashboard}

    Wait Until Element Is Visible  ${pageListOfPapers.btnBackListToDashboard}  30
    Click Element       ${pageListOfPapers.btnBackListToDashboard}


Withdraw_Transaction_Janela
    [Arguments]  ${ITEM}

    ${idViewGroup}  evaluate  ${ITEM}+1

    Wait Until Element Is Visible  //androidx.cardview.widget.CardView[1]/android.view.ViewGroup
    Wait Until Element Is Visible  //android.view.ViewGroup/androidx.cardview.widget.CardView

    Wait Until Element Is Visible  ${MainMenu.btnMenuPrincipal}  30
    Click Element       ${MainMenu.btnMenuPrincipal}
    
    Wait Until Element Is Visible  ${MainMenu.btnResgate}  30
    Click Element       ${MainMenu.btnResgate}

    Wait Until Element Is Visible  //android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]
    Click Element  //android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[${idViewGroup}]

    Wait Until Element Is Visible  ${validationCardsDetalhesResgate.lblDetailSaldoBruto}  30
    Scroll  ${validationCardsDetalhesResgate.lblDetailSaldoBruto}   ${validationCardsDetalhesResgate.lblTitleStartDetail}

    Wait Until Element Is Visible  ${validationCardsDetalhesResgate.lblTitleJanelaWithdraw}  30
    Element Text Should Be  ${validationCardsDetalhesResgate.lblTitleJanelaWithdraw}  Horário para resgate 

    Wait Until Element Is Visible  ${validationDetalhamentoInvestimento.lblJanelaInfoWithdrawHour}  30
    ${valueFrontJanelaWithdrawHour}  Get Text  ${validationDetalhamentoInvestimento.lblJanelaInfoWithdrawHour}
    ${contains}  evaluate  "09:00 às 22:00" in """${valueFrontJanelaWithdrawHour}"""
    Log  ${contains}

    Wait Until Element Is Visible  ${pageDetailWithdraw.btnBackDetailWithdrawJanela}  30
    Click Element       ${pageDetailWithdraw.btnBackDetailWithdrawJanela}

    Wait Until Element Is Visible  ${pageDetailWithdraw.btnBackWithdrawJanela}  30
    Click Element       ${pageDetailWithdraw.btnBackWithdrawJanela}

    Wait Until Element Is Visible  //androidx.cardview.widget.CardView/android.view.ViewGroup  30
    Click Element  //androidx.cardview.widget.CardView/android.view.ViewGroup

    Update WindowTransaction  07:00  22:00
