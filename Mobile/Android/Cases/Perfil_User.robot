*** Keywords ***

Perfil_User

    Wait Until Element Is Visible  ${MainMenu.btnMenuPrincipal}  30
    Click Element       ${MainMenu.btnMenuPrincipal}
    
    Wait Until Element Is Visible  ${MainMenu.btnPerfilMenu}  30
    Click Element       ${MainMenu.btnPerfilMenu}

    Wait Until Element Is Visible  ${pagePerfilMenu.titlePerfil}  30
    Element Text Should Be  ${pagePerfilMenu.titlePerfil}  Perfil

    Wait Until Element Is Visible  ${pagePerfilMenu.btnBackPerfil}  30
    Click Element       ${pagePerfilMenu.btnBackPerfil}

    Stop Screen Recording