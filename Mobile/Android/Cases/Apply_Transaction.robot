*** Keywords ***

Apply_Transaction_CDB
    [Arguments]  ${ITEM}

    ${idViewGroup}  evaluate  ${ITEM}+1

    Wait Until Element Is Visible  //androidx.cardview.widget.CardView[1]/android.view.ViewGroup
    Wait Until Element Is Visible  //android.view.ViewGroup/androidx.cardview.widget.CardView
    Wait Until Element Is Visible  //android.widget.FrameLayout/android.widget.ImageView

    Wait Until Element Is Visible  ${pageDashboardInvestimentos.btnViewBalance}  30
    Element Text Should Be  ${pageDashboardInvestimentos.btnViewBalance}  Mostrar saldo
    Click Element  ${pageDashboardInvestimentos.btnViewBalance}

    Verification user balance graphic  ${cpf}  ${senha}

    ${valueFrontInitialInvestment}  Get Text  ${pageDashboardInvestimentos.lblInicitalInvestment}    
    ${str}  Remove String  ${valueFrontInitialInvestment}  R$  .  ,
    ${valueFrontInitialInvestmentStripp}  Strip String  ${str}
    Should Be Equal As Integers   ${valueFrontInitialInvestmentStripp}  ${investmentValueTotal}

    ${valueFrontBalanceTotal}  Get Text  ${pageDashboardInvestimentos.lblViewBalanceValue}
    ${str}  Remove String  ${valueFrontBalanceTotal}  R$  .  ,
    ${valueFrontBalanceTotalStripp}  Strip String  ${str}
    Should Be Equal As Integers  ${valueFrontBalanceTotalStripp}  ${balanceTotal}

    ${valueFrontGrossValueTotal}  Get Text  ${pageDashboardInvestimentos.ViewGrossEarnings}
    ${str}  Remove String  ${valueFrontGrossValueTotal}  R$  .  ,
    ${valueFrontGrossValueTotalStripp}  Strip String  ${str}
    Should Be Equal As Integers  ${valueFrontGrossValueTotalStripp}  ${grossEarningsTotal}

    Element Text Should Be  ${pageDashboardInvestimentos.btnInvestDashboard}  Investir
    Click Element  ${pageDashboardInvestimentos.btnInvestDashboard}

    Wait Until Element Is Visible  ${validationRendaFixa.lblTitlePageRendaFixa}  30
    Element Text Should Be  ${validationRendaFixa.lblTitlePageRendaFixa}  Renda Fixa
    
    ${type}  Set Variable  CDB
    Enable list of papers  ${type}  ${ITEM}

    ${valueFrontTitleCardRendaFixa}  Get Text  ${validationRendaFixa.lblTitleCardRendaFixa}
    Should Be Equal As Strings  ${valueFrontTitleCardRendaFixa}  ${description}

    ${valueFrontRendaFixaValueRentabilidade}  Get Text  //android.view.ViewGroup[${idViewGroup}]/android.widget.TextView[4]
    Should Be Equal As Strings  ${valueFrontRendaFixaValueRentabilidade}  ${profitability}

    ${valueFrontRendaFixaInvestimentoMin}  Get Text  //android.view.ViewGroup[${idViewGroup}]/android.widget.TextView[7]
    ${str}  Remove String  ${valueFrontRendaFixaInvestimentoMin}  R$  .  ,
    ${valueFrontRendaFixaInvestimentoMin}  Strip String  ${str}
    Should Be Equal As Integers  ${valueFrontRendaFixaInvestimentoMin}  ${minimumInvestmentValue}

    ${valueFrontRendaFixaVencimento}  Get Text  //android.view.ViewGroup[${idViewGroup}]/android.widget.TextView[8]
    ${maturityDate}  Convert Date  ${maturityDate}  datetime  date_format=%Y-%m-%d
    ${valueFrontRendaFixaVencimento}  Convert Date  ${valueFrontRendaFixaVencimento}  datetime  date_format=%d/%m/%Y
    Should Be Equal  ${valueFrontRendaFixaVencimento}  ${maturityDate}

    Wait Until Element Is Visible  ${pageListOfPapers.mdlFGC}  30
    Element Text Should Be  ${validationRendaFixa.lblRendaFixaModal}  FGC
    Click Element  ${pageListOfPapers.mdlFGC}

    Wait Until Element Is Visible  ${pageListOfPapers.btnExitModalFGC}  30
    Click Element  ${pageListOfPapers.btnExitModalFGC}

    Wait Until Element Is Visible  ${pageListOfPapers.lblCardType}  30
    Click Element  //androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[${idViewGroup}]

    ${type}  Set Variable  CDB
    View investment details  ${type}  ${ITEM}

    Wait Until Element Is Visible  ${validationDetalhamentoInvestimento.lblTitlePageDetalhamentoInvestimento}  30
    Element Text Should Be  ${validationDetalhamentoInvestimento.lblTitlePageDetalhamentoInvestimento}  Detalhes do produto

    Wait Until Element Is Visible  ${validationDetalhamentoInvestimento.lblTitleDetalhamentoInvestimento}  30
    ${valueFrontTitleDetalhamento}  Get Text  ${validationDetalhamentoInvestimento.lblTitleDetalhamentoInvestimento}
    Should Be Equal As Strings  ${valueFrontTitleDetalhamento}  ${descriptionTitleDetail}

    Element Text Should Be  ${validationDetalhamentoInvestimento.lblDIRentabilidade}  Rentabilidade
    ${valueFrontProfitabilityDetail}  Get Text  ${validationDetalhamentoInvestimento.lblDIValueRentabilidade}
    Should Be Equal As Strings  ${valueFrontProfitabilityDetail}  ${profitabilityDetail}

    Element Text Should Be  ${validationDetalhamentoInvestimento.lblDIInvestimentoMin}  Investimento mínimo
    ${valueFrontDetailInvestimentoMin}  Get Text  ${validationDetalhamentoInvestimento.lblDIValueInvestimentoMin}
    ${str}  Remove String  ${valueFrontDetailInvestimentoMin}  R$  .  ,
    ${valueFrontDetailInvestimentoMin}  Strip String  ${str}
    Should Be Equal As Integers  ${valueFrontDetailInvestimentoMin}  ${minimumInvestmentValueDetail}

    Element Text Should Be  ${validationDetalhamentoInvestimento.lblDIVencimento}  Vencimento
    ${valueDetailVencimentoInvestment}  Get Text  ${validationDetalhamentoInvestimento.lblDIValueVencimento}
    ${maturityDate}  Convert Date  ${maturityDate}  result_format=%d.%m.%Y
    @{words}  Split String  ${valueDetailVencimentoInvestment}  ${SPACE}
    ${numMes}  Dealing with dates with texts  ${words[2]}
    ${dtCompare}  catenate  ${words[0]}.${numMes}.${words[4]}
    Should Be Equal As Strings  ${maturityDate}  ${dtCompare}

    Element Text Should Be  ${validationDetalhamentoInvestimento.lblDiasResgate}  Resgate
    ${valueFrontDiasResgate}  Get Text  ${validationDetalhamentoInvestimento.lblDiasValueResgate}
    ${str}  Remove String  ${valueFrontDiasResgate}  D+
    ${valueFrontDiasResgate}  Strip String  ${str}
    Should Be Equal As Strings  ${valueFrontDiasResgate}  ${liquidityDetail}

    Wait Until Element Is Visible  ${pageDetailAssets.mdlAssetsFGC}  30
    Click Element  ${pageDetailAssets.mdlAssetsFGC}

    Wait Until Element Is Visible  ${pageDetailAssets.btnExitDetailModalFGC}  30
    Click Element  ${pageDetailAssets.btnExitDetailModalFGC}

    Wait Until Element Is Visible  ${pageDetailAssets.btnInfoIR}  30
    Click Element  ${pageDetailAssets.btnInfoIR}

    Wait Until Element Is Visible  ${validationImpostoDeRenda.lblTitleImpostoDeRenda}  30
    Element Text Should Be  ${validationImpostoDeRenda.lblTitleImpostoDeRenda}  Imposto de renda

    Element Text Should Be  ${validationImpostoDeRenda.lblDescImpostoDeRenda}  O Imposto de Renda é calculado sobre os rendimentos de sua aplicação no resgate. A alíquota varia de acordo com o prazo.

    Element Text Should Be  ${validationImpostoDeRenda.lblPrazoImpostoDeRenda}  Prazo
    Element Text Should Be  ${validationImpostoDeRenda.lblPrazo180ImpostoDeRenda}  Até 180 dias
    Element Text Should Be  ${validationImpostoDeRenda.lblPrazo360ImpostoDeRenda}  De 181 a 360 dias
    Element Text Should Be  ${validationImpostoDeRenda.lblPrazo720ImpostoDeRenda}  De 361 a 720 dias
    Element Text Should Be  ${validationImpostoDeRenda.lblPrazoAcima720ImpostoDeRenda}  Acima de 720 dias

    Element Text Should Be  ${validationImpostoDeRenda.lblAliquotaImpostoDeRenda}  Alíquota
    Element Text Should Be  ${validationImpostoDeRenda.lblAliquota180ImpostoDeRenda}  22,50 %
    Element Text Should Be  ${validationImpostoDeRenda.lblAliquota360ImpostoDeRenda}  22,00 %
    Element Text Should Be  ${validationImpostoDeRenda.lblAliquota720ImpostoDeRenda}  17,50 %
    Element Text Should Be  ${validationImpostoDeRenda.lblAliquotaAcima720ImpostoDeRenda}  15,00 %

    Wait Until Element Is Visible  ${pageDetailAssets.btnBackInfoIR}  30
    Click Element  ${pageDetailAssets.btnBackInfoIR}

    Wait Until Element Is Visible  ${pageDetailAssets.btnBackInfoIR}  30
    Scroll  ${pageDetailAssets.lblIOFRendimentoScroll}   ${pageDetailAssets.lblTitleScroll}

    Wait Until Element Is Visible  ${validationDetalhamentoInvestimento.lblDIIRSobreRendimento}  30
    Element Text Should Be  ${validationDetalhamentoInvestimento.lblDIIRSobreRendimento}  IR sobre o rendimento
    Element Text Should Be  ${validationDetalhamentoInvestimento.lblDIValueIRSobreRendimento}  Regressivo

    Element Text Should Be  ${validationDetalhamentoInvestimento.lblDIIOF}  IOF sobre o rendimento
    Element Text Should Be  ${validationDetalhamentoInvestimento.lblDIValueIOF}  Isento após 30 dias

    Element Text Should Be  ${validationDetalhamentoInvestimento.lblDITaxaADM}  Taxa administrativa
    ${valueFrontDITaxaADM}  Get Text  ${validationDetalhamentoInvestimento.lblDIValueTaxaADM}
    ${str}  Remove String  ${valueFrontDITaxaADM}  %
    ${valueFrontDITaxaADM}  Strip String  ${str}
    Should Be Equal As Strings  ${valueFrontDITaxaADM}  ${administrativeTax}

    Element Text Should Be  ${validationDetalhamentoInvestimento.lblDIEmissor}  Emissor
    ${valueFrontDIValueEmissor}  Get Text  ${validationDetalhamentoInvestimento.lblDIValueEmissor}
    Should Be Equal As Strings  ${valueFrontDIValueEmissor}  ${issuerDetail}

    Element Text Should Be  ${validationDetalhamentoInvestimento.lblDIRating}  Rating
    ${valueFrontDIValueRating}  Get Text  ${validationDetalhamentoInvestimento.lblDIValueRating}
    Should Be Equal As Strings  ${valueFrontDIValueRating}  ${ratingValueDetail}

    Element Text Should Be  ${validationDetalhamentoInvestimento.lblDIProtecao}  Proteção
    Wait Until Element Is Visible  ${pageDetailAssets.btnGarantidoFGC}  30
    Element Text Should Be  ${validationDetalhamentoInvestimento.lblDIValueProtecao}  Garantido pelo FGC 
    Click Element  ${pageDetailAssets.btnGarantidoFGC}

    Wait Until Element Is Visible  ${pageDetailAssets.btnClosedGarantidoFGC}  30
    Click Element  ${pageDetailAssets.btnClosedGarantidoFGC}

    Wait Until Element Is Visible  ${pageDetailAssets.btnContinueAssets}  30
    Element Text Should Be  ${pageDetailAssets.btnContinueAssets}  Investir
    Click Element  ${pageDetailAssets.btnContinueAssets}

    Wait Until Element Is Visible  ${validationValorDoInvestimento.lblTitleValorDoInvestimento}  30
    Element Text Should Be  ${validationValorDoInvestimento.lblTitleValorDoInvestimento}  Valor do Investimento
    Wait Until Element Is Visible  ${validationValorDoInvestimento.lblTextValorDoInvestimento}  30
    Element Text Should Be  ${validationValorDoInvestimento.lblTextValorDoInvestimento}  Quanto você quer investir?
    Element Text Should Be  ${validationValorDoInvestimento.lblTextSaldoAplicacaoInvestimento}  Saldo em conta
    
    Wait Until Element Is Visible  ${pageValueInvestment.lblInsertValue}  30
    ${pageValueInvestmentInsertValue}  Set Variable  ${pageValueInvestment.lblInsertValue}
    ${valueInvestmentApply}  Set Variable  950
    Input Text  ${pageValueInvestmentInsertValue}  ${valueInvestmentApply}

    Wait Until Element Is Visible  ${pageValueInvestment.lblInsertValue}  30
    ${pageValueInvestmentInsertValue}  Set Variable  ${pageValueInvestment.lblInsertValue}
    ${valueInvestmentApply}  Set Variable  999999995000
    Input Text  ${pageValueInvestmentInsertValue}  ${valueInvestmentApply}

    Wait Until Element Is Visible  ${pageValueInvestment.lblInsertValue}  30
    ${pageValueInvestmentInsertValue}  Set Variable  ${pageValueInvestment.lblInsertValue}
    ${valueInvestmentApply}  Set Variable  5000
    Input Text  ${pageValueInvestmentInsertValue}  ${valueInvestmentApply}
    
    Wait Until Element Is Visible  ${pageValueInvestment.btnContinueApply}  30
    Element Text Should Be  ${pageValueInvestment.btnContinueApply}  Continuar
    Click Element  ${pageValueInvestment.btnContinueApply}

    Wait Until Element Is Visible  ${validationResumoDoInvestimento.lblTitlePageResumoDoInvestimento}  30
    Element Text Should Be  ${validationResumoDoInvestimento.lblTitlePageResumoDoInvestimento}  Resumo do investimento

    Element Text Should Be  ${validationResumoDoInvestimento.lblResumoValorInvestimento}  Valor do investimento
    ${valueFrontResumoValorInvestimento}  Get Text  ${validationResumoDoInvestimento.lblResumoValueInvestimento}
    ${str}  Remove String  ${valueFrontResumoValorInvestimento}  R$  .  ,
    ${valueFrontResumoValorInvestimento}  Strip String  ${str}
    Should Be Equal As Strings  ${valueFrontResumoValorInvestimento}  ${valueInvestmentApply}

    Element Text Should Be  ${validationResumoDoInvestimento.lblResumoInvestimentoRentabilidade}  Rentabilidade
    ${valueFrontInvestimentoRentabilidade}  Get Text  ${validationResumoDoInvestimento.lblResumoValueInvestimentoRentabilidade}
    Should Be Equal As Strings  ${valueFrontInvestimentoRentabilidade}  ${profitabilityDetail}

    Element Text Should Be  ${validationResumoDoInvestimento.lblResumoInvestimentoVencimento}  Vencimento
    ${valueResumoInvestimentoDtVencimento}  Get Text  ${validationResumoDoInvestimento.lblResumoValueInvestimentoVencimento}
    @{words}  Split String  ${valueResumoInvestimentoDtVencimento}  ${SPACE}
    ${numMes}  Dealing with dates with texts  ${words[2]}
    ${dtCompare}  catenate  ${words[0]}.${numMes}.${words[4]}
    Should Be Equal As Strings  ${maturityDate}  ${dtCompare}

    Wait Until Element Is Visible  ${pageResumeInvestment.btnConfirmApplication}  30
    ${applyResult}  Set Variable  Apply

    Element Text Should Be  ${pageResumeInvestment.btnConfirmApplication}  Confirmar aplicação
    Click Element  ${pageResumeInvestment.btnConfirmApplication}

    Wait Until Element Is Visible  ${pageResumeInvestment.btnMyInvestments}  30
    Element Text Should Be  ${validationModalInvestimentoRealizado.lblTitleModalInvestimento}  Investimento realizado com sucesso
    Element Text Should Be  ${validationModalInvestimentoRealizado.lblResumeModalInvestimento}  Parabéns, seu investimento foi realizado e a partir de agora seu dinheiro vai render muito mais.

    Wait Until Element Is Visible  ${pageResumeInvestment.btnMyInvestments}  30
    Element Text Should Be  ${pageResumeInvestment.btnMyInvestments}  Meus investimentos
    Click Element  ${pageResumeInvestment.btnMyInvestments}

    Wait Until Element Is Visible  //android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]

    Wait Until Element Is Visible  ${pageMyInvestments.btnBackMyInvestments}  30
    Click Element  ${pageMyInvestments.btnBackMyInvestments}

    Wait Until Element Is Visible  //androidx.cardview.widget.CardView[1]/android.view.ViewGroup
    Wait Until Element Is Visible  //android.view.ViewGroup/androidx.cardview.widget.CardView
    Wait Until Element Is Visible  //android.widget.FrameLayout/android.widget.ImageView


Apply_Transaction_LCI
    [Arguments]  ${ITEM}

    ${idViewGroup}  evaluate  ${ITEM}+1

    Wait Until Element Is Visible  //androidx.cardview.widget.CardView[1]/android.view.ViewGroup
    Wait Until Element Is Visible  //android.view.ViewGroup/androidx.cardview.widget.CardView
    Wait Until Element Is Visible  //android.widget.FrameLayout/android.widget.ImageView

    Wait Until Element Is Visible  ${pageDashboardInvestimentos.btnViewBalance}  30
    Element Text Should Be  ${pageDashboardInvestimentos.btnViewBalance}  Mostrar saldo
    Click Element  ${pageDashboardInvestimentos.btnViewBalance}

    Verification user balance graphic  ${cpf}  ${senha}

    ${valueFrontInitialInvestment}  Get Text  ${pageDashboardInvestimentos.lblInicitalInvestment}    
    ${str}  Remove String  ${valueFrontInitialInvestment}  R$  .  ,
    ${valueFrontInitialInvestmentStripp}  Strip String  ${str}
    Should Be Equal As Integers   ${valueFrontInitialInvestmentStripp}  ${investmentValueTotal}

    ${valueFrontBalanceTotal}  Get Text  ${pageDashboardInvestimentos.lblViewBalanceValue}
    ${str}  Remove String  ${valueFrontBalanceTotal}  R$  .  ,
    ${valueFrontBalanceTotalStripp}  Strip String  ${str}
    Should Be Equal As Integers  ${valueFrontBalanceTotalStripp}  ${balanceTotal}

    ${valueFrontGrossValueTotal}  Get Text  ${pageDashboardInvestimentos.ViewGrossEarnings}
    ${str}  Remove String  ${valueFrontGrossValueTotal}  R$  .  ,
    ${valueFrontGrossValueTotalStripp}  Strip String  ${str}
    Should Be Equal As Integers  ${valueFrontGrossValueTotalStripp}  ${grossEarningsTotal}

    Element Text Should Be  ${pageDashboardInvestimentos.btnInvestDashboard}  Investir
    Click Element  ${pageDashboardInvestimentos.btnInvestDashboard}

    Wait Until Element Is Visible  ${validationRendaFixa.lblTitlePageRendaFixa}  90
    Element Text Should Be  ${validationRendaFixa.lblTitlePageRendaFixa}  Renda Fixa

    Wait Until Element Is Visible  ${validationRendaFixa.lblTitleCardRendaFixa}  30 

    Wait Until Element Is Visible  ${pageListOfPapers.lblFilterLCI}  30
    Click Element  ${pageListOfPapers.lblFilterLCI}

    Wait Until Element Is Visible  ${validationRendaFixa.lblTitleCardRendaFixa}  50 
    
    ${type}  Set Variable  LCI
    Enable list of papers  ${type}  ${ITEM}

    ${valueFrontTitleCardRendaFixa}  Get Text  ${validationRendaFixa.lblTitleCardRendaFixa}
    Should Be Equal As Strings  ${valueFrontTitleCardRendaFixa}  ${description}

    ${valueFrontRendaFixaValueRentabilidade}  Get Text  //android.view.ViewGroup[${idViewGroup}]/android.widget.TextView[4]
    Should Be Equal As Strings  ${valueFrontRendaFixaValueRentabilidade}  ${profitability}

    ${valueFrontRendaFixaInvestimentoMin}  Get Text  //android.view.ViewGroup[${idViewGroup}]/android.widget.TextView[7]
    ${str}  Remove String  ${valueFrontRendaFixaInvestimentoMin}  R$  .  ,
    ${valueFrontRendaFixaInvestimentoMin}  Strip String  ${str}
    Should Be Equal As Integers  ${valueFrontRendaFixaInvestimentoMin}  ${minimumInvestmentValue}

    ${valueFrontRendaFixaVencimento}  Get Text  //android.view.ViewGroup[${idViewGroup}]/android.widget.TextView[8]
    ${maturityDate}  Convert Date  ${maturityDate}  datetime  date_format=%Y-%m-%d
    ${valueFrontRendaFixaVencimento}  Convert Date  ${valueFrontRendaFixaVencimento}  datetime  date_format=%d/%m/%Y
    Should Be Equal  ${valueFrontRendaFixaVencimento}  ${maturityDate}

    Wait Until Element Is Visible  ${pageListOfPapers.mdlFGC}  30
    Element Text Should Be  ${validationRendaFixa.lblRendaFixaModal}  FGC
    Click Element  ${pageListOfPapers.mdlFGC}

    Wait Until Element Is Visible  ${pageListOfPapers.btnExitModalFGC}  30
    Click Element  ${pageListOfPapers.btnExitModalFGC}

    Wait Until Element Is Visible  ${pageListOfPapers.lblCardType}  30
    Click Element  //androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[${idViewGroup}]

    ${type}  Set Variable  LCI
    View investment details  ${type}  ${ITEM}

    Wait Until Element Is Visible  ${validationDetalhamentoInvestimento.lblTitlePageDetalhamentoInvestimento}  30
    Element Text Should Be  ${validationDetalhamentoInvestimento.lblTitlePageDetalhamentoInvestimento}  Detalhes do produto

    Wait Until Element Is Visible  ${validationDetalhamentoInvestimento.lblTitleDetalhamentoInvestimento}  30
    ${valueFrontTitleDetalhamento}  Get Text  ${validationDetalhamentoInvestimento.lblTitleDetalhamentoInvestimento}
    Should Be Equal As Strings  ${valueFrontTitleDetalhamento}  ${descriptionTitleDetail}

    Element Text Should Be  ${validationDetalhamentoInvestimento.lblDIRentabilidade}  Rentabilidade
    ${valueFrontProfitabilityDetail}  Get Text  ${validationDetalhamentoInvestimento.lblDIValueRentabilidade}
    Should Be Equal As Strings  ${valueFrontProfitabilityDetail}  ${profitabilityDetail}

    Element Text Should Be  ${validationDetalhamentoInvestimento.lblDIInvestimentoMin}  Investimento mínimo
    ${valueFrontDetailInvestimentoMin}  Get Text  ${validationDetalhamentoInvestimento.lblDIValueInvestimentoMin}
    ${str}  Remove String  ${valueFrontDetailInvestimentoMin}  R$  .  ,
    ${valueFrontDetailInvestimentoMin}  Strip String  ${str}
    Should Be Equal As Integers  ${valueFrontDetailInvestimentoMin}  ${minimumInvestmentValueDetail}

    Element Text Should Be  ${validationDetalhamentoInvestimento.lblDIVencimento}  Vencimento
    ${valueDetailVencimentoInvestment}  Get Text  ${validationDetalhamentoInvestimento.lblDIValueVencimento}
    ${maturityDate}  Convert Date  ${maturityDate}  result_format=%d.%m.%Y
    @{words}  Split String  ${valueDetailVencimentoInvestment}  ${SPACE}
    ${numMes}  Dealing with dates with texts  ${words[2]}
    ${dtCompare}  catenate  ${words[0]}.${numMes}.${words[4]}
    Should Be Equal As Strings  ${maturityDate}  ${dtCompare}
    
    Element Text Should Be  ${validationDetalhamentoInvestimento.lblDiasResgate}  Resgate

    Wait Until Element Is Visible  ${pageDetailAssets.mdlAssetsFGC}  30
    Click Element  ${pageDetailAssets.mdlAssetsFGC}
 
    Wait Until Element Is Visible  ${pageDetailAssets.btnExitDetailModalFGC}  30
    Click Element  ${pageDetailAssets.btnExitDetailModalFGC}

    Scroll  ${pageDetailAssets.lblIOFRendimentoScroll}   ${pageDetailAssets.lblTitleScroll}

    Wait Until Element Is Visible  ${validationDetalhamentoInvestimento.lblDIIRSobreRendimento}  30
    Element Text Should Be  ${validationDetalhamentoInvestimento.lblDIIRSobreRendimento}  IR sobre o rendimento
    Element Text Should Be  ${validationDetalhamentoInvestimento.lblDIValueIRSobreRendimentoIsent}  Isento

    Element Text Should Be  ${validationDetalhamentoInvestimento.lblDIIOF}  IOF sobre o rendimento
    Element Text Should Be  ${validationDetalhamentoInvestimento.lblDIValueIOF}  Isento após 30 dias

    Element Text Should Be  ${validationDetalhamentoInvestimento.lblDITaxaADM}  Taxa administrativa
    ${valueFrontDITaxaADM}  Get Text  ${validationDetalhamentoInvestimento.lblDIValueTaxaADM}
    ${str}  Remove String  ${valueFrontDITaxaADM}  %
    ${valueFrontDITaxaADM}  Strip String  ${str}
    Should Be Equal As Strings  ${valueFrontDITaxaADM}  ${administrativeTax}

    Element Text Should Be  ${validationDetalhamentoInvestimento.lblDIEmissor}  Emissor
    ${valueFrontDIValueEmissor}  Get Text  ${validationDetalhamentoInvestimento.lblDIValueEmissor}
    Should Be Equal As Strings  ${valueFrontDIValueEmissor}  ${issuerDetail}

    Element Text Should Be  ${validationDetalhamentoInvestimento.lblDIRating}  Rating
    ${valueFrontDIValueRating}  Get Text  ${validationDetalhamentoInvestimento.lblDIValueRating}
    Should Be Equal As Strings  ${valueFrontDIValueRating}  ${ratingValueDetail}

    Element Text Should Be  ${validationDetalhamentoInvestimento.lblDIProtecao}  Proteção
    
    Wait Until Element Is Visible  ${pageDetailAssets.btnGarantidoFGC}  30
    Element Text Should Be  ${validationDetalhamentoInvestimento.lblDIValueProtecao}  Garantido pelo FGC 
    Click Element  ${pageDetailAssets.btnGarantidoFGC}

    Wait Until Element Is Visible  ${pageDetailAssets.btnClosedGarantidoFGC}  30
    Click Element  ${pageDetailAssets.btnClosedGarantidoFGC}

    Wait Until Element Is Visible  ${pageDetailAssets.btnContinueAssets}  30
    Element Text Should Be  ${pageDetailAssets.btnContinueAssets}  Investir
    Click Element  ${pageDetailAssets.btnContinueAssets}

    Wait Until Element Is Visible  ${validationValorDoInvestimento.lblTitleValorDoInvestimento}  30
    Element Text Should Be  ${validationValorDoInvestimento.lblTitleValorDoInvestimento}  Valor do Investimento
    Wait Until Element Is Visible  ${validationValorDoInvestimento.lblTextValorDoInvestimento}  30
    Element Text Should Be  ${validationValorDoInvestimento.lblTextValorDoInvestimento}  Quanto você quer investir?

    Element Text Should Be  ${validationValorDoInvestimento.lblTextSaldoAplicacaoInvestimento}  Saldo em conta

    Wait Until Element Is Visible  ${pageValueInvestment.lblInsertValue}  30
    ${pageValueInvestmentInsertValue}  Set Variable  ${pageValueInvestment.lblInsertValue}
    ${valueInvestmentApply}  Set Variable  4000
    Input Text  ${pageValueInvestmentInsertValue}  ${valueInvestmentApply}
    
    Wait Until Element Is Visible  ${pageValueInvestment.btnContinueApply}  30
    Element Text Should Be  ${pageValueInvestment.btnContinueApply}  Continuar
    Click Element  ${pageValueInvestment.btnContinueApply}

    Wait Until Element Is Visible  ${validationResumoDoInvestimento.lblTitlePageResumoDoInvestimento}  30
    Element Text Should Be  ${validationResumoDoInvestimento.lblTitlePageResumoDoInvestimento}  Resumo do investimento

    Element Text Should Be  ${validationResumoDoInvestimento.lblResumoValorInvestimento}  Valor do investimento
    ${valueFrontResumoValorInvestimento}  Get Text  ${validationResumoDoInvestimento.lblResumoValueInvestimento}
    ${str}  Remove String  ${valueFrontResumoValorInvestimento}  R$  .  ,
    ${valueFrontResumoValorInvestimento}  Strip String  ${str}
    Should Be Equal As Strings  ${valueFrontResumoValorInvestimento}  ${valueInvestmentApply}

    Element Text Should Be  ${validationResumoDoInvestimento.lblResumoInvestimentoRentabilidade}  Rentabilidade
    ${valueFrontInvestimentoRentabilidade}  Get Text  ${validationResumoDoInvestimento.lblResumoValueInvestimentoRentabilidade}
    Should Be Equal As Strings  ${valueFrontInvestimentoRentabilidade}  ${profitabilityDetail}

    Element Text Should Be  ${validationResumoDoInvestimento.lblResumoInvestimentoVencimento}  Vencimento
    ${valueResumoInvestimentoDtVencimento}  Get Text  ${validationResumoDoInvestimento.lblResumoValueInvestimentoVencimento}
    @{words}  Split String  ${valueResumoInvestimentoDtVencimento}  ${SPACE}
    ${numMes}  Dealing with dates with texts  ${words[2]}
    ${dtCompare}  catenate  ${words[0]}.${numMes}.${words[4]}
    Should Be Equal As Strings  ${maturityDate}  ${dtCompare}

    Wait Until Element Is Visible  ${pageResumeInvestment.btnConfirmApplication}  30
    ${applyResult}  Set Variable  Apply 

    Element Text Should Be  ${pageResumeInvestment.btnConfirmApplication}  Confirmar aplicação
    Click Element  ${pageResumeInvestment.btnConfirmApplication}

    Wait Until Element Is Visible  ${pageResumeInvestment.btnMyInvestments}  30
    Element Text Should Be  ${validationModalInvestimentoRealizado.lblTitleModalInvestimento}  Investimento realizado com sucesso
    Element Text Should Be  ${validationModalInvestimentoRealizado.lblResumeModalInvestimento}  Parabéns, seu investimento foi realizado e a partir de agora seu dinheiro vai render muito mais.

    Wait Until Element Is Visible  ${pageResumeInvestment.btnMyInvestments}  30
    Element Text Should Be  ${pageResumeInvestment.btnMyInvestments}  Meus investimentos
    Click Element  ${pageResumeInvestment.btnMyInvestments}

    Wait Until Element Is Visible  //android.widget.LinearLayout/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]

    Wait Until Element Is Visible  ${pageMyInvestments.btnBackMyInvestments}  30
    Click Element  ${pageMyInvestments.btnBackMyInvestments}

    Wait Until Element Is Visible  //androidx.cardview.widget.CardView[1]/android.view.ViewGroup
    Wait Until Element Is Visible  //android.view.ViewGroup/androidx.cardview.widget.CardView
    Wait Until Element Is Visible  //android.widget.FrameLayout/android.widget.ImageView