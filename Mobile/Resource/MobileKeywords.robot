*** Keywords *** 

Open APP
    Open Application  remote_url=http://localhost:4723/wd/hub  udid=0053837094  platformName=Android  deviceName=moto g(7) play  appPackage=br.com.bancobari.qa  appActivity=br.com.bancobari.OnboardingActivity

Login
    [Arguments]  ${cpf}  ${senha}
    Wait Until Element Is Visible  ${pageLogin.btnAcessar}  800
    Click Element  ${pageLogin.btnAcessar}
    Wait Until Element Is Visible  ${pageLogin.cpf}  30
    Input Text  ${pageLogin.cpf}  ${cpf}
    Click Element  ${pageLogin.btncontinue}
    Wait Until Element Is Visible  ${pageLogin.psw}  30
    Input Text  ${pageLogin.psw}  ${senha}
    Wait Until Element Is Visible  ${pageLogin.btncontinue}  30
    Click Element  ${pageLogin.btncontinue}
    Wait Until Element Is Visible  ${pageBiometria.btnAgoraNao}  30
    Click Element  ${pageBiometria.btnAgoraNao}  
    Wait Until Page Does Not Contain Element  ${pageLoading.carregando}
    [Return]  ${cpf}  ${senha}

Verification user balance
    [Arguments]  ${cpf}  ${senha}
    Abrir conexao   https://api.${server}.bancobari.com.br
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    ${RESPOSTAREQUEST}  Consult Balance User  ${guid}  ${token}
    Set Test Variable  ${customerInvestments}  ${RESPOSTAREQUEST.json()}
    Set Test Variable  ${investmentValueTotal}  ${customerInvestments['investmentValue']}
    Set Test Variable  ${grossEarningsTotal}  ${customerInvestments['grossEarnings']}
    Set Test Variable  ${balanceTotal}  ${customerInvestments['balance']}

Verification user balance graphic
    [Arguments]  ${cpf}  ${senha}
    Abrir conexao   https://api.${server}.bancobari.com.br
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${RESPOSTAREQUEST}  Consult Balance User  ${guid}  ${token}
    Set Test Variable  ${customerInvestments}  ${RESPOSTAREQUEST.json()}

    Set Test Variable  ${investmentValueTotal}  ${customerInvestments['investmentValue']}
    Set Test Variable  ${grossEarningsTotal}  ${customerInvestments['grossEarnings']}
    Set Test Variable  ${balanceTotal}  ${customerInvestments['balance']}

    Set Test Variable  ${customerTypeLCI}  ${customerInvestments['byType'][0]['type']}
    Set Test Variable  ${customerInvestmentsValueLCI}  ${customerInvestments['byType'][0]['investmentValue']}
    Set Test Variable  ${customerGrossEarningsLCI}  ${customerInvestments['byType'][0]['grossEarnings']}
    Set Test Variable  ${customerBalanceLCI}  ${customerInvestments['byType'][0]['balance']}

    Set Test Variable  ${customerTypeCDB}  ${customerInvestments['byType'][1]['type']}
    Set Test Variable  ${customerInvestmentsValueCDB}  ${customerInvestments['byType'][1]['investmentValue']}
    Set Test Variable  ${customerGrossEarningsCDB}  ${customerInvestments['byType'][1]['grossEarnings']}
    Set Test Variable  ${customerBalanceCDB}  ${customerInvestments['byType'][1]['balance']}

    Set Test Variable  ${customerTypeLC}  ${customerInvestments['byType'][2]['type']}
    Set Test Variable  ${customerInvestmentsValueLC}  ${customerInvestments['byType'][2]['investmentValue']}
    Set Test Variable  ${customerGrossEarningsLC}  ${customerInvestments['byType'][2]['grossEarnings']}
    Set Test Variable  ${customerBalanceLC}  ${customerInvestments['byType'][2]['balance']}

    # Set Test Variable  ${customerTypeCRI}  ${customerInvestments['byType'][3]['type']}
    # Set Test Variable  ${customerInvestmentsValueCRI}  ${customerInvestments['byType'][3]['investmentValue']}
    # Set Test Variable  ${customerGrossEarningsCRI}  ${customerInvestments['byType'][3]['grossEarnings']}
    # Set Test Variable  ${customerBalanceCRI}  ${customerInvestments['byType'][3]['balance']}

Verification user balance graphic general
    [Arguments]  ${cpf}  ${senha}
    Abrir conexao   https://api.${server}.bancobari.com.br
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${RESPOSTAREQUEST}  Consult Balance User  ${guid}  ${token}
    Set Test Variable  ${customerInvestments}  ${RESPOSTAREQUEST.json()}

    Set Test Variable  ${investmentValueTotal}  ${customerInvestments['investmentValue']}
    Set Test Variable  ${grossEarningsTotal}  ${customerInvestments['grossEarnings']}
    Set Test Variable  ${balanceTotal}  ${customerInvestments['balance']}

    Set Test Variable  ${customerType}  ${customerInvestments['byType'][0]['type']}
    Set Test Variable  ${customerInvestmentsValue}  ${customerInvestments['byType'][0]['investmentValue']}
    Set Test Variable  ${customerGrossEarnings}  ${customerInvestments['byType'][0]['grossEarnings']}
    Set Test Variable  ${customerBalance}  ${customerInvestments['byType'][0]['balance']}

Enable list of papers
    [Arguments]     ${type}  ${ITEM}
    Abrir conexao   https://api.${server}.bancobari.com.br
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    ${RESPOSTA}  List Of Papers  ${guid}  ${token}  ${type}
    Log  ${RESPOSTA.text}
    Set Test Variable  ${page}  ${RESPOSTAREQUEST.json()}
    Set Test Variable  ${Id}  ${page['items'][${ITEM}]['id']}
    Set Test Variable  ${description}  ${page['items'][${ITEM}]['description']}
    Set Test Variable  ${profitability}  ${page['items'][${ITEM}]['profitability']} 
    Set Test Variable  ${minimumInvestmentValue}  ${page['items'][${ITEM}]['minimumInvestmentValue']}   
    Set Test Variable  ${maturityDate}  ${page['items'][${ITEM}]['maturityDate']}

View investment details
    [Arguments]  ${type}  ${ITEM}
    Abrir conexao     https://api.${server}.bancobari.com.br
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    ${RESPOSTALIST}   List Of Papers  ${guid}  ${token}  ${type}
    Set Test Variable  ${page}  ${RESPOSTALIST.json()}
    Set Test Variable  ${Id}  ${page['items'][${ITEM}]['id']}
    ${RESPOSTACHOOSELIST}  Choose Investment Type  ${guid}  ${token}  ${Id}
    Log  ${RESPOSTACHOOSELIST.text}
    Set Test Variable  ${pageDetail}  ${RESPOSTACHOOSELIST.json()}
    Set Test Variable  ${descriptionTitleDetail}  ${pageDetail['description']}
    Set Test Variable  ${profitabilityDetail}  ${pageDetail['profitability']}
    Set Test Variable  ${minimumInvestmentValueDetail}  ${pageDetail['minimumInvestmentValue']}
    Set Test Variable  ${maturityDateDetail}  ${pageDetail['maturityDate']}
    Set Test Variable  ${iofFreeAfter}  ${pageDetail['iofFreeAfter']}
    Set Test Variable  ${ir}  ${pageDetail['ir']}
    Set Test Variable  ${liquidityDetail}  ${pageDetail['liquidity']}
    Set Test Variable  ${administrativeTax}  ${pageDetail['administrativeTax']}
    Set Test Variable  ${issuerDetail}  ${pageDetail['issuer']}
    Set Test Variable  ${ratingValueDetail}  ${pageDetail['ratingValue']}

View Withdraw
    [Arguments]  ${ITEM}
    Abrir conexao   https://api.${server}.bancobari.com.br
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable  ${guid}
    Set Test Variable  ${token}
    ${itemsPerPage}   Set Variable   4
    ${Page}  Set Variable   0
    ${type}  Set Variable   true
    ${RESPONSE}       Redemption Investments   ${guid}     ${token}    ${itemsPerPage}    ${Page}    ${type}
    Set Test Variable  ${page}  ${RESPONSE.json()}
    Set Test Variable  ${pageWithdraw}  ${RESPOSTAREQUEST.json()}
    Set Test Variable  ${valueWithdrawId}  ${page['items'][${ITEM}]['id']}
    Set Test Variable  ${valueTitleDescriptionWithdraw}  ${page['items'][${ITEM}]['description']}
    Set Test Variable  ${valueWithdrawInvestmentValue}  ${page['items'][${ITEM}]['investmentValue']}
    Set Test Variable  ${valueWithdrawProfitability}  ${page['items'][${ITEM}]['profitability']}
    Set Test Variable  ${valueWithdrawGrossEarningsValue}  ${page['items'][${ITEM}]['grossEarningsValue']}
    Set Test Variable  ${valueWithdrawEmissionDate}  ${page['items'][${ITEM}]['emissionDate']}
    Set Test Variable  ${valueWithdrawMaturityDate}  ${page['items'][${ITEM}]['maturityDate']}
    Set Test Variable  ${valueWithdrawLiquidity}  ${page['items'][${ITEM}]['liquidity']}

View Withdraw Detail
    [Arguments]  ${ITEM}
    Abrir conexao   https://api.${server}.bancobari.com.br
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}  ${senha}
    Set Test Variable   ${guid}
    Set Test Variable   ${token}
    ${itemsPerPage}   Set Variable   10
    ${Page}  Set Variable   0
    ${type}  Set Variable   true
    ${RESPONSE}  Redemption Investments   ${guid}  ${token}  ${itemsPerPage}  ${Page}  ${type}
    Set Test Variable  ${page}   ${RESPONSE.json()}
    Set Test Variable  ${id}   ${page['items'][${ITEM}]['id']}
    ${RESPOSTAREQUEST}  View Details Investments Redemption   ${guid}  ${token}  ${id}
    Set Test Variable  ${pageWithdrawDetail}  ${RESPOSTAREQUEST.json()}
    Set Test Variable  ${valueDescriptionDetailWithdraw}  ${pageWithdrawDetail['description']}
    Set Test Variable  ${valueNetValueDetailWithdraw}  ${pageWithdrawDetail['netValue']}
    Set Test Variable  ${valueProfitabilityDetailWithdraw}  ${pageWithdrawDetail['profitability']}
    Set Test Variable  ${valueMaturityDateDetailWithdraw}  ${pageWithdrawDetail['maturityDate']}
    Set Test Variable  ${valueInvestmentValueDetailWithdraw}  ${pageWithdrawDetail['investmentValue']}
    Set Test Variable  ${valueGrossEarningsValueDetailWithdraw}  ${pageWithdrawDetail['grossEarningsValue']}
    Set Test Variable  ${valueGrossValueDetailWithdraw}  ${pageWithdrawDetail['grossValue']}
    Set Test Variable  ${valueIRDetailWithdraw}  ${pageWithdrawDetail['irValue']}
    Set Test Variable  ${valueIOFDetailWithdraw}  ${pageWithdrawDetail['iofValue']}
    Set Test Variable  ${valueAdministrativeTax}  ${pageWithdrawDetail['administrativeTax']}
    Set Test Variable  ${valueIssuer}  ${pageWithdrawDetail['issuer']}
    Set Test Variable  ${valueRatingValue}  ${pageWithdrawDetail['ratingValue']}

View Withdraw False
    [Arguments]  ${ITEM}
    Abrir conexao   https://api.${server}.bancobari.com.br
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}    ${senha}
    Set Test Variable   ${guid}
    Set Test Variable   ${token}
    ${itemsPerPage}   Set Variable   1
    ${Page}  Set Variable   0
    ${type}  Set Variable   false
    ${RESPONSE}  Redemption Investments   ${guid}  ${token}  ${itemsPerPage}  ${Page}  ${type}
    Set Test Variable  ${page}  ${RESPONSE.json()}
    Set Test Variable  ${pageWithdraw}  ${RESPOSTAREQUEST.json()}
    Set Test Variable  ${valueWithdrawId}  ${page['items'][${ITEM}]['id']}
    Set Test Variable  ${valueTitleDescriptionWithdraw}  ${page['items'][${ITEM}]['description']}
    Set Test Variable  ${valueWithdrawInvestmentValue}  ${page['items'][${ITEM}]['investmentValue']}
    Set Test Variable  ${valueWithdrawProfitability}  ${page['items'][${ITEM}]['profitability']}
    Set Test Variable  ${valueWithdrawGrossEarningsValue}  ${page['items'][${ITEM}]['grossEarningsValue']}
    Set Test Variable  ${valueWithdrawEmissionDate}  ${page['items'][${ITEM}]['emissionDate']}
    Set Test Variable  ${valueWithdrawMaturityDate}  ${page['items'][${ITEM}]['maturityDate']}
    Set Test Variable  ${valueWithdrawLiquidity}  ${page['items'][${ITEM}]['liquidity']}

View Withdraw Detail False
    [Arguments]  ${ITEM}
    Abrir conexao   https://api.${server}.bancobari.com.br
    ${guid}  Gera Guid
    ${token}  Gera Token Investimentos  ${cpf}    ${senha}
    Set Test Variable   ${guid}
    Set Test Variable   ${token}
    ${itemsPerPage}   Set Variable   1
    ${Page}  Set Variable   0
    ${type}  Set Variable   false
    ${RESPONSE}  Redemption Investments   ${guid}     ${token}    ${itemsPerPage}    ${Page}    ${type}
    Set Test Variable  ${page}   ${RESPONSE.json()}
    Set Test Variable  ${id}   ${page['items'][${ITEM}]['id']}
    ${RESPOSTAREQUEST}   View Details Investments Redemption   ${guid}  ${token}  ${id}
    Set Test Variable  ${pageWithdrawDetail}  ${RESPOSTAREQUEST.json()}
    Set Test Variable  ${valueDescriptionDetailWithdraw}  ${pageWithdrawDetail['description']}
    Set Test Variable  ${valueNetValueDetailWithdraw}  ${pageWithdrawDetail['netValue']}
    Set Test Variable  ${valueProfitabilityDetailWithdraw}  ${pageWithdrawDetail['profitability']}
    Set Test Variable  ${valueMaturityDateDetailWithdraw}  ${pageWithdrawDetail['maturityDate']}
    Set Test Variable  ${valueInvestmentValueDetailWithdraw}  ${pageWithdrawDetail['investmentValue']}
    Set Test Variable  ${valueGrossEarningsValueDetailWithdraw}  ${pageWithdrawDetail['grossEarningsValue']}
    Set Test Variable  ${valueGrossValueDetailWithdraw}  ${pageWithdrawDetail['grossValue']}
    Set Test Variable  ${valueIRDetailWithdraw}  ${pageWithdrawDetail['irValue']}
    Set Test Variable  ${valueIOFDetailWithdraw}  ${pageWithdrawDetail['iofValue']}
    Set Test Variable  ${valueAdministrativeTax}  ${pageWithdrawDetail['administrativeTax']}
    Set Test Variable  ${valueIssuer}  ${pageWithdrawDetail['issuer']}
    Set Test Variable  ${valueRatingValue}  ${pageWithdrawDetail['ratingValue']}

Dealing with dates with texts
    [Arguments]  ${mesTexto}
    ${numMes}=  Set Variable If  
    ...  """${mesTexto}"""=="Janeiro"  01
    ...  """${mesTexto}"""=="Fevereiro"  02
    ...  """${mesTexto}"""=="Março"  03
    ...  """${mesTexto}"""=="Abril"  04
    ...  """${mesTexto}"""=="Maio"  05
    ...  """${mesTexto}"""=="Junho"  06
    ...  """${mesTexto}"""=="Julho"  07
    ...  """${mesTexto}"""=="Agosto"  08
    ...  """${mesTexto}"""=="Setembro"  09
    ...  """${mesTexto}"""=="Outubro"  10
    ...  """${mesTexto}"""=="Novembro"  11
    ...  """${mesTexto}"""=="Dezembro"  12
    [Return]  ${numMes}