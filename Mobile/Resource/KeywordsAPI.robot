*** Variable ***
${server}=  qa


*** Keywords ***

Abrir conexao    
    [Arguments]     ${url}
    Create Session      newsession      ${url}  verify=True

Gera Token Investimentos
    [Arguments]  ${cpf}  ${senha}

    Abrir conexao   https://api.${server}.bancobari.com.br
    ${HEADERS}  Create Dictionary
    ...  Content-Type=application/x-www-form-urlencoded
    ${PARAMS}  Create Dictionary
    ...  username=${cpf}
    ...  password=${senha}
    ...  client_id=mobile
    ...  client_secret=mH8341Kop
    ...  grant_type=password
    Set Test Variable  ${HEADERS}
    ${RESPOSTAREQUEST}     Post Request     newsession      auth/connect/token
    ...     data=${PARAMS}
    ...     headers=${HEADERS}
    Log                          ${RESPOSTAREQUEST.text}
    ${json_value}  Set Variable  ${RESPOSTAREQUEST.json()}
    ${token}  Catenate  Bearer  ${json_value['access_token']}
    Set Test Variable  ${token}
    [Return]  ${token}

Consult Balance User
    [Arguments]     ${guid}     ${token}

    ${PARAMS}       Create Dictionary            
    ${HEADERS}      Create Dictionary            accept=application/json  User-Agent=${guid}  Authorization=${token}  
    Abrir conexao   https://api.${server}.bancobari.com.br
    ${RESPOSTAREQUEST}       Get Request         new session         investment/v1/customers/balance
    ...                                          data=${PARAMS}
    ...                                          headers=${HEADERS}
    Set Test Variable  ${RESPOSTAREQUEST}
    Log     ${RESPOSTAREQUEST.text}
    [Return]  ${RESPOSTAREQUEST}

List Of Papers
    [Arguments]     ${guid}     ${token}    ${type}

    ${PARAMS}       Create Dictionary            
    ${HEADERS}      Create Dictionary            accept=application/json  type=${type}  User-Agent=${guid}  Authorization=${token}  
    Abrir conexao   https://api.${server}.bancobari.com.br
    ${RESPOSTAREQUEST}     Get Request           new session         investment/v1/assets?type=${type}
    ...                                          data=${PARAMS}
    ...                                          headers=${HEADERS}
    Set Test Variable  ${RESPOSTAREQUEST}
    Log     ${RESPOSTAREQUEST.text}
    [Return]  ${RESPOSTAREQUEST}

Choose Investment Type
    [Arguments]     ${guid}     ${token}      ${Id}

    ${PARAMS}       Create Dictionary            
    ${HEADERS}      Create Dictionary            accept=application/json  User-Agent=${guid}  Authorization=${token}  
    Abrir conexao   https://api.${server}.bancobari.com.br
    ${RESPOSTAREQUEST}     Get Request           new session         investment/v1/assets/${Id}
    ...                                          data=${PARAMS}
    ...                                          headers=${HEADERS}
    Set Test Variable  ${RESPOSTAREQUEST}
    Log     ${RESPOSTAREQUEST.text}
    [Return]  ${RESPOSTAREQUEST}

Redemption Investments
    [Arguments]     ${guid}    ${token}    ${itemsPerPage}   ${Page}    ${type}

    ${PARAMS}       Create Dictionary            
    ${HEADERS}      Create Dictionary            accept=application/json  User-Agent=${guid}  Authorization=${token}  
    Abrir conexao   https://api.${server}.bancobari.com.br
    ${RESPOSTAREQUEST}     Get Request           new session         investment/v1/custody?withdrawalAvailable=${type}&itemsPerPage=${itemsPerPage}&Page=${Page}
    ...                                          data=${PARAMS}
    ...                                          headers=${HEADERS}
    Set Test Variable  ${RESPOSTAREQUEST}
    Log     ${RESPOSTAREQUEST.text}
    [Return]  ${RESPOSTAREQUEST}

View Details Investments Redemption
    [Arguments]     ${guid}    ${token}    ${id}
    
    ${PARAMS}       Create Dictionary            
    ${HEADERS}      Create Dictionary            accept=application/json  User-Agent=${guid}  Authorization=${token}  
    Abrir conexao   https://api.${server}.bancobari.com.br
    ${RESPOSTAREQUEST}     Get Request           new session         investment/v1/custody/${id}
    ...                                          data=${PARAMS}
    ...                                          headers=${HEADERS}
    Set Test Variable  ${RESPOSTAREQUEST}
    Log     ${RESPOSTAREQUEST.text}
    [Return]  ${RESPOSTAREQUEST}
    



