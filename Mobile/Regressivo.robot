*** Settings ***
Resource                  ${EXECDIR}/Mobile/Resource/Settings.resource

*** Variable ***
@{asserts}  
${cpf}=  08130385961
${senha}=  123Abc

*** Test Cases ***

Regressivo_Graphic_New_Dashboard

    Open APP
    Login  ${cpf}  ${senha}

    ${progress1}  Run Keyword and Ignore Error  Graphic_Persona_Four_Investments
    Append To List  ${asserts}  ${progress1[1]}
    Run Keyword If  "${progress1[0]}"=="FAIL"  Falha  ${cpf}  ${senha}

    Close Application

    Open APP
    ${cpf}  Set Variable  09699387998
    ${senha}  Set Variable  123Abc
    Login  ${cpf}  ${senha}

    ${progress2}  Run Keyword and Ignore Error  Graphic_Persona_Investments_CDB
    Append To List  ${asserts}  ${progress2[1]}
    Run Keyword If  "${progress2[0]}"=="FAIL"  Falha  ${cpf}  ${senha}
    
    Close Application

    Open APP
    ${cpf}  Set Variable  10146495950
    ${senha}  Set Variable  123Abc
    Login  ${cpf}  ${senha}

    ${progress3}  Run Keyword and Ignore Error  Graphic_Persona_Investments_LCI
    Append To List  ${asserts}  ${progress3[1]}
    Run Keyword If  "${progress3[0]}"=="FAIL"  Falha  ${cpf}  ${senha}

    Close Application

    Open APP
    ${cpf}  Set Variable  76432015871
    ${senha}  Set Variable  123Abc
    Login  ${cpf}  ${senha}

    ${progress4}  Run Keyword and Ignore Error  Graphic_Persona_No_Investments
    Append To List  ${asserts}  ${progress4[1]}
    Run Keyword If  "${progress4[0]}"=="FAIL"  Falha  ${cpf}  ${senha}

    Close Application


Regressivo_Persona_Investments

    Open APP
    ${cpf}  Set Variable  08130385961
    ${senha}  Set Variable  123Abc
    Login  ${cpf}  ${senha}
    
    ${progress5}  Run Keyword and Ignore Error  Suitability_Conservador
    Append To List  ${asserts}  ${progress5[1]}
    Run Keyword If  "${progress5[0]}"=="FAIL"  Falha  ${cpf}  ${senha}

    ${progress6}  Run Keyword and Ignore Error  Suitability_Moderado
    Append To List  ${asserts}  ${progress6[1]}
    Run Keyword If  "${progress6[0]}"=="FAIL"  Falha  ${cpf}  ${senha}

    ${progress7}  Run Keyword and Ignore Error  Suitability_Agressive
    Append To List  ${asserts}  ${progress7[1]}
    Run Keyword If  "${progress7[0]}"=="FAIL"  Falha  ${cpf}  ${senha}

    ${progress8}  Run Keyword and Ignore Error  FOR_Apply_Transaction_CDB
    Append To List  ${asserts}  ${progress8[1]}
    Run Keyword If  "${progress8[0]}"=="FAIL"  Falha  ${cpf}  ${senha}

    ${progress9}  Run Keyword and Ignore Error  FOR_Apply_Transaction_LCI
    Append To List  ${asserts}  ${progress9[1]}
    Run Keyword If  "${progress9[0]}"=="FAIL"  Falha  ${cpf}  ${senha}

    ${progress10}  Run Keyword and Ignore Error  FOR_Withdraw_Transaction
    Append To List  ${asserts}  ${progress10[1]}
    Run Keyword If  "${progress10[0]}"=="FAIL"  Falha  ${cpf}  ${senha}

    ${progress11}  Run Keyword and Ignore Error  No_Income_Report
    Append To List  ${asserts}  ${progress11[1]}
    Run Keyword If  "${progress11[0]}"=="FAIL"  Falha  ${cpf}  ${senha}

    ${progress12}  Run Keyword and Ignore Error  Perfil_User
    Append To List  ${asserts}  ${progress12[1]}
    Run Keyword If  "${progress12[0]}"=="FAIL"  Falha  ${cpf}  ${senha}

    ${progress13}  Run Keyword and Ignore Error  FOR_Apply_Transaction_Janela
    Append To List  ${asserts}  ${progress13[1]}
    Run Keyword If  "${progress13[0]}"=="FAIL"  Falha  ${cpf}  ${senha}

    ${progress14}  Run Keyword and Ignore Error  FOR_Withdraw_Transaction_Janela
    Append To List  ${asserts}  ${progress14[1]}
    Run Keyword If  "${progress14[0]}"=="FAIL"  Falha  ${cpf}  ${senha}

    ${progress15}  Run Keyword and Ignore Error  FOR_My_Investments
    Append To List  ${asserts}  ${progress15[1]}
    Run Keyword If  "${progress15[0]}"=="FAIL"  Falha  ${cpf}  ${senha}

    ${progress16}  Run Keyword and Ignore Error  FOR_No_Assets_Investment
    Append To List  ${asserts}  ${progress16[1]}
    Run Keyword If  "${progress16[0]}"=="FAIL"  Falha  ${cpf}  ${senha}

    ${progress17}  Run Keyword and Ignore Error  Withdraw_Transaction_LC
    Append To List  ${asserts}  ${progress17[1]}
    Run Keyword If  "${progress17[0]}"=="FAIL"  Falha  ${cpf}  ${senha}

    ${progress18}  Run Keyword and Ignore Error  My_Investment_LC
    Append To List  ${asserts}  ${progress18[1]}
    Run Keyword If  "${progress18[0]}"=="FAIL"  Falha  ${cpf}  ${senha}

    ${progress19}  Run Keyword and Ignore Error  History_Withdraw
    Append To List  ${asserts}  ${progress19[1]}
    Run Keyword If  "${progress19[0]}"=="FAIL"  Falha  ${cpf}  ${senha}

    ${progress20}  Run Keyword and Ignore Error  History_My_Investments
    Append To List  ${asserts}  ${progress20[1]}
    Run Keyword If  "${progress20[0]}"=="FAIL"  Falha  ${cpf}  ${senha}

    ${progress21}  Run Keyword and Ignore Error  Learn_to_invest
    Append To List  ${asserts}  ${progress21[1]}
    Run Keyword If  "${progress21[0]}"=="FAIL"  Falha  ${cpf}  ${senha}

    Close Application


Regressivo_Persona_No_Investments

    Open APP
    Start Screen Recording
    ${cpf}  Set Variable  76432015871
    ${senha}  Set Variable  123Abc
    Login  ${cpf}  ${senha}

    ${progress22}  Run Keyword and Ignore Error  Suitability_Moderado_No_Investments
    Append To List  ${asserts}  ${progress22[1]}
    Run Keyword If  "${progress22[0]}"=="FAIL"  Falha  ${cpf}  ${senha}

    ${progress23}  Run Keyword and Ignore Error  Validation_dashboard_no_investments
    Append To List  ${asserts}  ${progress23[1]}
    Run Keyword If  "${progress23[0]}"=="FAIL"  Falha  ${cpf}  ${senha}

    ${progress24}  Run Keyword and Ignore Error  No_Withdraw_Transaction
    Append To List  ${asserts}  ${progress24[1]}
    Run Keyword If  "${progress24[0]}"=="FAIL"  Falha  ${cpf}  ${senha}

    ${progress25}  Run Keyword and Ignore Error  No_Income_Report_Investments
    Append To List  ${asserts}  ${progress25[1]}
    Run Keyword If  "${progress25[0]}"=="FAIL"  Falha  ${cpf}  ${senha}

    ${progress26}  Run Keyword and Ignore Error  Perfil_User_Investments
    Append To List  ${asserts}  ${progress26[1]}
    Run Keyword If  "${progress26[0]}"=="FAIL"  Falha  ${cpf}  ${senha}

    ${progress27}  Run Keyword and Ignore Error  Attendance_Menu_Install_WhatsApp
    Append To List  ${asserts}  ${progress27[1]}
    Run Keyword If  "${progress27[0]}"=="FAIL"  Falha  ${cpf}  ${senha}

    ${progress28}  Run Keyword and Ignore Error  Attendance_Menu_Desinstall_WhatsApp
    Append To List  ${asserts}  ${progress28[1]}
    Run Keyword If  "${progress28[0]}"=="FAIL"  Falha  ${cpf}  ${senha}

    Stop Screen Recording
    Close Application


    Set Test Variable  ${validacao}  ${EMPTY}
    :FOR  ${assert}  IN  @{asserts}
    ${var}  Evaluate  "None"=="""${assert}"""
    Run Keyword Unless   ${var}  FalhaValidacao
    END
    Should Be Equal As Strings  ${validacao}  ${EMPTY}


***Keyword***

Falha
    [Arguments]  ${cpf}  ${senha}
    Close Application
    Open APP
    Login  ${cpf}  ${senha}

FalhaValidacao
    ${validacao}  Set Variable  Fail
    Set Test Variable  ${validacao}

FOR_Apply_Transaction_CDB
    Start Screen Recording
    FOR  ${ITEM}  IN RANGE  0  2
        Apply_Transaction_CDB  ${ITEM}
    END
    Stop Screen Recording

FOR_Apply_Transaction_LCI
    Start Screen Recording
    FOR  ${ITEM}  IN RANGE  0  2
        Apply_Transaction_LCI  ${ITEM}
    END
    Stop Screen Recording

FOR_Withdraw_Transaction
    Start Screen Recording
    FOR  ${ITEM}  IN RANGE  0  1
        Withdraw_Transaction  ${ITEM}
    END
    Stop Screen Recording

FOR_Apply_Transaction_Janela
    Start Screen Recording
    FOR  ${ITEM}  IN RANGE  0  1
        Apply_Transaction_Janela  ${ITEM}
    END

FOR_Withdraw_Transaction_Janela
    FOR  ${ITEM}  IN RANGE  0  1
        Withdraw_Transaction_Janela  ${ITEM}
    END
    Stop Screen Recording

FOR_My_Investments
    Start Screen Recording
    FOR  ${ITEM}  IN RANGE  0  1
        My_Investments  ${ITEM}
    END
    Stop Screen Recording

FOR_OrderPaper_Privacy
    Start Screen Recording
    FOR  ${ITEM}  IN RANGE  0  2
        OrderPaper_Privacy  ${ITEM}
    END
    Stop Screen Recording

FOR_No_Assets_Investment
    Start Screen Recording
    FOR  ${ITEM}  IN RANGE  0  2
        No_Assets_Investment  ${ITEM}
    END
    Stop Screen Recording